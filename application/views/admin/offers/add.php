
<!-- Start of the main content -->
<div id="main_content">
<?php if ($error) { ?>
                <div class="alert error">
                    <span></span><span class="hide">x</span>
                    <?php echo $error; ?>
                </div>
            <?php } ?>
<h2 class="grid_12">Add Offer<div style="float:right;">
                    <a href="<?php echo site_url('admin/offers/') ?>">Back</a>
                </div></h2>
    <div class="clean"></div>
        <div class="grid_12">

            <div class="box">
                <div class="header">
                    <img src="<?php echo base_url(); ?>template/admin/img/icons/packs/fugue/16x16/block--plus.png" alt="" width="16"
                         height="16">
                    <h3>Add Offer</h3>
                    <span></span>
                </div>
                <form id="frm" method="post" class="validate" action="<?php echo site_url('admin/offers/add/');?>" enctype="multipart/form-data">

                <div class="content no-padding">
                         
                    
                       
                    <div class="section _100">
                        <label>
                            Offer Title
                        </label>
                        <div>
                <input name="txtname" id="txtname" type="text" placeholder="Offer Title" class="required" />
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Merchants
                        </label>
                        <div class="input select">
<select id="ddltypemerchant" name="ddltypemerchant[]" multiple class="required">
                                <option value="">Select Merchant</option>
                                <?php foreach ($merchants as $merchant) {
                                    
                                    ?>
                                    <option value="<?php echo $merchant['merchant_id'];?>"><?php echo $merchant['merchant_name'];?></option>
                                    <?php }?>
                                </select>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Offer Type
                        </label>
                        <div class="input select">
<select id="ddltypeoffer" name="ddltypeoffer" class="required">
                                <option value="">Select Offer Type</option>
                                <?php foreach ($offertypes as $type) {
                                    
                                    ?>
                                    <option value="<?php echo $type['id'];?>"><?php echo $type['offer_type'];?></option>
                                    <?php }?>
                                </select>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                        Description                        </label>
                        <div>
<textarea name="txtdesc" id="txtdesc" placeholder="Description" class="required"> </textarea>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                             Start Date
                        </label>
                        <div>
                <input name="txtstartdate" id="txtstartdate" type="text" placeholder="Start Date" class="required"/>
                           
                        </div>
                    </div>

                    <div class="section _100">
                        <label>
                            End Date
                        </label>
                        <div>
                <input name="txtenddate" id="txtenddate" type="text" placeholder="End Date" class="required"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div id="abcd" style="display:none;">
                    <div class="section _100">
                        <label>
                             Amount
                        </label>
                        <div>
                <input name="txtamount" id="txtamount" type="text" placeholder="Amount" class="required"/>
                           
                        </div>
                    </div>

                    <div class="section _100">
                        <label>
                            Discount
                        </label>
                        <div>
                <input name="txtdiscount" id="txtdiscount" type="text" placeholder="Discount" class="required"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                     <div class="section _100">
                        <label>
                           Max. Discount(Per User)
                        </label>
                        <div>
                <input name="txtmaxdiscountusr" id="txtmaxdiscountuser" type="text" placeholder="Maximum Discount(Per User)" class="required"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                <div class="section _100">
                        <label>
                           Max. Discount(Per Transaction)
                        </label>
                        <div class="input select">
                <input name="txtmaxdiscounttrans" id="txtmaxdiscounttrans" type="text" placeholder="Maximum Discount(Per Transaction)" class="required"/>

                        </div>
                    </div>                   
                    </div>
                     <div class="section _100" id="promotion" style="display:none;">
                        <label>
                        Promotional Text                        </label>
                        <div>
<textarea name="txtpromotional" id="txtpromotional" placeholder="Promotional Text" class="required"> </textarea>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                           Offer Image
                        </label>
                        <div style="margin-top:30px;">
                <input name="image" id="image" type="file" class="required" />

                            
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <div class="actions-right">
                        <input type="submit" id="submit" name="submit" value="Submit"/>
                    </div>
                </div>
                 </form>

            </div> <!-- End of .box -->
        </div> <!-- End of .grid_6 -->
</div>
<script type="text/javascript">
$(document).ready(function () {
$('#txtstartdate').datepicker({
                    dateFormat: "yy-mm-dd",
                changeMonth: true,
                    changeYear: true,
                    minDate: '0d',
                onSelect: function(dateStr) {
                $('#txtenddate').datepicker({
                    dateFormat: "yy-mm-dd",
                changeMonth: true,
              changeYear: true,

                     });

                    var newDate = $(this).datepicker('getDate');

                    $('#txtenddate').datepicker('option','minDate', newDate);
                    },

                });
$("#ddltypeoffer").change(function(){
    var value = $(this).val();
    if(value==1){
     $("#abcd").css('display','');
      $("#txtpromotional").removeClass('required');
     

    }else if(value==2){
  $("#abcd").css('display','none');
  $("#promotion").css('display','');
  $("#txtpromotional").addClass('required');
    }else if(value==''){
$("#promotion").css('display','none');
 $("#abcd").css('display','none');
 $("#txtpromotional").removeClass('required');

    }
});
});
</script>