
<div id="main_content">
        <h2 class="grid_12">Edit Offer
        <div style="float:right;">
            <a href="<?php echo site_url('admin/offers/') ?>">Back</a>
        </div></h2>
        <div class="clean"></div>
        <div class="grid_12">
                <div class="box">
                        <div class="header">
                                <img src="<?php echo base_url();?>template/admin/img/icons/packs/fugue/16x16/block--pencil.png" alt="" width="16"
                                height="16">
                                <h3>Edit Offer</h3>
                                <span></span>
                        </div>
                       <form id="frm" method="post" class="validate" action="<?php echo site_url('admin/offers/edit/'.$this->uri->segment(4));?>" enctype="multipart/form-data">

                <div class="content no-padding">
                         
                    
                       
                    <div class="section _100">
                        <label>
                            Offer Title
                        </label>
                        <div>
                <input name="txtname" id="txtname" type="text" placeholder="Offer Title" class="required" value="<?php echo $portsDetailsArr[0]['title'];?>" />
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Merchants
                        </label>
                        <div class="input select">
<select id="ddltypemerchant" name="ddltypemerchant[]" multiple class="required">
                                <option value="">Select Merchant</option>
                                <?php foreach ($merchants as $merchant) {
                                      $div = explode(',', $portsDetailsArr[0]['merchant_id']);
                                    ?>
                                    <option value="<?php echo $merchant['merchant_id'];?>" <?php if(in_array($merchant['merchant_id'],$div)){echo "selected";}?>><?php echo $merchant['merchant_name'];?></option>
                                    <?php }?>
                                </select>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Offer Type
                        </label>
                        <div class="input select">
<select id="ddltypeoffer" name="ddltypeoffer" class="required">
                                <option value="">Select Offer Type</option>
                                <?php foreach ($offertypes as $type) {
                                    
                                    ?>
                                    <option value="<?php echo $type['id'];?>" <?php if($portsDetailsArr[0]['type']==$type['id']){echo "selected";}?>><?php echo $type['offer_type'];?></option>
                                    <?php }?>
                                </select>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                        Description                        </label>
                        <div>
<textarea name="txtdesc" id="txtdesc" placeholder="Description" class="required"> <?php echo $portsDetailsArr[0]['description'];?> </textarea>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                             Start Date
                        </label>
                        <div>
                <input name="txtstartdate" id="txtstartdate" type="text" placeholder="Start Date" class="required" value="<?php echo date("Y-m-d", strtotime($portsDetailsArr[0]['start_date']));?>" />
                           
                        </div>
                    </div>

                    <div class="section _100">
                        <label>
                            End Date
                        </label>
                        <div>
                <input name="txtenddate" id="txtenddate" type="text" placeholder="End Date" class="required" value="<?php echo date("Y-m-d", strtotime($portsDetailsArr[0]['end_date']));?>"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                   
                    <div id="abcd" <?php if($portsDetailsArr[0]['type']!=1){?> style="display:none;"<?php }?>>
                    <div class="section _100">
                        <label>
                             Amount
                        </label>
                        <div>
                <input name="txtamount" id="txtamount" type="text" placeholder="Amount" class="required" value="<?php echo $portsDetailsArr[0]['amount'];?>"/>
                           
                        </div>
                    </div>

                    <div class="section _100">
                        <label>
                            Discount
                        </label>
                        <div>
                <input name="txtdiscount" id="txtdiscount" type="text" placeholder="Discount" class="required" value="<?php echo $portsDetailsArr[0]['offer_discount'];?>"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                     <div class="section _100">
                        <label>
                           Max. Discount(Per User)
                        </label>
                        <div>
                <input name="txtmaxdiscountusr" id="txtmaxdiscountuser" type="text" placeholder="Maximum Discount(Per User)" class="required" value="<?php echo $portsDetailsArr[0]['maxdiscountperuser'];?>"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                <div class="section _100">
                        <label>
                           Max. Discount(Per Transaction)
                        </label>
                        <div class="input select">
                <input name="txtmaxdiscounttrans" id="txtmaxdiscounttrans" type="text" placeholder="Maximum Discount(Per Transaction)" class="required" value="<?php echo $portsDetailsArr[0]['maxdiscountpertransaction'];?>"/>

                        </div>
                    </div>                   
                    </div>
                    
                     <div class="section _100" id="promotion" <?php if($portsDetailsArr[0]['type']!=2){?> style="display:none;"<?php }?>>
                        <label>
                        Promotional Text                        </label>
                        <div>
<textarea name="txtpromotional" id="txtpromotional" placeholder="Promotional Text" <?php if($portsDetailsArr[0]['type']==2){?>class="required"<?php }?>><?php echo $portsDetailsArr[0]['promotionaltext'];?></textarea>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    
                    <div class="section _100">
                        <label>
                           Offer Image
                        </label>
                        <div style="margin-top:30px;">
                <input name="image" id="image" type="file"/>
<img width="80" height="80" style="margin-left:270px;" src="<?php if($portsDetailsArr[0]['image']) {echo site_url()?>/uploads/offers/<?php echo $portsDetailsArr[0]['image'];}else{echo site_url()?>/template/admin/img/no-images.jpg<?php }?>">

                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <div class="actions-right">
                        <input type="submit" id="submit" name="submit" value="Submit"/>
                    </div>
                </div>
                 </form>
                </div> <!-- End of .box -->
        </div> <!-- End of .grid_6 -->
</div>

<script type="text/javascript">
$(document).ready(function () {
$('#txtstartdate').datepicker({
                    dateFormat: "yy-mm-dd",
                changeMonth: true,
                    changeYear: true,
                    //minDate: '0d',
                onSelect: function(dateStr) {
                $('#txtenddate').datepicker({
                    dateFormat: "yy-mm-dd",
                changeMonth: true,
              changeYear: true,

                     });

                    var newDate = $(this).datepicker('getDate');

                    $('#txtenddate').datepicker('option','minDate', newDate);
                    },

                });
$("#ddltypeoffer").change(function(){
    var value = $(this).val();
    if(value==1){
     $("#abcd").css('display','');
     $("#txtpromotional").removeClass('required');

    }else if(value==2){
  $("#abcd").css('display','none');
  $("#promotion").css('display','');
  $("#txtpromotional").addClass('required');
    }else if(value==''){
         $("#abcd").css('display','none');
$("#promotion").css('display','none');
$("#txtpromotional").removeClass('required');


    }
});
});
</script>