<script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.dataTables.js"></script>
<script>
    $(document).ready(function () {
         var $dialog = $("#view_dialog").dialog(
                {
                    autoOpen: false,
                    title: 'Merchant Categories',
                   
                    width: 400,
                    resizable: true,
                    modal: true,
                    buttons:
                    {
                        "Cancel": function()
                        {
                            $(this).dialog("close");
                        }
                    }
                });
          $(".view_dialog").click(function()
            {
                       
                $dialog.load($(this).attr('href'), function ()
                        {
                            $dialog.dialog('open');
                        });
                return false;
            });
            
            
       var $dialog1 = $("#view_dialog").dialog(
            {
                autoOpen: false,
                title: 'Merchant Categories',
                
                width: 400,
                resizable: true,
                modal: true,
                buttons:
                {
                    "Cancel": function()
                    {
                        $(this).dialog("close");
                    }
                }
            });
            $(".edit_link").click(function()
            {
                $dialog1.load($(this).attr('href'), function ()
                        {
                            $dialog1.dialog('open');
                        });
                return false;
            });
        
        $(".status").click(function(){
                        var id=$(this).val();
                        if($(this).is(":unchecked")) {
                        // checkbox is checked -> do something
                                $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: baseURL+"admin/segment/publish/"+id,
                                        success: function(response) {
                                        // success function is called when data came back
                                        // for example: get your content and display it on your site
                                        
                                        }
                                });
                        }
                        else{
                                // checkbox is not checked -> do something different
                                $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: baseURL+"admin/segment/unpublish/"+id,
                                        success: function(response) {
                                        // success function is called when data came back
                                        // for example: get your content and display it on your site
                                       
                                        }
                                });
                        }
                });
        

        $('#table-example').dataTable({
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': [0,2]}
            ]
        });
        
    });
</script>

<div id="main_content">
<?php if ($this->session->flashdata('success')) {
                ?>
                <div class="alert success">
                    <span class="icon"></span><span class="hide">x</span><strong>Success</strong>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php } elseif ($this->session->flashdata('error')) { ?>
                <div class="alert error">
                    <span class="icon"></span><span class="hide">x</span><strong>Error</strong>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>
    <h2 class="grid_12">Category List
         <div style="float:right;">
            <a href="<?php echo site_url('admin/merchants/addcategory') ?>" class="view_dialog">Add Category</a>
        </div>
    </h2>
    <div class="clean"></div>
    <div class="grid_12">
    <div id="view_dialog"></div>
        <div class="box">

            <div class="header">
                <img src="<?php echo base_url(); ?>template/admin/img/icons/packs/fugue/16x16/shadeless/chart.png" width="16" height="16">
                <h3>Category List</h3>
                
            </div>

            <div class="content">
                <table id="table-example" class="table">
                    <thead>
                        <tr>
                            
                            <th>
                                Sr. No.
                            </th>
                            <th>
                                Category
                            </th>
                            

                            <th>
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $x = 1;
                        foreach ($categories as $category) {
                            ?>
                            <tr class="gradeX" id="recordsArray" >
                                
                                <td>
                                    <?php echo $x++; ?>
                                </td>
                                <td>
                                    <?php echo $category['category']; ?>
                                </td>
                                 
                                <td class="center">
                                    <a title="Edit Item" href="<?php echo site_url('admin/merchants/editcategory/' . $category['category_id']); ?>" class="edit_link">
                                        <img src='<?php echo base_url(); ?>template/admin/img/icons/packs/diagona/16x16/019.png'/>
                                    </a>
                                   
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div> <!-- End of .content -->
        </div>
    </div>
</div> <!-- End of #main_content -->


<script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery-fallr-1.2.js"></script>
