<div id="main_content">
    <h2 class="grid_12">Dashboard</h2>
    <div class="clean"></div>
    <div class="grid_12">
        <div id="shortcuts-steps" class="box tabbed">
            <div class="header">
                <h3>Panel</h3>
            </div>
            <div class="content">
                <ul id="shortcuts" class="shortcuts tab-content">
                    <li>
                        <div class="shortcut-icon">
                            <img src="<?php echo base_url(); ?>template/admin/img/icons/25x25/blue/group.png" width="25" height="25">
                            <div class="divider"></div>
                        </div>
                        <a class="shortcut-description" href="<?php echo site_url('admin/merchants') ?>">
                            <strong>Merchants</strong>
                            <span>Your description here.</span>
                        </a>
                    </li>
                    <li>
                        <div class="shortcut-icon">
                            <img src="<?php echo base_url(); ?>template/admin/img/icons/25x25/blue/user.png" width="25" height="25">
                            <div class="divider"></div>
                        </div>
                        
                        <a class="shortcut-description" href="<?php echo site_url('admin/users');?>">
                            <strong>Users</strong>
                            <span>Your description here.</span>
                        </a>
                    </li>
                    <li>
                        <div class="shortcut-icon">
                            <img src="<?php echo base_url(); ?>template/admin/img/icons/25x25/blue/shopping-cart-2.png" width="25" height="25">
                            <div class="divider"></div>
                        </div>
                        <a class="shortcut-description" href="<?php echo site_url('admin/offers');?>">
                            <strong>Offers</strong>
                            <span>Your description here.</span>
                        </a>
                    </li>
                    <!-- <li>
                        <div class="shortcut-icon">
                            <img src="<?php echo base_url(); ?>template/admin/img/icons/25x25/blue/admin-user.png" width="25" height="25">
                            <div class="divider"></div>
                        </div>
                        <a class="shortcut-description" href="<?php echo site_url('admin/users') ?>">
                            <strong>Users</strong>
                            <span>Your description here.</span>
                        </a>
                    </li>
                    
                    <li>
                        <div class="shortcut-icon">
                            <img src="<?php echo base_url(); ?>template/admin/img/icons/25x25/blue/group.png" width="25" height="25">
                            <div class="divider"></div>
                        </div>
                        <a class="shortcut-description" href="<?php echo site_url('admin/customers') ?>">
                            <strong>Customers</strong>
                            <span>Your description here.</span>
                        </a>
                    </li>

					<li>
                        <div class="shortcut-icon">
                            <img src="<?php echo base_url(); ?>template/admin/img/icons/25x25/blue/scan-label.png" width="25" height="25">
                            <div class="divider"></div>
                        </div>
                        <a class="shortcut-description" href="<?php echo site_url('admin/products') ?>">
                            <strong>Products</strong>
                            <span>Your description here.</span>
                        </a>
                    </li>

					<li>
                        <div class="shortcut-icon">
                            <img src="<?php echo base_url(); ?>template/admin/img/icons/25x25/blue/shuffle.png" width="25" height="25">
                            <div class="divider"></div>
                        </div>
                        <a class="shortcut-description" href="<?php echo site_url('admin/related') ?>">
                            <strong>Related</strong>
                            <span>Your description here.</span>
                        </a>
                    </li>

					<li>
                        <div class="shortcut-icon">
                            <img src="<?php echo base_url(); ?>template/admin/img/icons/25x25/blue/airplane.png" width="25" height="25">
                            <div class="divider"></div>
                        </div>
                        <a class="shortcut-description" href="<?php echo site_url('admin/exports') ?>">
                            <strong>Exports</strong>
                            <span>Your description here.</span>
                        </a>
                    </li>

					<li>
                        <div class="shortcut-icon">
                            <img src="<?php echo base_url(); ?>template/admin/img/icons/25x25/blue/cash-register.png" width="25" height="25">
                            <div class="divider"></div>
                        </div>
                        <a class="shortcut-description" href="<?php echo site_url('admin/segment') ?>">
                            <strong>Segments</strong>
                            <span>Your description here.</span>
                        </a>
                    </li>

					<li>
                        <div class="shortcut-icon">
                            <img src="<?php echo base_url(); ?>template/admin/img/icons/25x25/blue/settings.png" width="25" height="25">
                            <div class="divider"></div>
                        </div>
                        <a class="shortcut-description" href="<?php echo site_url('admin/zones') ?>">
                            <strong>Zones</strong>
                            <span>Your description here.</span>
                        </a>
                    </li> -->
                    
                </ul> 
                <!-- End of #shortcuts -->
            </div>
            <!-- End of .content -->
        </div> <!-- End of .box -->
    </div> <!-- End of .grid_8 -->
</div> <!-- End of #main_content -->


