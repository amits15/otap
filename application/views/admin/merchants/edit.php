
<div id="main_content">
        <h2 class="grid_12">Edit Merchant
        <div style="float:right;">
            <a href="<?php echo site_url('admin/merchants/') ?>">Back</a>
        </div></h2>
        <div class="clean"></div>
        <div class="grid_12">
                <div class="box">
                        <div class="header">
                                <img src="<?php echo base_url();?>template/admin/img/icons/packs/fugue/16x16/block--pencil.png" alt="" width="16"
                                height="16">
                                <h3>Edit Merchant</h3>
                                <span></span>
                        </div>
                        <form id="frm_add_db" method="post" class="validate" action="<?php echo site_url('admin/merchants/edit/'.$this->uri->segment(4,0));?>" enctype="multipart/form-data">
                                 <div class="content no-padding">
                         
                    
                       
                    <div class="section _100">
                        <label>
                            Merchant Name
                        </label>
                        <div>
                <input name="txtname" id="txtname" type="text" placeholder="Merchant Name" class="required" value="<?php echo $merchantdetails[0]['merchant_name'];?>" />
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                        Description                        </label>
                        <div>
<textarea name="txtadesc" id="txtadesc" placeholder="Description" class="required"><?php echo $merchantdetails[0]['description'];?> </textarea>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                           Shop Name
                        </label>
                        <div>
                <input name="txtshopname" id="txtshopname" type="text" placeholder="Shop Name" value="<?php echo $merchantdetails[0]['merchantshop_name'];?>" class="required" />
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                        Address                        </label>
                        <div>
<textarea name="txtaddress" id="txtaddress" placeholder="Address" class="required"><?php echo $merchantdetails[0]['address'];?> </textarea>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                     <div class="section _100">
                        <label>
                             Pin Code
                        </label>
                        <div>
                <input name="txtpin" id="txtpin" type="digits" placeholder="Pin Code" class="required" value="<?php echo $merchantdetails[0]['pincode'];?>" />
                           
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                             Mobile
                        </label>
                        <div>
                        <?php 
                                               
        $div = explode(',', $merchantdetails[0]['merchant_contact']);
        $arr['divs'] = $div;
        
            ?>
        <a class="btn btn-success btn-xs add-txtupdate" href="javascript:void(0);" style="float:right;margin-top:14px;">
        <img src="<?php echo base_url(); ?>template/admin/img/icons/packs/diagona/16x16/103.png"></a>
<div class="more" id="<?php echo count($arr['divs']);?>">
                <?php if($merchantdetails[0]['merchant_contact']==''){?>
                <input name="txtmobile[]" id="1" type="text" placeholder="Mobile No."  style="width: 96%;"/>
                <?php }else{
                    $x = 1;
                    $y=1;
                    foreach($arr['divs'] as $d){

                        if($d!=''){?>
                        <a class="btn btn-success btn-xs delete" id = "<?php echo $x++;?>" href="javascript:void(0);" style="float:right;margin-top:14px;<?php if($x==2){ ?>display:none;<?php }?>"><img src="<?php echo base_url(); ?>template/admin/img/icons/packs/diagona/16x16/104.png"></a>
                <input name="txtmobile[]" id="<?php echo $y++;?>" type="text" placeholder="Mobile No." value="<?php echo $d;?>"  style="width: 96%;"/>
                <?php }}}?>
                    </div>
                </div>
                </div>
                    <div class="section _100">
                        <label>
                            Email
                        </label>
                        <div>
                <input name="txtemail" id="txtemail" type="email" placeholder="Email" value="<?php echo $merchantdetails[0]['merchant_email'];?>" class="required"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Password
                        </label>
                        <div>
                <input name="txtpassword" id="txtpassword" type="password" placeholder="Password" />
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Category
                        </label>
                        <div>
<select name="ddlcategory[]" id="ddlcategory" multiple class="required" >
<?php if($categories){
    
    foreach($categories as $category){
       $div = explode(',', $merchantdetails[0]['categories']);
      ?>
        
<option value="<?php echo $category['category_id'];?>" <?php if(in_array($category['category_id'],$div)){echo "selected";}?>><?php echo $category['category'];?></option>
<?php }}?>
</select>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                             Bank Name
                        </label>
                        <div>
                <input name="txtbank" id="txtbank" type="text" placeholder="Bank Name" class="required" value="<?php echo $merchantdetails[0]['bankname'];?>"/>
                           
                        </div>
                    </div>

                    <div class="section _100">
                        <label>
                            Account Number
                        </label>
                        <div>
                <input name="txtaccount" id="txtaccount" type="text" placeholder="Account Number" class="required" value="<?php echo $merchantdetails[0]['accountnumber'];?>"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Branch Name
                        </label>
                        <div>
                <input name="txtbranch" id="txtbranch" type="text" placeholder="Branch Name" class="required" value="<?php echo $merchantdetails[0]['branch'];?>"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            IFSC
                        </label>
                        <div>
                <input name="txtifsc" id="txtifsc" type="text" placeholder="IFSC" class="required" value="<?php echo $merchantdetails[0]['ifsc'];?>"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>MICR
                        </label>
                        <div>
                <input name="txtmicr" id="txtmicr" type="text" placeholder="MICR" class="required" value="<?php echo $merchantdetails[0]['micr'];?>"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            PAN Number
                        </label>
                        <div>
                <input name="txtpan" id="txtpan" type="text" placeholder="PAN Number" class="required" value="<?php echo $merchantdetails[0]['pan'];?>"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Date Of Incorporation
                        </label>
                        <div>
                <input name="txtincorporation" id="txtincorporation" type="text" placeholder="Date Of Incorporation" class="required" value="<?php echo $merchantdetails[0]['dateofincorporation'];?>"/>
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            KYC Document
                        </label>
                        <div style="margin-top:30px;">
                <input name="kyc" id="kyc" type="file"/>

                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Profile Photo
                        </label>
                        <div style="margin-top:30px;">
                <input name="profilepic" id="profilepic" type="file"/>

                <img width="80" height="80" style="margin-left:270px;" src="<?php if($merchantdetails[0]['profilepic']) {echo site_url()?>/uploads/merchants/<?php echo $merchantdetails[0]['profilepic'];}else{echo site_url()?>/template/admin/img/no-images.jpg<?php }?>">
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                </div>
                                <div class="actions" >
                                        <div class="actions-left">

                                        </div>
                                        <div class="actions-right">
                                                <input type="submit" name='submit' />
                                        </div>
                                </div>
                        </form>
                </div> <!-- End of .box -->
        </div> <!-- End of .grid_6 -->
</div><script type="text/javascript">
$(document).ready(function () {
$('#txtincorporation').datepicker({
                    dateFormat: "yy-mm-dd",
                changeMonth: true,
                    changeYear: true                
                

                });
});
</script>
<script type="text/javascript">
$('body').on('click','.add-txtupdate', function () {
     var a = $(".more").attr('id');
        //alert(a);
                var no = a;
                //alert(no);
                //$(".more").attr('id',no);
    if(no<=4){
                no = parseInt(no,10) + 1;
    var more_textbox = $('<a class="btn btn-success btn-xs delete" id = "'+no+'" href="javascript:void(0);" style="float:right;margin-top:14px;"><img src="<?php echo base_url(); ?>template/admin/img/icons/packs/diagona/16x16/104.png"></a><input name="txtmobile[]" id="'+no+'" type="digits" placeholder="Mobile Number"  style="width: 96%;"/>');
                more_textbox.hide();
                $(".more").after(more_textbox);
                $(".more").attr('id',no);
                more_textbox.fadeIn("slow");
                return false;
            }
});
$('body').on('click','.delete', function () {
   id = $(this).attr('id');
   
   no = parseInt(id,10) - 1;
  $(".more").attr('id',no);
  //alert(id);
  $('input[id='+id+']').remove();
  $('a[id='+id+']').remove();

});
</script>