<script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.dataTables.js"></script>

<script>
    $(document).ready(function () {
        $('.checkall').click(function () {
                if ($(this).attr('checked')=='checked')
                {   
                    $('input:checkbox.chkbox').removeAttr('checked');
                }
                else
                {
                   $('input:checkbox.chkbox').attr('checked', 'checked');
                }
        });

         $('#mysubmit').click(function(){
                        $.fallr('show', {
                                buttons : {
                                        button1 : {
                                                text: 'Yes',
                                                danger: true, 
                                                onclick: function(){
                                                 var l =  $('.chkbox:checked').length;
                                                 if(l > 0){
                                                     $('#frmproducts').submit();
                                                 }
                                                 else{
                                                        //Remove Buttons
                                                        $("#fallr-wrapper #fallr-buttons").html(""); 
                                                        $("#fallr-wrapper #fallr-icon").hide(); 
                                                        //Replace Content
                                                        $("#fallr-wrapper #fallr").html("<b>Select atleast one record</b>");
                                                        // Hide after 5 seconds
                                                        setTimeout(function(){ $.fallr('hide'); }, 3000);
                                                    }
                                                }
                                        },
                                        button2 : {
                                                text: 'Cancel',
                                                onclick: function(){
                                                        $.fallr('hide')
                                                }
                                        }
                                },
                                content : '<p>Are you sure you want to delete records?</p>',
                                icon    : 'error'
                        });
                });
        $(".status").click(function(){
                        var id=$(this).val();
                        if($(this).is(":unchecked")) {
                        // checkbox is checked -> do something
                                $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: baseURL+"admin/customers/publish/"+id,
                                        success: function(response) {
                                        // success function is called when data came back
                                        // for example: get your content and display it on your site
                                        
                                        }
                                });
                        }
                        else{
                                // checkbox is not checked -> do something different
                                $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: baseURL+"admin/customers/unpublish/"+id,
                                        success: function(response) {
                                        // success function is called when data came back
                                        // for example: get your content and display it on your site
                                       
                                        }
                                });
                        }
                });
        
        $('#table-example').dataTable({
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': [2]}
            ]
        });
        
    });
</script>
<div id="main_content">
<?php if ($this->session->flashdata('success')) {
                ?>
                <div class="alert success">
                    <span class="icon"></span><span class="hide">x</span><strong>Success</strong>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php } elseif ($this->session->flashdata('error')) { ?>
                <div class="alert error">
                    <span class="icon"></span><span class="hide">x</span><strong>Error</strong>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>
    <h2 class="grid_12">Merchants Update Requests List
        
    </h2>
    <div class="clean"></div>
    <div class="grid_12">
        <div class="box">

            <div class="header">
                <img src="<?php echo base_url(); ?>template/admin/img/icons/packs/fugue/16x16/shadeless/block.png" width="16" height="16">
                <h3>Merchants Update Requests List</h3>
                
            </div>

            <div class="content">
             <form name="frmproducts" id="frmproducts" method="post">
                <table id="table-example" class="table">
                    <thead>
                        <tr>
                            
                            <th>
                                Sr.No.
                            </th>
                            <th>
                                Name
                            </th>
                           
                                                    

                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $x = 1;
                        foreach ($merchants as $customer) {
                            ?>
                            <tr class="gradeX" id="recordsArray" >
                                
                                <td>
                                    <?php echo $x++; ?>
                                </td>
                                <td>
                                    <?php echo $customer['merchant_name']; ?>
                                </td>
								
								
                                        
                                <td class="center">
                                    <a href="<?php echo site_url('admin/merchants/approveupdate/'.$customer['id']."/".$customer['merchant_id']);?>">
                                    Approve
                                    </a>
                                   
                                    
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div> <!-- End of .content -->
        </div>
    </div>
</div> <!-- End of #main_content -->


<script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery-fallr-1.2.js"></script>
<script>
        

                $('.confirm').click(function() {
                    var baseurl = "<?php echo base_url();?>";
                    var id = $(this).data('cid');
                    var clicked = function(){
                        $.fallr('hide');
                        window.location.href = baseurl+'admin/merchants/delete/'+id;
                        
                    };

                        $.fallr('show', {
                                buttons : {
                                        button1 : {text: 'Yes', danger: true, onclick: clicked},
                                        button2 : {text: 'Cancel', onclick: function(){$.fallr('hide')}}
                                },
                                content : '<p>Are you sure you want to delete Merchants?</p>',
                                icon    : 'error'
                        });
                });


        
</script>