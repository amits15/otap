<script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.dataTables.js"></script>
<script>
    $(document).ready(function () {
        var $dialog = $("#view_dialog").dialog(
                {
                    autoOpen: false,
                    title: 'Merchant Contacts',
                    width: 400,
                    resizable: true,
                    modal: true,
                    buttons:
                    {
                        "Cancel": function()
                        {
                            $(this).dialog("close");
                        }
                    }
                });
          $(".view_dialog").click(function()
            {
                       
                $dialog.load($(this).attr('href'), function ()
                        {
                            $dialog.dialog('open');
                        });
                return false;
            });
            
            
       var $dialog1 = $("#view_dialog").dialog(
            {
                autoOpen: false,
                title: 'Merchant Contacts',
                width: 400,
                resizable: true,
                modal: true,
                buttons:
                {
                    "Cancel": function()
                    {
                        $(this).dialog("close");
                    }
                }
            });
            $(".edit_link").click(function()
            {
                $dialog1.load($(this).attr('href'), function ()
                        {
                            $dialog1.dialog('open');
                        });
                return false;
            });
        $('.checkall').click(function () {
                if ($(this).attr('checked')=='checked')
                {   
                    $('input:checkbox.chkbox').removeAttr('checked');
                }
                else
                {
                   $('input:checkbox.chkbox').attr('checked', 'checked');
                }
        });

         
        $(".status").click(function(){
                        var id=$(this).val();
                        if($(this).is(":unchecked")) {
                        // checkbox is checked -> do something
                                $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: baseURL+"admin/merchants/publishcontact/"+id,
                                        success: function(response) {
                                        // success function is called when data came back
                                        // for example: get your content and display it on your site
                                        
                                        }
                                });
                        }
                        else{
                                // checkbox is not checked -> do something different
                                $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: baseURL+"admin/merchants/unpublishcontact/"+id,
                                        success: function(response) {
                                        // success function is called when data came back
                                        // for example: get your content and display it on your site
                                       
                                        }
                                });
                        }
                });
        
        $('#table-example').dataTable({
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': [0,5,6]}
            ]
        });
        
    });
</script>

<div id="main_content">
<?php if ($this->session->flashdata('success')) {
                ?>
                <div class="alert success">
                    <span class="icon"></span><span class="hide">x</span><strong>Success</strong>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php } elseif ($this->session->flashdata('error')) { ?>
                <div class="alert error">
                    <span class="icon"></span><span class="hide">x</span><strong>Error</strong>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>
    <h2 class="grid_12">Merchant Contacts List
        <div style="float:right;">
            <a href="<?php echo site_url('admin/merchants/addcontact/'.$this->uri->segment(4)); ?>" class='view_dialog'>Add Merchant Contact</a>
                 |    <a href="<?php echo site_url('admin/merchants'); ?>">Back</a>
            
        </div>
    </h2>
    <div class="clean"></div>
    <div class="grid_12">

    <div id="view_dialog"></div>
        <div class="box">

            <div class="header">
                <img src="<?php echo base_url(); ?>template/admin/img/icons/packs/fugue/16x16/shadeless/block.png" width="16" height="16">
                <h3>Merchants Contacts List</h3>
                
            </div>

            <div class="content">
             <form name="frmproducts" id="frmproducts" method="post">
                <table id="table-example" class="table">
                    <thead>
                        <tr>
                            
                            <th>
                                Sr.No.
                            </th>
                            <th>
                                Merchant Name
                            </th>
                            <th>
                                Employee Name
                            </th>
                            <th>
                                Employee Index
                            </th>
                            <th>
                                Mobile No.
                            </th>
					         <th>
                                Publish
                            </th>
                            <th>
                                Actions
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $x = 1;
                        foreach ($contacts as $contact) {
                            ?>
                            <tr class="gradeX" id="recordsArray" >
                                
                                <td>
                                    <?php echo $x++; ?>
                                </td>
                                <td>
                                    <?php echo $contact['merchant_name']; ?>
                                </td>
								<td>
                                    <?php echo $contact['employeename']; ?>
                                </td>
                                <td>
                                    <?php echo $contact['cindex']; ?>
                                </td>
								
								<td>
                                    <?php echo $contact['contact']; ?>
                                </td>
								
                                <td class="center">
                        <label for="newsletter">
                                <input name="newsletter" type="checkbox" class="status"
                                id="<?php echo  $contact['id'];?>" value='<?php echo  $contact['id'];?>'
                                 <?php if($contact['status']=='1'){?>checked<?php }?> />
                     </label>
                </td>
								
                                 
                                <td class="center">
                                    <a title="Edit Item" href="<?php echo site_url('admin/merchants/editcontact/'.$this->uri->segment(4)."/".$contact['id']); ?>" class='edit_link'>
                                        <img src='<?php echo base_url(); ?>template/admin/img/icons/packs/diagona/16x16/019.png' />
                                    </a>
                                    &nbsp; | &nbsp;
                                <a data-cid="<?php echo $contact['id'];?>" class="delete tooltip confirm" href='javascript:' title="Delete Item">
                                    <img src='<?php echo base_url(); ?>template/admin/img/icons/packs/diagona/16x16/101.png'/>
                                </a>
                                    
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                
            </div> <!-- End of .content -->
        </div>
    </div>
</div> <!-- End of #main_content -->


<script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery-fallr-1.2.js"></script>
<script>
        

                $('.confirm').click(function() {
                    var baseurl = "<?php echo base_url();?>";
                    var id = $(this).data('cid');
                    var merchantid = "<?php echo $this->uri->segment(4);?>"
                    var clicked = function(){
                        $.fallr('hide');
                        window.location.href = baseurl+'admin/merchants/deletecontact/'+id+'/'+merchantid;
                        
                    };

                        $.fallr('show', {
                                buttons : {
                                        button1 : {text: 'Yes', danger: true, onclick: clicked},
                                        button2 : {text: 'Cancel', onclick: function(){$.fallr('hide')}}
                                },
                                content : '<p>Are you sure you want to delete Employee?</p>',
                                icon    : 'error'
                        });
                });


        
</script>