<!-- <style>
    form {
        margin-left:2%;
        margin-right:2%;
        width: 150%;
    }

</style> -->
<script type="text/javascript" src="<?php echo base_url(); ?>template/admin/ckeditor/ckeditor.js"></script>
<div id="main_content">
    <h2 class="grid_12">Home
        <div style="float:right;">
            <a href="<?php echo site_url('admin/dashboard') ?>"><font size=2>Back To Dashboard</font></a>
        </div></h2>
    <div class="clean"></div>
    <div class="grid_12">
        <div class="box">
            <div class="header" style="width: 150%;margin-left:2%;margin-right:2%;">
                <img src="<?php echo base_url(); ?>template/admin/img/icons/packs/fugue/16x16/document-word-text.png">
                <h3>Text Editor</h3>
            </div>
            <form id="frmedit" method="post" class="validate" action="<?php echo site_url('admin/home'); ?>" enctype="multipart/form-data">
                <h3>Databases</h3>
                <div class="content no-padding">

                    <div>
                        <input name="txttitle1" id="txttitle1" type="text" placeholder="Title" class="required" value="<?php echo $description[0]['title1']; ?>" />
                    </div>
                </div>
                <div class="content no-padding">
                    <div>
                        <textarea name="tadescription1" id="tadescription1"  class="ckeditor" style="width:50%;resize:none;">
                            <?php echo $description[0]['db_cms'];
                            echo set_value('tadescription1');
                            ?>
                        </textarea>
                        <label class="error"><?php echo form_error('tadescription1'); ?></label>
                    </div>

                </div>
                <h3>Reports</h3>
                <div class="content no-padding">

                    <div>
                        <input name="txttitle2" id="txttitle2" type="text" placeholder="Title" class="required" value="<?php echo $description[0]['title2']; ?>"/>
                    </div>
                </div>
                <div class="content no-padding">
                    <div>
                        <textarea name="tadescription2" id="tadescription2"  class="ckeditor" style="width:50%;resize:none;">
                            <?php echo $description[0]['report_cms'];
                            echo set_value('tadescription2');
                            ?>
                        </textarea>
                        <label class="error"><?php echo form_error('tadescription2'); ?></label>
                    </div>

                </div>
                <div class="actions">
                    <div class="actions-right">
                        <input type="submit" id="submit" name='submit' value="Update Content"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

