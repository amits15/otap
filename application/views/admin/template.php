<!doctype html>
<!--
paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <!-- DNS prefetch -->
        <link rel=dns-prefetch href="//fonts.googleapis.com">
        <!-- Use the .htaccess and remove these lines to avoid edge case issues.
        More info: h5bp.com/b/378 -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title; ?></title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Mobile viewport optimized: j.mp/bplateviewport -->
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory:
        mathiasbynens.be/notes/touch-icons -->

        <!-- CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/960gs/fluid.css"> <!-- 960.gs Grid System -->
        <!-- The HTML5-Boilerplate CSS styling -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/h5bp/normalize.css"> <!-- RECOMMENDED: H5BP reset styles -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/h5bp/non-semantic.helper.classes.css"> <!-- RECOMMENDED: H5BP helpers (e.g. .clear or .hidden) -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/h5bp/print.styles.css"> <!-- OPTIONAL: H5BP print styles -->
        <!-- The main styling -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/sprites.css"> <!-- STRONGLY RECOMMENDED: Basic sprites (e.g. buttons, jGrowl) -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/header.css"> <!-- REQUIRED: Header styling -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/navigation.css"> <!-- REQUIRED: Navigation styling -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/sidebar.css"> <!-- OPTIONAL: Sidebar -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/content.css"> <!-- REQUIRED: Content styling -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/footer.css"> <!-- REQUIRED: Footer styling -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/typographics.css"> <!-- REQUIRED: Typographics -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/ie.fixes.css"> <!-- OPTIONAL: Fixes for IE7 -->
        <!-- Sprites styling -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/sprite.forms.css"> <!-- SPRITE: Forms styling -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/sprite.lists.css"> <!-- SPRITE: Lists styling -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/sprite.gallery.css"> <!-- SPRITES: Gallery styling -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/icons.css"> <!-- Icons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/sprite.tables.css"> <!-- SPRITE: Tables styling -->
        <!-- Plugin styling -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/plugin.wysiwyg.css"> <!-- PLUGIN: Wysiwyg Editor -->
        <!-- Styling of JS plugins -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/external/jquery-ui-1.8.16.custom.css">	<!-- PLUGIN: jQuery UI styling -->
        <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/css/sidebar.css"> <!-- TODO: Remove this line! -->
        <!-- All JavaScript at the bottom, except this Modernizr build incl. Respond.js
        Respond is a polyfill for min/max-width media queries. Modernizr enables HTML5
        elements & feature detects;
        for optimal performance, create your own custom Modernizr build:
        www.modernizr.com/download/ -->
        <script src="<?php echo base_url(); ?>template/admin/js/libs/modernizr-2.0.6.min.js"></script>
        <script type="text/javascript">
            var baseURL = "<?php echo base_url(); ?>";
            var subcat_id = "<?php echo $this->uri->segment(5, 0); ?>";
        </script>
        <style>
            .dialogFixed{
                position:fixed !important;
            }
            .ui-autocomplete {
                max-height: 200px;
                overflow-y: auto;
                /* prevent horizontal scrollbar */
                overflow-x: hidden;
                /* add padding to account for vertical scrollbar */

            }
            a#toTop {
                /* float: right; */
                position: fixed;
                right: 10px;
                bottom: 20px;
            }

        </style>
    </head>

    <body>
        <!-- Begin of #height-wrapper -->
        <div id="height-wrapper">
            <?php $this->load->view('admin/header'); ?>
            <!-- Start of the #content-wrapper -->
            <div role="main" class="container_12" id="content-wrapper">

                <?php //$this->load->view('admin/sidebar'); ?>
                <?php $this->load->view($middle); ?>

                <div class="push clear"></div>
            </div>
            <!-- <div>
                <a style="display: block;" id="toTop">
                    <img src="<?php echo site_url(); ?>template/site/img/top.png" alt="Go To Top" title="Go To Top"></a>
            </div> -->
            <!-- End of #content-wrapper -->
            <div class="clear"></div>
            <div class="push"></div> <!-- BUGFIX if problems with sidebar in Chrome -->
        </div> <!-- End of #height-wrapper -->


<?php $this->load->view('admin/footer'); ?>
        <!--<script type="text/javascript" src="<?php echo site_url(); ?>template/site/js/top.js"></script>-->
        <script>
    $(window).scroll(function () {
        if ($(document).scrollTop() > 0)
        {

            {
                $('#header_toolbar').addClass('fixed');
                // alert("Here");
                $('#header_toolbar').stop().animate({
                    height: '20px'
                }, 600);
            }
        }
        else
        {
            {
                $('#header_toolbar').removeClass('fixed');

                $('#header_toolbar').stop().animate({
                    height: '41px'
                }, 600);
            }
        }
    });
        </script>

        <!-- scripts concatenated and minified via build script -->
        <script defer src="<?php echo base_url(); ?>template/admin/js/plugins.js"></script> <!-- REQUIRED: Different own jQuery plugnis -->
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.ba-resize.js"></script> <!-- RECOMMENDED when using sidebar: page resizing -->
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.easing.1.3.js"></script> <!-- RECOMMENDED: box animations -->
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.ui.touch-punch.js"></script> <!-- RECOMMENDED: touch compatibility -->
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.chosen.js"></script>
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.validate.js"></script>
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.jgrowl.js"></script>
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.checkbox.js"></script>
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.fileinput.js"></script> <!-- Needed for <input type=file> -->
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.placeholder.js"></script>
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.miniColors.js"></script> <!-- Needed for <input type=color> -->
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.text-overflow.js"></script> <!-- Needed for <input type=file> -->
        <script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.prettyPhoto.js"></script>
        <!--<script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.dataTables.rowReordering.js"></script>-->
        <script defer src="<?php echo base_url(); ?>template/admin/js/script.js"></script> <!-- REQUIRED: Generic scripts -->

        <!-- end scripts -->
        <script>
            $(window).load(function () {
                $('#accordion').accordion();

            });

        </script>
        <script type="text/javascript">

            $(document).ready(function () {
                $('a.view-answer').live('click', function () {

                    var id = $(this).attr('id');

                    var target = $('#main_content');
                    $("#dialog_" + id).dialog({
                        dialogClass: 'dialogFixed',
                        width: 400,
                    });
                });

            });
        </script>

        <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to
        support IE 6.
        chromium.org/developers/how-tos/chrome-frame-getting-started -->
        <!--[if lt IE 7 ]>
        <script defer
        src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
        <script
        defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
        <![endif]-->
    </body>
</html>
