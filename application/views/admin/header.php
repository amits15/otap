<!-- Begin of header -->		
<header>
    <!-- Begin of the header toolbar -->
    <div id="header_toolbar">
        <div class="container_12">
            <h1 class="grid_8">Otap | Admin Panel</h1>
            <!-- Start of right part -->
            <div class="grid_4">


                <!-- A large toolbar button -->
                <div class="toolbar_large">
                    <div class="toolbutton">
                        <div class="toolicon">
                            <img src="<?php echo base_url(); ?>template/admin/img/icons/16x16/user.png" width="16" height="16" alt="user" >
                        </div>
                        <div class="toolmenu">
                            <div class="toolcaption">
                                <span><?php echo ucfirst($this->session->userdata('username')); ?></span>
                            </div>
                            <!-- Start of menu -->
                            <div class="dropdown" style="width:63px;">
                                <ul>
                                    <li>
                                        <a href="<?php echo site_url('admin/settings'); ?>">Settings</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('admin/logout'); ?>">Logout</a>
                                    </li>
                                </ul>
                            </div> <!-- End of menu -->
                        </div>
                    </div>
                </div> 
                <!-- End of large toolbar button -->
            </div>
            <!-- End of right part -->
        </div>
    </div>
    <!-- End of the header toolbar -->

    <!-- Start of the main header bar -->
    <nav id="header_main">
        <div class="container_12">

            <!-- Start of the main navigation -->
            <ul id="nav_main">

                <li class="<?php if($tab == 'dashboard'){echo 'current';}?>">
    <a href="<?php echo site_url('admin/dashboard');?>">
        <img src="<?php echo base_url();?>template/admin/img/icons/25x25/dark/presentation.png" width=25 height=25 alt="">
        Dashboard
    </a>
    
</li>
<li class="<?php if($tab == 'merchants'){echo 'current';}?>">
    <a href="#">
        <img src="<?php echo base_url();?>template/admin/img/icons/25x25/dark/group.png" width=25 height=25 alt="">
        Merchants
    </a>
    <ul>
    <li class="<?php if($tab == 'merchants' && $tab1==''){echo 'current';}?>">
            <a href="<?php echo site_url('admin/merchants/');?>"><font size="3">Merchants</font></a>
        </li>
        <li class="<?php if($tab1 == 'category'){echo 'current';}?>">
            <a href="<?php echo site_url('admin/merchants/category');?>"><font size="3">Merchants Categories</font></a>
        </li>
         <li class="<?php if($tab1 == 'requests'){echo 'current';}?>">
            <a href="<?php echo site_url('admin/merchants/requests');?>"><font size="3">Update Requests</font></a>
        </li>
        </ul>
</li>
<li class="<?php if($tab == 'users'){echo 'current';}?>">
    <a href="<?php echo site_url('admin/users');?>">
        <img src="<?php echo base_url();?>template/admin/img/icons/25x25/dark/user.png" width=25 height=25 alt="">
        Users
    </a>
   
</li>
<li class="<?php if($tab == 'offers' || $tab=='offertype' || $tab == 'createrequests' || $tab=='updaterequests' || $tab=='deleterequests'){echo 'current';}?>">
    <a href="#">
        <img src="<?php echo base_url();?>template/admin/img/icons/25x25/dark/shopping-cart-2.png" width=25 height=25 alt="">
        Offers
    </a>
    <ul>
    <li class="<?php if($tab == 'offers' && $tab1==''){echo 'current';}?>">
            <a href="<?php echo site_url('admin/offers/');?>"><font size="3">Offers</font></a>
        </li>
        <li class="<?php if($tab == 'offertype'){echo 'current';}?>">
            <a href="<?php echo site_url('admin/offers/offertype');?>"><font size="3">Offer Types</font></a>
        </li>
         <li class="<?php if($tab == 'createrequests'){echo 'current';}?>">
            <a href="<?php echo site_url('admin/offers/createrequests');?>"><font size="3">Create Offers Requests</font></a>
        </li>
         <li class="<?php if($tab == 'updaterequests'){echo 'current';}?>">
            <a href="<?php echo site_url('admin/offers/updaterequests');?>"><font size="3">Update Offers Requests</font></a>
        </li>
         <li class="<?php if($tab == 'deleterequests'){echo 'current';}?>">
            <a href="<?php echo site_url('admin/offers/deleterequests');?>"><font size="3">Delete Offers Requests</font></a>
        </li>
        </ul>
</li>

            </ul>
            <!-- End of the main navigation -->
        </div>
    </nav>
    <div id="nav_sub"></div>
</header>
<!-- JavaScript at the bottom for fast page loading -->
<!-- Grab Google CDN's jQuery + jQueryUI, with a protocol relative URL; fall back to local -->		
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
<script>window.jQuery || document.write('<script src="template/admin/js/libs/jquery-1.7.1.js"><\/script>')</script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script>window.jQuery.ui || document.write('<script src="template/admin/js/libs/jquery-ui-1.8.16.min.js"><\/script>')</script>
