<script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery.dataTables.js"></script>
<script>
    $(document).ready(function () {
        
        
        $('#table-example').dataTable({
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': [5]}
            ]
        });
        
    });
</script>

<div id="main_content">
<?php if ($this->session->flashdata('success')) {
                ?>
                <div class="alert success">
                    <span class="icon"></span><span class="hide">x</span><strong>Success</strong>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php } elseif ($this->session->flashdata('error')) { ?>
                <div class="alert error">
                    <span class="icon"></span><span class="hide">x</span><strong>Error</strong>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
            <?php } ?>
    <h2 class="grid_12">Users List
        
    </h2>
    <div class="clean"></div>
    <div class="grid_12">
        <div class="box">

            <div class="header">
                <img src="<?php echo base_url(); ?>template/admin/img/icons/packs/fugue/16x16/shadeless/user.png" width="16" height="16">
                <h3>Users List</h3>
                
            </div>

            <div class="content">
                <table id="table-example" class="table">
                    <thead>
                        <tr>
                            
                            <th>
                                Sr. No.
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Username
                            </th>
                             <th>
                                Email id
							</th>
							 <th>
                                Mobile No.
							</th>
                            <th>Address</th>
							 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $x = 1;
                        foreach ($users as $user) {
                            ?>
                            <tr class="gradeX" id="recordsArray" >
                                
                                <td>
                                    <?php echo $x++; ?>
                                </td>
                                <td>
                                    <?php echo $user['name']; ?>
                                </td>
								<td>
									<?php echo $user['username']; ?>
								</td>
								<td>
									<?php echo $user['email']; ?>
								</td>
								<td>
									<?php echo $user['contact']; ?>
								</td>
								
                                 <td>
                                <a href="#" class="view-answer" id="<?php echo $user['id'];?>">View</a>
                                                                <div class="dialog" id="dialog_<?php echo $user['id'];?>" title="<?php echo $user['name']; ?>"  style="display:none;">
                                                                

                       
                    <p><?php echo $user['address'];?></p>
                    </div>
                                                                
                                                                                                                        </td>                               
                                                                                                                        
                                
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <?php //echo form_button(array('name'=>'mysubmit','id'=>'mysubmit'), 'Delete');?>
            </div> <!-- End of .content -->
        </div>
    </div>
</div> <!-- End of #main_content -->


<script defer src="<?php echo base_url(); ?>template/admin/js/mylibs/jquery-fallr-1.2.js"></script>
