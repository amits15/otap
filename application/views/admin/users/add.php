
<!-- Start of the main content -->
<div id="main_content">
<?php if ($error) { ?>
                <div class="alert error">
                    <span></span><span class="hide">x</span>
                    <?php echo $error; ?>
                </div>
            <?php } ?>
<h2 class="grid_12">Add User<div style="float:right;">
                    <a href="<?php echo site_url('admin/users/') ?>">Back</a>
                </div></h2>
    <div class="clean"></div>
       <div class="grid_12">

            <div class="box">
                <div class="header">
                    <img src="<?php echo base_url(); ?>template/admin/img/icons/packs/fugue/16x16/user--plus.png" alt="" width="16"
                         height="16">
                    <h3>Add User</h3>
                    <span></span>
                </div>
                <form id="frm" method="post" class="validate" action="<?php echo site_url('admin/users/add/');?>" enctype="multipart/form-data">

                <div class="content no-padding">
                    <div class="section _100">
                        <label>
                            Name
                        </label>
                        <div>
                <input name="txtname" id="txtname" type="text" placeholder="Name" class="required" />
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            UserName
                        </label>
                        <div>
                <input name="username" id="username" type="text" placeholder="UserName" class="required" />
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                        Password                        </label>
                        <div>
                <input name="password" id="password" type="password" placeholder="Password" class="required"  />
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                             Email
                        </label>
                        <div>
                <input name="email" id="email" type="email" placeholder="Email" class="required" />
                           
                        </div>
                    </div>

                    <div class="section _100">
                        <label>
                            Mobile No.
                        </label>
                        <div>
                <input name="telephone" id="telephone" type="text" placeholder="Mobile No." class="required" />
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Company
                        </label>
                        <div class="input select">
<select id="company" name="company" class="required">
                                <option value="">Select Company</option>
                                <?php foreach ($companies as $company) {
                                    
                                    ?>
                                    <option value="<?php echo $company['company_id'];?>"><?php echo $company['company_name'];?></option>
                                    <?php }?>
                                </select>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Division
                        </label>
                        <div class="input select">
<select class="chosen-select required" id="division" name="division" >
                                            <option value="">Select Division</option>
                                        </select>                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            User Type
                        </label>
                        <div class="input text required">
                <input name="txttype" id="txttype" type="text" placeholder="User Type"  />
                            <!--<label><?php echo form_error($field->name); ?></label>-->
                        </div>
                    </div>
                  
                </div>
                <div class="actions">
                    <div class="actions-right">
                        <input type="submit" id="submit" name="submit" value="Submit"/>
                    </div>
                </div>
                 </form>

            </div> <!-- End of .box -->
        </div> <!-- End of .grid_6 -->
</div>
<script type="text/javascript">
$('body').on('click','.add-txtupdate', function () {
     var a = $(".form-group").last().find('input').attr('id');
        //alert(a);
                var no = a;
                no = parseInt(no,10) + 1;
    var more_textbox = $('<input name="division[]" id="'+no+'" type="text" placeholder="Division name" />');
                more_textbox.hide();
                $(".more").after(more_textbox);
                more_textbox.fadeIn("slow");
                return false;
});
$('#company').change(function () { 
    var compny = $(this).val();
    $.ajax({
                type: "POST",
                dataType: "json",
                url: baseURL+"admin/company/getdivision/"+compny,
                success: function(response) {
                    //console.log(response);
                    var t = response[0].division;
                    var myArray = t.split(',');
                    //alert(myArray);
                    var txt = "<option>Select Division</option>";
    for(var i=0;i<myArray.length;i++){
        txt += "<option value="+myArray[i]+">"+myArray[i]+"</option>";
    }

    $('#division').html(txt);
    $('#division').trigger("liszt:updated");

                // success function is called when data came back
                // for example: get your content and display it on your site
                
                }
        });

});
</script>