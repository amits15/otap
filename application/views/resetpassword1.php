<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Reset Password</title>

	<link rel="stylesheet" href="<?php echo site_url()?>template/admin/css/demo.css">
	<link rel="stylesheet" href="<?php echo site_url()?>template/admin/css/form-basic.css">

</head>


	


    <div class="main-content">

        <!-- You only need this form and the form-basic.css -->

        <form class="form-basic" method="post" action="<?php echo site_url('Merchant/update_reset_password/'.$token)?>">

            <div class="form-title-row">
                <h1>Reset Password</h1>
            </div>

            <div class="form-row">
                <label>
                    <span>New Password</span>
                    <input type="password" name="password" required>
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Confirm Password</span>
                    <input type="password" name="confirmpassword" required>
                    <input type="hidden" value="<?php echo $merchant_id; ?>" name="txtHidUserId">
                </label>
            </div>

            

            


            

            <div class="form-row">
                <button type="submit">Submit Form</button>
            </div>

        </form>

    </div>

</body>

</html>
