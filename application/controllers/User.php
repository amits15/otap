<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home Class 
 *
 * @author Ketan Sangani
 * @package Codeigniter
 * @subpackage Controller
 *
 */

class User extends CI_Controller {
    
    
   /**
    * construct
    * 
    * constructor method (checks login status)
    * 
    * @author Ketan Sangani
    * @access public
    * @param none
    * @return void
    * 
    */
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
       
    }
    
   /**
    * index
    *
    * display content of home  page
    * 
    *@author Ketan Sangani
    *@access public
    *@param none 
    *@return void
    */
    public function index()
    {  

        $token = $this->uri->segment(3);
        if($this->User_model->check_token($token)){

            $url = base64_decode(urldecode($token));
            $url_chunks = explode('/',$url);
            $arr_data['user_id'] = $url_chunks['1'];
            
            $arr_data['token'] = $token;
            $this->load->view('resetpassword', $arr_data);
        }else{
            $this->session->set_flashdata('success', "The link you are trying to access has expired.");
          $arr_data['title'] = 'Failure';
           $this->load->view('success',$arr_data);
        }
    }
    /**
     * update_reset_password
     *
     * Resets password for for forgot password process
     *
     * @param void
     * @return void
     */
    public function update_reset_password(){
        $token = $this->uri->segment(3);
        $this->form_validation->set_rules('password', 'New Password', 'trim|required');
        $this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'trim|required|matches[password]');
        if ($this->form_validation->run() === TRUE){
            $data['password_hash'] = $this->input->post('password');
            $data['id'] = $this->input->post('txtHidUserId');
            $this->User_model->update_reset_password($data);
            $this->session->set_flashdata('success', "Your password has been changed.<br/> Please Login with your new password.");
            redirect('User/success');
        }
        if (validation_errors() !== ""){
            $this->session->set_flashdata('failure', validation_errors());
            redirect('User/index/'.$token);
        }

    }
     /**
    * success
    *
    * display content of home  page
    * 
    *@author Ketan Sangani
    *@access public
    *@param none 
    *@return void
    */
    public function success()
    {  

            $arr_data['title'] = 'Success';
            $this->load->view('success',$arr_data);
        }
    
}
/* End of file About_us.php */
/* Location: ./application/controllers/admin/About_us.php */
