<?php
/**
 * Ws Class 
 *
 * @author Ketan Sangani
 * @package Codeigniter
 * @subpackage Controller
 *
 */
class Ws extends CI_Controller {
	/**
	 * __construct
	 *
	 * Calls parent constructor
	 * @author	Ashok Jadhav
	 * @access	public
	 * @return	void
	 */
	function __construct()
	{
		// Initialization of class
		parent::__construct();
		
		 $this->load->library('PHPMailer');

	}

	 /**
     * call_ws
     *
     * Call webservice 
     * 
     * @author Ketan Sangani
     * @access public
     * @param none
     * @return void
     */
	
	public function call_ws() {
		
		$service = $_POST['webservice'];
		
		switch ($service){
			case "registeruser" :
				$this->registeruser($_POST);
				break;
			case "loginuser" :
				$this -> loginuser($_POST);
				break;
			case "registermerchant" :
				$this -> registermerchant($_POST);
				break;
			case "loginmerchant" :
				$this->loginmerchant($_POST);
				break;
			case "updateuser" :
				$this->updateuser($_POST);
				break;
			case "updatemerchant" :
				$this->updatemerchant($_POST);
				break;
			case "logout" :
				$this->logout();
				break;
			case "addoffer" :
				$this -> addoffer($_POST);
				break;
			case "editoffer" :
				$this -> editoffer($_POST);
				break;
			case "deleteoffer" :
				$this -> deleteoffer($_POST);
				break;
			case "verifypinuser" :
				$this -> verifypinuser($_POST);
				break;
			case "verifypinmerchant" :
				$this->verifypinmerchant($_POST);
				break;
			
			case "merchantprofile" :
				$this -> merchantprofile($_POST);
				break;
			case "offerslist" :
				$this -> offerslist($_POST);
				break;
			case "categories" :
				$this -> categories($_POST);
				break;
			case "userchangepassword" :
				$this -> userchangepassword($_POST);
				break;
			case "categorywisemerchants" :
				$this -> categorywisemerchants($_POST);
				break;
			case "changepassword" :
				$this -> merchantchangepassword($_POST);
				break;
			case "myofferslist" :
				$this -> myofferslist($_POST);
				break;
			case "offerdetails" :
				$this -> offerdetails($_POST);
				break;
			case "enablemerchantsmsnotification" :
				$this -> enablepushnotification($_POST);
				break;
			case "employees" :
				$this -> contacts($_POST);
				break;
			case "enablecontact" :
				$this -> enablecontact($_POST);
				break;
				case "makefavourite" :
				$this -> makefavourite($_POST);
				break;
				case "favourites" :
				$this -> favourites($_POST);
				break;
			case "merchantdetails" :
				$this -> merchantdetails($_POST);
				break;
			case "search" :
				$this -> searchmerchant($_POST);
				break;
				case "nearbymerchants" :
				$this -> nearbymerchants($_POST);
				break;
				case "registeruserdevice" :
				$this -> registeruserdevice($_POST);
				break;
				case "Updatesettings" :
				$this -> Updatesettings($_POST);
				break;
				case "unregisterDevice" :
				$this -> unregisterDevice($_POST);
				break;
				case "resendotp" :
				$this -> resendotp($_POST);
				break;
				case "forgotpassword" :
				$this -> forgot_password($_POST);
				break;
				case "forgotpasswordmerchant" :
				$this -> forgotpassword($_POST);
				break;
				case "getcategories" :
				$this -> getcategories();
				break;
				case "resendotpmerchant" :
				$this -> resendotpmerchant($_POST);
				break;
				case "registermerchantdevice" :
				$this -> registermerchantdevice($_POST);
				break;
				case "addemployee" :
				$this -> addemployee($_POST);
				break;
				case "deleteemployee" :
				$this -> deleteemployee($_POST);
				break;
				/*case "disablesms" :
				$this -> disablesms($_POST);
				break;*/
			default :
				echo "Sorry , You are trying to access undefined webservice.";
		}
	}
/**
     * registeruser
     *
     * Register user and saves the details of user in database
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function registeruser($param){

		$arrData['name'] = $param['name'];
        $arrData['username'] = $param['username'];
		$arrData['contact']  = $param['mobile'];
		$arrData['email']  = $param['email'];
        
		$arrData['created_date'] = $date = date("Y-m-d H:i:s");
		$arrData['password_hash']  = md5($param['password']);

		$arrData['created_date'] = $date = date("Y-m-d H:i:s");
		$this->load->library('form_validation');
       $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
		if ($arrData['name']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Your Name';
			echo json_encode($error);
			exit ;
		}
		else if ($arrData['username']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Your Username';
			echo json_encode($error);
			exit ;
		}else if($this->form_validation->run() == FALSE) {

           $error['isValid'] = false;
			$error['message'] = 'This Usename has already registered Please try Another';
			echo json_encode($error);
			exit ;
		}else if ($arrData['contact']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Mobile Number';
			echo json_encode($error);
			exit ;
		} 
		else if(!preg_match('/^[0-9]{10}+$/', $arrData['contact'])){
			$error['isValid'] = false;
            $error['message'] = 'Please enter Valid Mobile Number';
			echo json_encode($error);
			exit ;

		}
		else if ($arrData['email']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Email Id';
			echo json_encode($error);
			exit ;
		} 
		else if(filter_var($arrData['email'], FILTER_VALIDATE_EMAIL) === false){
			$error['isValid'] = false;
           $error['message'] = 'Please enter Valid Email';
			echo json_encode($error);
			exit ;

		}
		else if ($param['password']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Password';
			echo json_encode($error);
			exit ;
		}else if($param['confirmpassword']==''){
			$error['isValid'] = false;

			$error['message'] = 'Please renter your Password';
			echo json_encode($error);
			exit ;
		}else if($param['confirmpassword']!=$param['password']){
			$error['isValid'] = false;

			$error['message'] = 'Your Confirm Password is not match with Password';
			echo json_encode($error);
			exit ;
		}
		 $this->load-> model('User_model');
		$result1 = $this->User_model->getalreadyuserexist($arrData['email'],$arrData['contact']);
       
           if($result1 && $result1[0]['verified']==1){
           $error['isValid'] = false;
           $error['message'] = 'This Mobile Number Or Email Already Registered.';
		   echo json_encode($error);
		   exit ;

           }else if($result1 && $result1[0]['verified']==0){
           	$arrResponse = array();
                $OTP = 9999;   
            //$OTP = mt_rand(100000,999999);
            $this->load->helper('sms_helper');      
             $message = "Dear User,\nYour OTP is $OTP\nThank you.";
            //$response = send_otp($message,  $result1[0]['merchant_contact']);
             $this->db->where('id', $result1[0]['id']);
            $arrData['user_pin'] = $OTP;
            $update = $this->User_model->updateuser($arrData,$result1[0]['id']);
             
           if($update){
			$arrResponse['isValid'] = true;
			$arrResponse['isVerified'] = false;
			$arrResponse['message'] = 'OTP sends On Your Mobile Number please verify it';
            $arrResponse['info'] = array('userid' => $result1[0]['id'] ,'mobile'=>$result1[0]['contact']);
			
		       }

           }else{
           	$result=$this->User_model->registeruser($arrData);
         
         if ($result){
			$OTP = 9999;   
            //$OTP = mt_rand(100000,999999);
            $this->load->helper('sms_helper');      
            $message = "Dear User,\nYour OTP is $OTP\nThank you.";
            //$response = send_otp($message,  $param['mobile']);
    
                
                $this->db->where('id', $result);
                $update = $this->db->update('users', array('user_pin' => $OTP));
                if($update){
			$arrResponse['isValid'] = true;
			$arrResponse['isVerified'] = false;
			$arrResponse['message'] = 'OTP sends On Your Mobile Number please verify it';
            $arrResponse['info'] = array('userid' => $result ,'mobile'=>$arrData['contact']);
                 }
		}
           
      
	}

		echo json_encode($arrResponse);
	}
	
/**
     * resendotp
     *
     * resend otp on user's registered mobile number
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */

	function resendotp($param){
        

		$OTP = 8888;   
            //$OTP = mt_rand(100000,999999);
            $this->load->helper('sms_helper');      
            $message = "Dear User,\nYour OTP is $OTP\nThank you.";
            $response = send_otp($message,  $param['mobile']);
		$this->load-> model('user_model');
		$this->db->where('id', $param['userid']);
        $update = $this->db->update('users', array('user_pin' => $OTP));
		 if($update){
			$arrResponse['isValid'] = true;
			$arrResponse['message'] = 'OTP sends On Your Mobile Number please verify it';
            
                 }
                 else{
                  $arrResponse['isValid'] = false;
			$arrResponse['message'] = 'Failed To send OTP on Your Mobile NUmber!!';
                 }
                 echo json_encode($arrResponse);
		
	}
	/**
     * resendotpmerchant
     *
     * resend otp on merchants's registered mobile number
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */

	function resendotpmerchant($param){
        

		$OTP = 8888;   
            //$OTP = mt_rand(100000,999999);
            $this->load->helper('sms_helper');      
            $message = "Dear Merchant,\nYour OTP is $OTP\nThank you.";
            $response = send_otp($message,  $param['mobile']);
		
		$this->db->where('merchant_id', $param['merchantid']);
        $update = $this->db->update('merchants', array('merchant_pin' => $OTP));
		 if($update){
			$arrResponse['isValid'] = true;
			$arrResponse['message'] = 'OTP sends On Your Mobile Number please verify it';
            
                 }
                 else{
                  $arrResponse['isValid'] = false;
			$arrResponse['message'] = 'Failed To send OTP on Your Mobile NUmber!!';
                 }
                 echo json_encode($arrResponse);
		
	}
/**
     * loginuser
     *
     * check user login
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */

	function loginuser($param){
        

		if ($param['username']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Your Name';
			echo json_encode($error);
			exit ;
		} else if ($param['password']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Password';
			echo json_encode($error);
			exit ;
		}
		$this->load-> model('user_model');
		$result=$this->user_model->loginuser($param['username'],$param['password']);
		
	}
	/**
     * getuserprofile
     *
     * Get  the details of profile of user
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function getuserprofile($param){
           
        $userid = $param['userid'];
		$this->load-> model('user_model');
		$arrResponse['userdetails']=$this->user_model->getuserDetails($userid);
		
		
		echo json_encode($arrResponse);
	}
	/**
     * merchantprofile
     *
     * Get  the details of profile of merchant
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function merchantprofile($param){
           
         $userid = $param['merchantid'];
		$this->load-> model('Merchants_model');
		$arrResponse['merchantdetails']=$this->Merchants_model->getmerchantprofile($userid);
		echo json_encode($arrResponse);
	}
	/**
     * userchangepassword
     *
     * Change password of user
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function userchangepassword($param){
           
         $userid = $param['userid'];
         $this->load-> model('user_model');
		 $arrResponse['userdetails']=$this->user_model->getuserDetails($userid);

        if($param['oldpassword']==''){
        $error['isValid'] = false;
		$error['message'] = 'Please enter Your Old Password';
		echo json_encode($error);
		exit ;
          
        }elseif($arrResponse['userdetails'][0]['password_hash']!=md5($param['oldpassword'])){
            $error['isValid'] = false;
		$error['message'] = 'Please enter Correct Old Password';
		echo json_encode($error);
		exit ;

        }
        elseif($param['newpassword']==''){
        	 $error['isValid'] = false;
		$error['message'] = 'Please enter Your New Password';
		echo json_encode($error);
		exit ;
        

        }else{

           $UpdateData['password_hash'] = md5($param['newpassword']);
        }
        

		$UpdateData['modified_date'] = $date = date("Y-m-d H:i:s");

		$this->load-> model('user_model');
		$result=$this->user_model->updateuser($UpdateData,$userid);
		$arrResponse = array();
		if ($result) {
			$arrResponse['isValid'] = true;
			$arrResponse['message'] = 'You Have Changed Your Password Successfully';
		}
		echo json_encode($arrResponse);
	}
	/**
     * merchantchangepassword
     *
     * Change password of merchant
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function merchantchangepassword($param){
           
         $userid = $param['merchantid'];
         $this->load-> model('Merchants_model');
		$arrResponse['userdetails']=$this->Merchants_model->getmerchantDetails($userid);
        if($param['oldpassword']==''){
        $error['isValid'] = false;
		$error['message'] = 'Please enter Your Old Password';
		echo json_encode($error);
		exit ;
          
        }elseif($arrResponse['userdetails'][0]['password_hash']!=md5($param['oldpassword'])){
            $error['isValid'] = false;
		$error['message'] = 'Please enter Correct Old Password';
		echo json_encode($error);
		exit ;

        }
        elseif($param['newpassword']==''){
        	 $error['isValid'] = false;
		$error['message'] = 'Please enter Your New Password';
		echo json_encode($error);
		exit ;
        

        }else{

           $UpdateData['password_hash'] = md5($param['newpassword']);
        }
        
        
		$UpdateData['modified_date'] = $date = date("Y-m-d H:i:s");
		
		$this->load-> model('Merchants_model');
		$result=$this->Merchants_model->update_merchant($userid,$UpdateData);
		$arrResponse = array();
		if ($result) {
			$arrResponse['isValid'] = true;
			$arrResponse['message'] = 'You Have Changed Your Password Successfully';
		}
		echo json_encode($arrResponse);
	}
	/**
     * updateuser
     *
     * Update the details of user
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function updateuser($param){
           
         $userid = $param['userid'];
        if($param['name']==''){
        	 $error['isValid'] = false;
		$error['message'] = 'Please enter Your Name';
		echo json_encode($error);
		exit ;
          
        }
        else{
        $UpdateData['name'] = $param['name'];

        }
        
        if ($param['mobile']=='') {
        	$error['isValid'] = false;
			$error['message'] = 'Please enter Mobile Number';
			echo json_encode($error);
			exit ;
        	
        }
        
		else if(!preg_match('/^[0-9]{10}+$/', $param['mobile'])){
			$error['isValid'] = false;
            $error['message'] = 'Please enter Valid Mobile Number';
			echo json_encode($error);
			exit ;

		}else{
           $UpdateData['contact']  = $param['mobile'];
		}
        if($param['address']==''){
            $error['isValid'] = false;
            $error['message'] = 'Please enter Your Address';
            echo json_encode($error);
            exit ;

        }
        else{
            $UpdateData['address'] = $param['address'];

        }
		
		if(isset($_FILES['image'])){
			$data = $this->do_upload('image');
			$data['error'] = array();
			if (isset($data['error'])) {
                            
	            $error['isValid'] = false;
	            $error['message'] = $data['error'];
				echo json_encode($error);
				exit ;    
                            
			} else {
                            $UpdateData["profilepic"] = $data['file_name'];
            }
		}
		
	

		$UpdateData['modified_date'] = $date = date("Y-m-d H:i:s");
		$this->load-> model('user_model');
		$result=$this->user_model->updateuser($UpdateData,$userid);
		$arrResponse = array();
		if ($result) {
			$arrResponse['isValid'] = true;
			$arrResponse['message'] = 'You Have Updated Your Profile Successfully';
			$arrResponse['info'] = $this->user_model->getUserInfo($userid);

		}
		echo json_encode($arrResponse);
	}
	/**
		 * do_upload
		 *
		 * uploads the image files
		 * 
		 * @author Amit Salunkhe
		 * @access public
		 * @param String - Image container name
		 * @return array - Image data/Error
		 */
		function do_upload($fieldName) {
				$config['upload_path'] = 'uploads/users/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';

				$this->upload->initialize($config);
                 
				if (!$this->upload->do_upload($fieldName)) {
					    
						$error = array("error" => $this->upload->display_errors());
						return $error;
				} else {
						$data = $this->upload->data();
						return $data;
				}
		}
	/**
     * updatemerchant
     *
     * Update the details of merchant
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function updatemerchant($param){
           
         $UpdateData['merchant_id'] = $param['merchantid'];
        if($param['name']==''){
        	 $error['isValid'] = false;
		$error['message'] = 'Please enter Your Name';
		echo json_encode($error);
		exit ;
          
        }
        else{
        $UpdateData['merchant_name'] = $param['name'];

        }
        if ($param['mobile']==''){
        	$error['isValid'] = false;
			$error['message'] = 'Please enter Mobile Number';
			echo json_encode($error);
			exit ;
        	
        }
        
		else{
           $UpdateData['merchant_contact']  = $param['mobile'];
		}
		
		
		if ($param['address']=='') {
			$error['isValid'] = false;
            $error['message'] = 'Please enter Address Of Your Shop';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $UpdateData['address']  = $param['address'];
		}
		if ($param['email']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Email Id';
			echo json_encode($error);
			exit ;
		} 
		else if(filter_var($param['email'], FILTER_VALIDATE_EMAIL) === false){
			$error['isValid'] = false;
           $error['message'] = 'Please enter Valid Email';
			echo json_encode($error);
			exit ;

		}
		else{
           
           $UpdateData['merchant_email']  = $param['email'];
		}

			$UpdateData['created_date'] = $date = date("Y-m-d H:i:s");
		
		if(isset($_FILES['image'])){
			$data = $this->do_upload('image');
			$data['error'] = array();
			if ($data['error']) {
                            
	            $error['isValid'] = false;
	            $error['message'] = $data['error'];
				echo json_encode($error);
				exit ;    
                            
			} else {
                            $UpdateData["profilepic"] = $data['file_name'];
            }
		}
			
		
		$this->load-> model('Merchants_model');
		 
		$result=$this->Merchants_model->request_update($UpdateData);
		$arrResponse = array();
		if ($result) {
			$arrResponse['isValid'] = true;
			$arrResponse['message'] = 'Your Request For Updating Your Profile Send To Admin Successfully';
		}
		echo json_encode($arrResponse);
	}

/**
     * registermerchant
     *
     * Register merchant and saves the details of merchant in database
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function registermerchant($param){
		
        $arrData['merchant_name'] = $param['name'];
		$arrData['merchantshop_name']  = $param['shopname'];
		$arrData['merchant_email']  = $param['email'];
        $arrData['merchant_contact']  = $param['mobile'];
        $arrData['pincode']  = $param['zipcode'];
        $arrData['categories']  = $param['categories'];
        $arrData['address']  = $param['address'];

		$arrData['created_date'] = $date = date("Y-m-d H:i:s");
		$arrData['password_hash']  = md5($param['password']);

		$arrData['created_date'] = $date = date("Y-m-d H:i:s");
		if ($arrData['merchant_name']=='') {
			$error['isValid'] =false;
			$error['message'] = 'Please enter Your Name';
			echo json_encode($error);
			exit ;
		}else if ($arrData['merchantshop_name']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Your Shop Name';
			echo json_encode($error);
			exit ;
		}else if ($arrData['address']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Your Address';
			echo json_encode($error);
			exit ;
		}  else if ($arrData['merchant_contact']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Mobile Number';
			echo json_encode($error);
			exit ;
		} 
		else if(!preg_match('/^[0-9]{10}+$/', $arrData['merchant_contact'])) {
			$error['isValid'] = false;
            $error['message'] = 'Please enter Valid Mobile Number';
			echo json_encode($error);
			exit ;

		}else if ($arrData['merchant_email']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Email Id';
			echo json_encode($error);
			exit ;
		} 
		else if (filter_var($arrData['merchant_email'], FILTER_VALIDATE_EMAIL) === false){
			$error['isValid'] = false;
           $error['message'] = 'Please enter Valid Email';
			echo json_encode($error);
			exit ;
        }
        
		else if ($param['password']==''){
			$error['isValid'] = false;
			$error['message'] = 'Please enter Password';
			echo json_encode($error);
			exit ;
		}else if($param['confirmpassword']==''){
			$error['isValid'] = false;

			$error['message'] = 'Please renter your Password';
			echo json_encode($error);
			exit ;
		}else if($param['confirmpassword']!=$param['password']){
			$error['isValid'] = false;
            $error['message'] = 'Your Confirm Password is not match with Password';
			echo json_encode($error);
			exit ;
		}
		else if ($arrData['pincode']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter ZIP Code';
			echo json_encode($error);
			exit ;
		} 
		else if ($arrData['categories']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please Select Atleast One Category';
			echo json_encode($error);
			exit ;
		} 
           $this->load->model('Merchants_model');
		$result1 = $this->Merchants_model->getalreadymerchantexist($arrData['merchant_email'],$arrData['merchant_contact']);
        //var_dump($result1);exit;
           if($result1 && $result1[0]['verified']==1){
           $error['isValid'] = false;
           $error['message'] = 'This Mobile Number Or Email Already Registered.';
		   echo json_encode($error);
		   exit ;

           }else if($result1 && $result1[0]['verified']==0){
           	$arrResponse = array();
                $OTP = 9999;   
            //$OTP = mt_rand(100000,999999);
            $this->load->helper('sms_helper');      
            $message = "Dear Merchant,\nYour OTP is $OTP\nThank you.";
            //$response = send_otp($message,  $result1[0]['merchant_contact']);
            $this->db->where('merchant_id', $result1[0]['merchant_id']);
            $arrData['merchant_pin'] = $OTP;
            $update = $this->Merchants_model->update_merchant($result1[0]['merchant_id'],$arrData);
             
           if($update){
			$arrResponse['isValid'] = true;
			$arrResponse['isVerified'] = false;
			$arrResponse['message'] = 'OTP sends On Your Mobile Number please verify it';
            $arrResponse['info'] = array('merchantid' => $result1[0]['merchant_id'] ,'mobile'=>$arrData['merchant_contact']);
			
		       }

           }else{
           	$result=$this->Merchants_model->savemerchantDetails($arrData);
		
		
		if ($result) {
           $OTP = 9999;   
            //$OTP = mt_rand(100000,999999);
            $this->load->helper('sms_helper');      
            $message = "Dear Merchant,\nYour OTP is $OTP\nThank you.";
            //$response = send_otp($message,  $param['mobile']);
    
                
                $this->db->where('merchant_id', $result);
                $update = $this->db->update('merchants', array('merchant_pin' => $OTP));
                if($update){
			$arrResponse['isValid'] = true;
			$arrResponse['isVerified'] = false;
			$arrResponse['message'] = 'OTP sends On Your Mobile Number please verify it';
            $arrResponse['info'] = array('merchantid' => $result ,'mobile'=>$arrData['merchant_contact']);
			
		}

           }
      
	}
	echo json_encode($arrResponse);
	}
	/**
     * loginmerchant
     *
     * check merchant login
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */

	function loginmerchant($param){
        
       if ($param['email']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Your Email';
			echo json_encode($error);
			exit ;
		} else if ($param['password']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Password';
			echo json_encode($error);
			exit ;
		}
		$this->load-> model('Merchants_model');
		$result=$this->Merchants_model->loginmerchant($param['email'],$param['password']);
		
	}
	/**
     * addoffer
     *
     * Add offer by merchant
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function addoffer($param){

		   $arrData["merchant_id"] = $param['merchantid'];
           $arrData["created_date"] = date("Y-m-d H:i:s");
		if($param['title']==''){
        	 $error['isValid'] = false;
		$error['message'] = 'Please enter Title of Offer';
		echo json_encode($error);
		exit ;
          
        }
        else{
         $arrData["title"] = $param['title'];

        }
        if ($param['type']=='') {
        	$error['isValid'] = false;
			$error['message'] = 'Please Select Offer Type';
			echo json_encode($error);
			exit ;
        	
        }
        else{
           $arrData["type"] = $param['type'];
		}
		if ($param['description']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Description of Offer';
			echo json_encode($error);
			exit ;
			
		}
		else{
            $arrData["description"] = $param['description'];
		}
		if ($param['startdate']=='') {
			$error['isValid'] = false;

			$error['message'] = 'Please enter Start Date Of Offer';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["start_date"] = $param['startdate'];
		}
		
		if ($param['enddate']=='') {
			$error['isValid'] = false;

			$error['message'] = 'Please enter End Date Of Offer';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["end_date"] = $param['enddate'];
		}
if($param['type']==1){
if ($param['amount']==''){
			$error['isValid'] = false;

			$error['message'] = 'Please enter Amount Of Offer';
			echo json_encode($error);
			exit ;
			

		}else{
           
            $arrData["amount"] = $param['amount'];
		}
			if ($param['discount']=='') {
			$error['isValid'] = false;

			$error['message'] = 'Please enter Discount Of Offer';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["offer_discount"] = $param['discount'];
		} 
		if ($param['maxdiscountpertransaction']=='') {
			$error['isValid'] = false;

			$error['message'] = 'Please enter Maximum Discount Per Transaction ';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["maxdiscountpertransaction"] = $param['maxdiscountpertransaction'];
		} 
		if ($param['maxdiscountperuser']==''){
			$error['isValid'] = false;

			$error['message'] = 'Please enter Maximum Discount Per User';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["maxdiscountperuser"] = $param['maxdiscountperuser'];
		} 
	}elseif ($param['type']==2) {
		if ($param['ffapromotionaltext']=='') {
			$error['isValid'] = false;

			$error['message'] = 'Please enter Promotional Text';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["promotionaltext"] = $param['promotionaltext'];
		} 
		
	}
	$arrData["crudtype"] = 'add';      
		$this->load-> model('Offers_model');
		$result=$this->Offers_model->saveoffercache($arrData);
		$arrResponse = array();
		if ($result) {
			$arrResponse['isValid'] = true;
			$arrResponse['message'] = 'Your Request Of Create Offer Goes For Admin Approval';
		}
		echo json_encode($arrResponse);
	}
	/**
     * editoffer
     *
     * edit offer by merchant
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function editoffer($param){

		   $arrData['offer_id'] = $param['offerid'];
		   $arrData['merchant_id'] = $param['merchantid'];
           $arrData["modified_date"] = date("Y-m-d H:i:s");
		if($param['title']==''){
        	 $error['isValid'] = false;
		$error['message'] = 'Please enter Title of Offer';
		echo json_encode($error);
		exit ;
          
        }
        else{
         $arrData["title"] = $param['title'];

        }
        if ($param['type']=='') {
        	$error['isValid'] = false;
			$error['message'] = 'Please Select Offer Type';
			echo json_encode($error);
			exit ;
        	
        }
        else{
           $arrData["type"] = $param['type'];
		}
		if ($param['description']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Description of Offer';
			echo json_encode($error);
			exit ;
			
		}
		else{
            $arrData["description"] = $param['description'];
		}
		if ($param['startdate']=='') {
			$error['isValid'] = false;

			$error['message'] = 'Please enter Start Date Of Offer';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["start_date"] = $param['startdate'];
		}
		
		if ($param['enddate']=='') {
			$error['isValid'] = false;

			$error['message'] = 'Please enter End Date Of Offer';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["end_date"] = $param['enddate'];
		}
if($param['type']==1){
if ($param['amount']==''){
			$error['isValid'] = false;

			$error['message'] = 'Please enter Amount Of Offer';
			echo json_encode($error);
			exit ;
			

		}else{
           
            $arrData["amount"] = $param['amount'];
		}
			if ($param['discount']=='') {
			$error['isValid'] = false;

			$error['message'] = 'Please enter Discount Of Offer';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["offer_discount"] = $param['discount'];
		} 
		if($param['maxdiscountpertransaction']=='') {
			$error['isValid'] = false;

			$error['message'] = 'Please enter Maximum Discount Per Transaction ';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["maxdiscountpertransaction"] = $param['maxdiscountpertransaction'];
		} 
		if ($param['maxdiscountperuser']=='') {
			$error['isValid'] = false;

			$error['message'] = 'Please enter Maximum Discount Per User';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["maxdiscountperuser"] = $param['maxdiscountperuser'];
		} 
	}elseif ($param['type']==2) {
		if ($param['promotionaltext']=='') {
			$error['isValid'] = false;

			$error['message'] = 'Please enter Promotional Text';
			echo json_encode($error);
			exit ;
			

		}else{
           
           $arrData["promotionaltext"] = $param['promotionaltext'];
		} 
		
	}
	   $arrData['crudtype'] = 'edit';      
		$this->load-> model('Offers_model');
		$result=$this->Offers_model->saveoffercache($arrData);
		$arrResponse = array();
		if($result){
			$arrResponse['isValid'] = true;
			$arrResponse['message'] = 'You Request Of Update Offer Goes For Admin Approval';
		}
		echo json_encode($arrResponse);
	}
	/**
     * 	
     *
     * request of delete offer goes  to admin
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */

	function deleteoffer($param){
        
           $arrData['offer_id'] = $param['offerid'];
		   $arrData['merchant_id'] = $param['merchantid'];
		   $arrData['crudtype'] = 'delete';      
		   $this->load-> model('Offers_model');
		   $result=$this->Offers_model->saveoffercache($arrData);
		   $arrResponse = array();
			if($result){
				$arrResponse['isValid'] = true;
				$arrResponse['message'] = 'You Request Of Delete Offer Goes For Admin Approval';
			}
	       echo json_encode($arrResponse);
		
	}
		/**
     * verifypinuser
     *
     * Verify pin and login for user
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
function verifypinuser($param){
		$otp = $param['pin'];
		$userid = $param['userid'];
		$this ->load -> model('User_model');
		$this ->User_model->verify_otp($otp, $userid);
	}
		/**
     * verifypinmerchant
     *
     * Verify pin and login for merchant
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function verifypinmerchant($param){
		$otp = $param['pin'];
		$userid = $param['merchantid'];
		$this ->load-> model('Merchants_model');
		$this ->Merchants_model->verify_otp($otp, $userid);
	}
	/**
     * offerslist
     *
     * Retrieves list of offers
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function offerslist($param){
           
        $offset = $param['offset'];
        $this->load-> model('Merchants_model');
		$this->load-> model('Offers_model');
        $arr['merchants'] = $this->Merchants_model->getmerchants($offset);
        $arrResponse['totalcount'] = $this->Merchants_model->getmerchantcount();
        foreach( $arr['merchants'] as $merchant){
        	if($merchant['profilepic']){
        		$imageURL = "http://". $_SERVER['SERVER_NAME'] . "/otap/uploads/merchants/" . $merchant['profilepic'];
        	}else{
        		$imageURL = "";        		
        	}

        	$merchant['profilepic'] = $imageURL;

        	$favouriteexist = $this ->Merchants_model->favouritemerchantexist($param['userid'],$merchant['merchant_id']);

        	if($favouriteexist){
        		$merchant['isFav'] = 1;
        	}else{
        		$merchant['isFav'] = 0;
        	}
                
            $offercount =$this->Offers_model->gettotaloffersofmerchant($merchant['merchant_id']);
            $merchant['offercount'] = $offercount;
            $arrResponse['merchantsinfo'][] = $merchant;


        }
        

		echo json_encode($arrResponse);
	}	
	/**
     * myofferslist
     *
     * Retrieves list of offers for particular merchants
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function myofferslist($param){
           
        $merchantid = $param['merchantid'];
        $offset = $param['offset'];
		$this->load-> model('Offers_model');
		$arrResponse['offers']=$this->Offers_model->getofferslistofmerchant($merchantid,$offset);
		$offercount =$this->Offers_model->gettotaloffersofmerchant($merchantid);
		$arrResponse['totalcount'] = $offercount;
		echo json_encode($arrResponse);
	}
	/**
     * myofferslist
     *
     * Retrieves list of offers for particular merchants
     * 
     * @author Ketan Sangani
     * @access public
     * @param none
     * @return void
     */
	function offerdetails($param){
           
        $offerid = $param['offerid'];
		$this->load-> model('Offers_model');
		$arrResponse['offerdetails']=$this->Offers_model->getofferDetail($offerid);
		echo json_encode($arrResponse);
	}	
	/**
     * categories
     *
     * Retrieves list of categories of merchants
     * 
     * @author Ketan Sangani
     * @access public
     * @param none
     * @return void
     */
	function categories($param){
          $offset = $param['offset'];
        $this->load-> model('Merchants_model');
		$categories =$this->Merchants_model->getcategories($offset);
         $arrResponse['totalcount']=$this->Merchants_model->gettotalcategories();
		foreach ($categories as $key => $category) {
			$imageURL = "http://". $_SERVER['SERVER_NAME'] . "/otap/uploads/category/" . $category['category_image'];
			$category['category_image'] = $imageURL;
			$arrResponse['categories'][] = $category;
		}
		echo json_encode($arrResponse);
	}
	/**
     * getcategories
     *
     * Retrieves list of categories of merchants
     * 
     * @author Ketan Sangani
     * @access public
     * @param none
     * @return void
     */
	function getcategories(){
          
        $this->load-> model('Merchants_model');
		$arrResponse['categories'] =$this->Merchants_model->getcategoriesofmerchants();
        
		
			
		echo json_encode($arrResponse);
	}		
	/**
     * categorywisemerchants
     *
     * Retrieves list merchants for particular category
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function categorywisemerchants($param){

        $categoryid = $param['category_id'];  
       
        	$offset = $param['offset'];
       
        $this->load-> model('Merchants_model');
		$arrResponse['merchants']=$this->Merchants_model->getmerchantsforcategory($categoryid,$offset);
    $arrResponse['totalcount'] = $this->Merchants_model->gettotalmerchantsforcategory($categoryid);
		echo json_encode($arrResponse);
	}
	/**
     * contacts
     *
     * Retrieves list of employees for particular merchant
     * 
     * @author Ketan Sangani
     * @access public
     * @param none
     * @return void
     */
	function contacts($param){
           
        $merchantid = $param['merchantid'];
        $offset = $param['offset'];
		$this->load-> model('Merchants_model');
		$arrResponse['employees']=$this->Merchants_model->getmerchantcontacts($merchantid,$offset);
		$arrResponse['totalcount']=$this->Merchants_model->gettotalcountmerchantcontacts($merchantid);
		echo json_encode($arrResponse);
	}
		/**
     * addcontact
     *
     * add mobile numbers of merchants
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function addcontact($param){
		$arrData['contact']  = $param['mobile'];
		$arrData['merchant_id'] = $param['merchantid'];
		$this->load->library('form_validation');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required|is_unique[merchants_contacts.contact]');
		if ($arrData['contact']==''){
			$error['isValid'] = false;
			$error['message'] = 'Please enter Mobile Number';
			echo json_encode($error);
			exit ;
		} 
		else if(!preg_match('/^[0-9]{10}+$/', $arrData['contact'])) {
			$error['isValid'] = false;
            $error['message'] = 'Please enter Valid Mobile Number';
			echo json_encode($error);
			exit ;

		}elseif($this->form_validation->run() == FALSE){
           $error['isValid'] = false;
           $error['message'] = 'This Mobile Number Already Registered.Please Enter Other Mobile Number';
			echo json_encode($error);
			exit ;
		}
		$this ->load-> model('Merchants_model');
		$result = $this ->Merchants_model->savemerchantcontactDetails($arrData);
		$arrResponse = array();
		if ($result){
			$arrResponse['isValid'] = true;
			$arrResponse['message'] = 'You Have Added Contact Succesfully';
		}
		echo json_encode($arrResponse);
	}
	
	/**
     * makefavourite
     *
     * Make merchant favourite or remove from list of favourite
     * 
     * @author Ketan Sangani
     * @access public
     * @param $param-array
     * @return void
     */
	function makefavourite($param){
           
        $arrData['userid'] = $param['userid'];
        $arrData['merchant_id'] = $param['merchantid'];
        $UpdateData['isfav'] = $arrData['isfav'] = $param['isfav'];
		$this->load-> model('Merchants_model');
		$arrResponse = array();
        if ($param['userid'] == "") {
        	$error['isValid'] = false;
			$error['message'] = 'Please enter User Id';
			echo json_encode($error);
			exit ;
		} else if ($param['merchantid']== '') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Merchant Id';
			echo json_encode($error);
			exit ;
		}
		$favouriteexist = $this ->Merchants_model->favouritemerchantexist($arrData['userid'],$arrData['merchant_id']);
	    if($favouriteexist){
	    	$UpdateData['modified_date']= date("Y-m-d H:i:s");

         $result1 = $this ->Merchants_model->updatemerchantfavourite($arrData['userid'],$arrData['merchant_id'],$UpdateData);
         if($result1){
         	if($param['isfav']==1){
			$arrResponse['isValid'] = true;
			$arrResponse['isfav'] = 1;
			$arrResponse['message'] = 'Merchant Added to favourites';
		}else{

            $arrResponse['isValid'] = true;
            $arrResponse['isfav'] = 0;
			$arrResponse['message'] = 'Merchant Removed from favourites';
		}
		}
	    }else{
	    	$arrData['created_date']= date("Y-m-d H:i:s");
		$result = $this ->Merchants_model->addmerchanttofavourite($arrData);
		if($result){
			if($param['isfav']==1){
			$arrResponse['isValid'] = true;
			$arrResponse['isfav'] = 1;
			$arrResponse['message'] = 'Merchant Added to favourites';
		}else{
             $arrResponse['isValid'] = true;
             $arrResponse['isfav'] = 0;
			$arrResponse['message'] = 'Merchant Removed from favourites';
		}
		}
	}
		echo json_encode($arrResponse);
	}
	/**
     * favourites
     *
     * Retrieves list of favourites merchants of user
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function favourites($param){
           
        $userid = $param['userid'];
        $offset = $param['offset'];
		$this->load-> model('Merchants_model');
		$arr['merchants'] =$this->Merchants_model->getfavouritesmerchants($userid,$offset);
		$arrResponse['merchants'] = array();
	    $arrResponse['totalcount'] =$this->Merchants_model->gettotalfavouritesmerchants($userid);
		foreach($arr['merchants'] as $merchant){
        	if($merchant['profilepic']){
        		$imageURL = "http://". $_SERVER['SERVER_NAME'] . "/otap/uploads/merchants/" . $merchant['profilepic'];
        	}else{
        		$imageURL = "";        		
        	}

        	$merchant['image'] = $imageURL;

        	$arrResponse['merchants'][] = $merchant;
    	}

		echo json_encode($arrResponse);
	}
	/**
     * favourites
     *
     * Retrieves list of favourites merchants of user
     * 
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
	function merchantdetails($param){
           
        $merchantid = $param['merchantid'];
        $userid = $param['userid'];
        $offset = $param['offset'];
		$this->load-> model('Merchants_model');
		$this->load-> model('Offers_model');
		$favouriteexist = $this ->Merchants_model->favouritemerchantexist($userid,$merchantid);

		
		$arrResponse['merchantdetails'] = $this->Merchants_model->getmerchantprofile($merchantid);
		if($favouriteexist)
		{
			$arrResponse['merchantdetails']['isfav'] = $favouriteexist['isfav'];
		}else
		{
           $arrResponse['merchantdetails']['isfav'] = 0;
		}
		$arrResponse['merchantdetails']['offers'] = $this->Offers_model->getofferslistofmerchant($merchantid,$offset);
		echo json_encode($arrResponse);
	}
    /**
     * searchmerchant
     *
     * search merchant
     *
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
    function searchmerchant($param){

        $search = $param['search'];

        $offset = $param['offset'];
        $this->load-> model('Merchants_model');

        $result = $this ->Merchants_model->searchmerchant($search,$offset);

        if($result)
        {
            $arrResponse['isValid'] = true;
            $arrResponse['message'] = 'Search Results Found';
            $arrResponse['merchants'] = $result;
            $arrResponse['totalcount'] = $this ->Merchants_model->searchmerchantcount($search);
        }else
        {
            $arrResponse['isValid'] = false;
            $arrResponse['message'] = 'There is No Merchant Found';
            $arrResponse['merchants'] = array();
            $arrResponse['totalcount'] = 0;
        }

        echo json_encode($arrResponse);
    }
     /**
     * nearbymerchants
     *
     * Retrieves list of nearby merchants
     *
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
    function nearbymerchants($param){

        $lat = $param['lat'];
        $long = $param['long'];
        $offset = $param['offset'];
        $this->load-> model('Merchants_model');

        $result = $this ->Merchants_model->getnearbymerchants($lat,$long,$offset);
        if($result)
        {
            $arrResponse['isValid'] = true;
            $arrResponse['message'] = 'Nearby Merchants Found';
            $arrResponse['merchants'] = $result;
            $arrResponse['totalcount'] = $this ->Merchants_model->totalgetnearbymerchants($lat,$long);
        }else
        {
            $arrResponse['isValid'] = false;
            $arrResponse['message'] = 'There is No Merchant Found';
            $arrResponse['merchants'] = array();
            $arrResponse['totalcount'] =0;
        }


        echo json_encode($arrResponse);
    }	
     /**
     * registeruserdevice
     *
     * register user device with app
     *
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
    function registeruserdevice($param){

        $arrData['userid'] = $param['userid'];
       $UpdateData['devicetoken']= $arrData['devicetoken'] = $param['devicetoken'];
      $UpdateData['platform']=  $arrData['platform'] = $param['platform'];
      $UpdateData['modified_date'] = $arrData['created_date'] = date("Y-m-d H:i:s");
        $this->load-> model('User_model');

        $result = $this ->User_model->checkuserdeviceregistered($arrData['userid']);
        if($result)
        {

           $update = $this ->User_model->updateregisteredevice($UpdateData,$arrData['userid']);
           if($update){
           	$arrResponse['isValid'] = true;
            $arrResponse['message'] = 'Registered device updated Successfully';
           }else{
           $arrResponse['isValid'] = false;
            $arrResponse['message'] = 'Registered device has not updated Successfully !!';

           }
        }else
        {
           $register = $this ->User_model->registeruserdevice($arrData);
           if($register){
           	$arrResponse['isValid'] = true;
            $arrResponse['message'] = 'Device registered  Successfully';
           }else{
               $arrResponse['isValid'] = false;
            $arrResponse['message'] = 'Device has not registered Successfully !!';

           }
        }

      echo json_encode($arrResponse);
    }
    /**
     * registermerchantdevice
     *
     * register merchant device with app
     *
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
    function registermerchantdevice($param){

        $arrData['merchantid'] = $param['merchantid'];
       $UpdateData['devicetoken']= $arrData['devicetoken'] = $param['devicetoken'];
      $UpdateData['platform']=  $arrData['platform'] = $param['platform'];
      $UpdateData['modified_date'] = $arrData['created_date'] = date("Y-m-d H:i:s");
        $this->load-> model('Merchants_model');

        $result = $this ->Merchants_model->checkmerchantdeviceregistered($arrData['merchantid']);
        if($result)
        {

           $update = $this ->Merchants_model->updateregisteredevice($UpdateData,$arrData['merchantid']);
           if($update){
           	$arrResponse['isValid'] = true;
            $arrResponse['message'] = 'Registered device updated Successfully';
           }else{
           $arrResponse['isValid'] = false;
            $arrResponse['message'] = 'Registered device has not updated Successfully !!';

           }
        }else
        {
           $register = $this ->Merchants_model->registermerchantdevice($arrData);
           if($register){
           	$arrResponse['isValid'] = true;
            $arrResponse['message'] = 'Device registered  Successfully';
           }else{
               $arrResponse['isValid'] = false;
            $arrResponse['message'] = 'Device has not registered Successfully !!';

           }
        }

      echo json_encode($arrResponse);
    }
     /**
     * Updatesettings
     *
     * Update the settings of user
     *
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
    function Updatesettings($param){
        $userid = $param['userid'];
         $UpdateData['pushon'] = $param['pushon'];
      $UpdateData['smson']=  $param['smson'];
      $UpdateData['modified_date'] = date("Y-m-d H:i:s");
        $this->load-> model('User_model');

        $result = $this->User_model->updateuser($UpdateData,$userid);
        if($result)
        {
            $arrResponse['isValid'] = true;
            $arrResponse['message'] = 'User Settings Updated Succesfully';
            
        }else
        {
            $arrResponse['isValid'] = false;
            $arrResponse['message'] = 'Failed to Update User Settings';
            
        }


        echo json_encode($arrResponse);
    }
/**
     * unregisterDevice
     *
     * Unregister device for user
     *
     * @author Ketan Sangani
     * @access public
     * @param array-$param
     * @return void
     */
    function unregisterDevice($param){

        $userid = $param['userid'];
        $platform = $param['platform'];
        
        $this->load-> model('User_model');

        $result =$this->User_model->unregisterDevice($platform,$userid);
        if($result)
        {
            $arrResponse['isValid'] = true;
            $arrResponse['message'] = 'Device unregistered  Successfully';

        }else
        {
            $arrResponse['isValid'] = false;
            $arrResponse['message'] = 'Failed to unregister Device !!';
           
        }


        echo json_encode($arrResponse);
    }
    /**
	 * forgot_password
	 *
	 * loads forgot_password page of the frontend
	 *
	 * @param void
	 * @return void
	 */
	public function forgot_password($param){

		 //$this->load->library('phpmailer');

		$arrData['email']=$param['email'];
		if ($arrData['email']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter your Email Id';
			echo json_encode($error);
			exit ;
		} 
		else if(filter_var($arrData['email'], FILTER_VALIDATE_EMAIL) === false){
			$error['isValid'] = false;
           $error['message'] = 'Please enter Valid Email';
			echo json_encode($error);
			exit ;

		}
		 $this->load-> model('User_model');
			$result = $this->User_model->forgot_password($arrData['email']);
			
			if($result != FALSE){
				$url_key = base64_encode($result['token'].'/'.$result['id']);
			//$message = "<a href='".site_url('User/index/'.urlencode($url_key))."' target='_blank'>".site_url('User/index/'.urlencode($url_key))."</a>";
            
				/*$message = "Hi {$result['name']}, <br /><br />";
				$message .= "This email contains a link. Please click on the link below to access your account. <br /><br />";
				$message .= "<a href='".site_url('User/index/'.urlencode($url_key))."' target='_blank'>".site_url('User/index/'.urlencode($url_key))."</a>";
				$message .= "<br /><br />Thanks & Regards,<br />Otap Team";
				$mail = new PHPMailer();
				$mail->IsSMTP();        // set mailer to use SMTP
				$mail->SMTPDebug = 1;  // debugging: 1 = errors and messages, 2 = messages only
				$mail->SMTPAuth = true;     // turn on SMTP authentication
				$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
				$mail->Host = 'smtp.gmail.com';
				$mail->Username = "jadhavashok470@gmail.com";  // SMTP username
				$mail->Password = "ashok1993"; // SMTP password
				$mail->Port = 465;
				$mail->From = "Otap Team";
				$mail->FromName = "Support";
				$mail->AddAddress($arrData['email']);
				$mail->WordWrap = 50;       // set word wrap to 50 characters
				$mail->Subject = "Reset Password";
				$mail->Body   =$message;
				$mail->IsHTML(true);*/
				//if($mail->Send()){
			$arrResponse['isValid'] = true;
            $arrResponse['message'] = 'An email has been sent to your registered email id. Please check your email to proceed.';
				//}
			}
			else{
            
			$arrResponse['isValid'] = false;
            $arrResponse['message'] = 'The email id you have entered is not registered with us.';
			}
		
		echo json_encode($arrResponse);
	}
	/**
	 * forgot_password
	 *
	 * loads forgot_password page of the frontend
	 *
	 * @param void
	 * @return void
	 */
	public function forgotpassword($param){

		 //$this->load->library('phpmailer');

		$arrData['email']=$param['email'];
		if ($arrData['email']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter your Email Id';
			echo json_encode($error);
			exit ;
		} 
		else if(filter_var($arrData['email'], FILTER_VALIDATE_EMAIL) === false){
			$error['isValid'] = false;
           $error['message'] = 'Please enter Valid Email';
			echo json_encode($error);
			exit ;

		}
		 $this->load-> model('Merchants_model');
			$result = $this->Merchants_model->forgot_password($arrData['email']);
			
			if($result != FALSE){
				$url_key = base64_encode($result['token'].'/'.$result['merchant_id']);
			//$message = "<a href='".site_url('Merchant/index/'.urlencode($url_key))."' target='_blank'>".site_url('Merchant/index/'.urlencode($url_key))."</a>";
           
				/*$message = "Hi {$result['name']}, <br /><br />";
				$message .= "This email contains a link. Please click on the link below to access your account. <br /><br />";
				$message .= "<a href='".site_url('Merchant/index/'.urlencode($url_key))."' target='_blank'>".site_url('Merchant/index/'.urlencode($url_key))."</a>";
				$message .= "<br /><br />Thanks & Regards,<br />Otap Team";
				$mail = new PHPMailer();
				$mail->IsSMTP();        // set mailer to use SMTP
				$mail->SMTPDebug = 1;  // debugging: 1 = errors and messages, 2 = messages only
				$mail->SMTPAuth = true;     // turn on SMTP authentication
				$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
				$mail->Host = 'smtp.gmail.com';
				$mail->Username = "jadhavashok470@gmail.com";  // SMTP username
				$mail->Password = "ashok1993"; // SMTP password
				$mail->Port = 465;
				$mail->From = "Otap Team";
				$mail->FromName = "Support";
				$mail->AddAddress($arrData['email']);
				$mail->WordWrap = 50;       // set word wrap to 50 characters
				$mail->Subject = "Reset Password";
				$mail->Body   =$message;
				$mail->IsHTML(true);*/
				//if($mail->Send()){
			$arrResponse['isValid'] = true;
            $arrResponse['message'] = 'An email has been sent to your registered email id. Please check your email to proceed.';
				//}
			}
			else{
            
			$arrResponse['isValid'] = false;
            $arrResponse['message'] = 'The email id you have entered is not registered with us.';
			}
		
		echo json_encode($arrResponse);
	}
	 /**
	 * addemployee
	 *
	 * Saves the Employee contact details to database
	 * 
	 * @author Ketan sangani
	 * @access public
	 * @param array-$param
	 * @return void
	 */
	public function addemployee($param) {
		
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('contact', 'Mobile', 'required|is_unique[merchants_contacts.contact]');
					 $arrData["merchant_id"] = $param['merchantid'];
					 $UpdateData['employeename'] = $arrData["employeename"] = $param['name'];
					 $UpdateData['contact'] = $arrData["contact"] = $param['contact'];
  					 $this->load-> model('Merchants_model');
  if ($param['name']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Employee Name';
			echo json_encode($error);
			exit ;
		} 
  else if ($param['contact']=='') {
			$error['isValid'] = false;
			$error['message'] = 'Please enter Mobile Number';
			echo json_encode($error);
			exit ;
		} 
		else if(!preg_match('/^[0-9]{10}+$/', $param['contact'])) {
			$error['isValid'] = false;
            $error['message'] = 'Please enter Valid Mobile Number';
			echo json_encode($error);
			exit ;

		}
		elseif($this->form_validation->run() == FALSE){
           $error['isValid'] = false;
           $error['message'] = 'This Mobile Number Already Registered.';
			echo json_encode($error);
			exit ;
		}
					$UpdateData['modified_date'] = $arrData["created_date"] = date("Y-m-d H:i:s");
					$query = $this->db->query('SELECT MIN(cindex) as cindex,id FROM  otap_merchants_contacts where isDeleted=1
');
					//echo $query;exit;
					$result = $query->result_array();
					//var_dump($result);exit;
                     if($result[0]['cindex']!=NULL){           
                       $UpdateData['isDeleted'] = 0;

 $insertedFlag = $this->Merchants_model->update_merchantcontactdetails($result[0]['id'],$UpdateData);
 if ($insertedFlag) {
			$arrResponse['isValid'] = true;
            $arrResponse['message'] = 'Employee Details Added Successfully';
					} 
}else{
$query1 = $this->db->query('SELECT MAX(cindex) as cindex FROM  otap_merchants_contacts where isDeleted=0
');
					$result1 = $query1->result_array();
 if($result1[0]['cindex']!=NULL)
{
$arrData["cindex"] = $result1[0]['cindex'] +1;
$arrData['isDeleted'] = 0;
$insertedFlag1 = $this->Merchants_model->savemerchantcontactDetails($arrData);
if ($insertedFlag1) {
			$arrResponse['isValid'] = true;
            $arrResponse['message'] = 'Employee Details Added Successfully';
					} 
}else{
$arrData["cindex"] = 1;
$arrData['isDeleted'] = 0;
$insertedFlag2 = $this->Merchants_model->savemerchantcontactDetails($arrData);
if ($insertedFlag2) {
			$arrResponse['isValid'] = true;
            $arrResponse['message'] = 'Employee Details Added Successfully';
					} 
}
}
echo json_encode($arrResponse);
					
					
}

/**
   * deleteemployee
   *
   * This help to delete employee contact Details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$param
   * @return  void
   */

  public function deleteemployee($param){
		
					$date = date("Y-m-d H:i:s");
					$this->load-> model('Merchants_model');
					$UpdateData["contact"] ='';                    
                    $UpdateData["modified_date"] = date("Y-m-d H:i:s");
					$UpdateData["isDeleted"] =1;
					$UpdateData["employeename"] = '';
					$updateFlag = $this->Merchants_model->update_merchantcontactdetails($param['employeeid'], $UpdateData);
						if ($updateFlag) {
							$arrResponse['isValid'] = true;
            				$arrResponse['message'] = 'Employee Details Deleted Successfully';
						}
  		echo json_encode($arrResponse);

}
/**
   * enablecontact
   *
   * This help to enable or disable  employee mobile number Details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$param
   * @return  void
   */

  public function enablecontact($param){
		
					$date = date("Y-m-d H:i:s");
					$this->load-> model('Merchants_model');
					$UpdateData["status"] =$param['status'];                    
                    $UpdateData["modified_date"] = date("Y-m-d H:i:s");
					
					$updateFlag = $this->Merchants_model->update_merchantcontactdetails($param['employeeid'], $UpdateData);
						if ($updateFlag) {
		     			$arrResponse['isValid'] = true;
							if($param['status']==1){
            				$arrResponse['message'] = 'Employee Contact Enabled For SMS Successfully';
            			}elseif($param['status']==0){
            				$arrResponse['message'] = 'Employee Contact Disabled For SMS Successfully';
            			}
			}
  		echo json_encode($arrResponse);

}	
/**
   * enablepushnotification
   *
   * This help to enable or disable  push notification Details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$param
   * @return  void
   */

  public function enablepushnotification($param){
		
					$date = date("Y-m-d H:i:s");
					$this->load-> model('Merchants_model');
					$UpdateData["isSmson"] =$param['isSmson'];                    
                    $UpdateData["modified_date"] = date("Y-m-d H:i:s");
					
					$updateFlag = $this->Merchants_model->update_merchant($param['merchantid'], $UpdateData);
						if ($updateFlag) {
		     			$arrResponse['isValid'] = true;
							if($param['isSmson']==1){
            				$arrResponse['message'] = 'SMS Notification Enabled Of Merchant Successfully';
            			}elseif($param['isSmson']==0){
            				$arrResponse['message'] = 'SMS Notification Disabled Of Merchant Successfully';
            			}
			}
  		echo json_encode($arrResponse);

}	
	   
	
}
?>