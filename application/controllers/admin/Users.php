<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users Class 
 *
 * @author Amit Salunkhe
 * @package Codeigniter
 * @subpackage Controller
 *
 */
class Users extends CI_Controller {

    /**
     * construct
     *
     * constructor method (checks login status)
     * @author Amit Salunkhe
     * @access public
     * @param none
     * @return void
     *
     */
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') == FALSE) {
            $this->session->set_flashdata('error', 'Please Login First!!');
            redirect('admin');
        }
        $this->load->model('User_model');
        
    }

    /**
     * index
     *
     * display users list
     * 
     * @author Amit Salunkhe
     * @access public
     * @param none 
     * @return void
     */
    public function index() {
        
        $arrData['tab'] = $this->uri->segment(2);
        $arrData['tab1']='';
        $arrData['title'] = 'Users List';
        $arrData['middle'] = 'admin/users/list';
        
        $arrData['users'] = $this->User_model->getuserDetails();
        $this->load->view('admin/template', $arrData);
    }

    

}

/* End of file Users.php */
/* Location: ./application/controllers/admin/Users.php */
