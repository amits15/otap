<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dashboard Class 
 *
 * @author Ashok Jadhav
 * @package Codeigniter
 * @subpackage Controller
 *
 */

class Dashboard extends CI_Controller {

/**
 *index
 *
 *display dashboard
 * 
 *@author Ashok Jadhav
 *@access public
 *@param none 
 *@return void
 */
    public function index()
    {   
        $arrData['tab'] = $this->uri->segment(2);
        $arrData['tab1']='';
        $arrData['title'] = 'Dashboard';
        $arrData['middle'] = 'admin/dashboard';
        $this->load->view('admin/template', $arrData);
    }
}
/* End of file Dashboard.php */
/* Location: ./application/controllers/admin/Dashboard.php */
