<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Login Class 
 *
 * @author Ashok Jadhav
 * @package Codeigniter
 * @subpackage Controller
 *
 */

class Login extends CI_Controller {
    
/**
* construct
*
* Calls parent constructor
* 
* @author	Ashok jadhav
* @access	public
* @return	void
*/
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('login_model');

    }
    
    /**
     *index
     *
     *Display login window
     * 
     *@author Ashok Jadhav
     *@access public
     *@param none 
     *@return void
     */
    public function index()
    {

        $this->load->view('admin/login');
    }
        
    /**
     * check
     *
     * This help to Authenticate admin login
     * 
     * @author  Ashok Jadhav
     * @access  public
     * @param $username,$password
     * @return  void
     */
    public function check($username,$password)
    {
        $this->login_model->verifyUser($username,$password);
        if ($this->session->userdata('logged_in')=== TRUE){
            $arrData['status'] = 1;
            echo json_encode($arrData);
            
        }
        else
        {
            $arrData['status'] = 0;
            echo json_encode($arrData);
           
        }
    }
}
