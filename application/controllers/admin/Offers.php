<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Offers Class 
 *
 * @author Ketan Sangani
 * @package Codeigniter
 * @subpackage Controller
 *
 */
class Offers extends CI_Controller {

	/**
	 * construct
	 *
	 * constructor method (checks login status)
	 * @author Ketan Sangani
	 * @access public
	 * @param none
	 * @return void
	 *
	 */
	function __construct() {
		parent::__construct();
		if ($this->session->userdata('logged_in') == FALSE) {
			$this->session->set_flashdata('error', 'Please Login First!!');
			redirect('admin');
		}
		$this->load->model('Offers_model');
		$this->load->model('Merchants_model');
	   
	}

	/**
	 * index
	 *
	 * display offers list
	 * 
	 * @author Ketan Sangani
	 * @access public
	 * @param none 
	 * @return void
	 */
	public function index(){
		/*if ($this->input->post('delete')) {
								$multiDelete = $this->Offers_model->multi_delete_offers($this->input->post('delete'));
								
								if ($multiDelete)
												$this->session->set_flashdata('success', 'Deleted !!');
								else
												$this->session->set_flashdata('error', 'Failed to Delete!!');
								
								 redirect('admin/offers');
				}*/
 
		 $arrData['tab'] = $this->uri->segment(2);
		$arrData['tab1']='';
		$arrData['title'] = 'Offers List';
		$arrData['middle'] = 'admin/offers/list';
		$arrData['offers'] = $this->Offers_model->getoffersDetails();
		$this->load->view('admin/template', $arrData);
	}

	/**
	 * add
	 *
	 * Saves the offer details to database
	 * 
	 * @author Ketan Sangani
	 * @access public
	 * @param none
	 * @return void
	 */
	public function add() {
		if ($_POST) {
			if ($this->input->post('submit')) {
				
					$arrData["title"] = $this->input->post('txtname');
					$arrData["merchant_id"] = implode($this->input->post('ddltypemerchant'), ",");
					
					
					$arrData["type"] = $this->input->post('ddltypeoffer');
					$arrData["description"] = $this->input->post('txtdesc');
					$arrData["start_date"] = $this->input->post('txtstartdate');
					
					$arrData["end_date"] = $this->input->post('txtenddate');
					$arrData["amount"] = $this->input->post('txtamount');
					$arrData["offer_discount"] = $this->input->post('txtdiscount');
					//$arrData["max_discount"] = $this->input->post('txtmaxdiscount');
				   
					 $arrData["maxdiscountperuser"] = $this->input->post('txtmaxdiscountusr');
					$arrData["maxdiscountpertransaction"] = $this->input->post('txtmaxdiscounttrans');
					$arrData["promotionaltext"] = $this->input->post('txtpromotional');
					$arrData["image"] = '';
					$arrData["created_date"] = date("Y-m-d H:i:s");
					

					if ($_FILES['image']) {
						$data = $this->do_upload('image');
						if (isset($data['error'])) {
							$this->session->set_flashdata('error', $data['error']);
							redirect('admin/offers/add');
						} else {
							$arrData["image"] = $data['file_name'];
						}
					
				   
					$insertedFlag = $this->Offers_model->saveofferDetails($arrData);
					if ($insertedFlag) {
						$this->session->set_flashdata('success', 'Offer Details Added Successfully !!');
						redirect('admin/offers');
					} else {
						$this->session->set_flashdata('error', 'Failed to Add Offer Details!!');
						redirect('admin/offers/add');
					}
				}
			}
		} else {
			$arrData['error'] = '';
		}
		$arrData['tab'] = $this->uri->segment(2);
		$arrData['tab1'] = '';
		$arrData['merchants'] = $this->Merchants_model->getmerchantDetails();

		$arrData['offertypes'] = $this->Offers_model->getoffertypeDetails();
		$arrData['title'] = 'Add Offer';
		$arrData['middle'] = 'admin/offers/add';
		$this->load->view('admin/template', $arrData);
	}
/**
   * edit
   *
   * This help to edit offer Details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   $iuserId
   * @return  void
   */

  public function edit($iuserId) {
		$arrData['portsDetailsArr'] = "";
		if ($_POST) {
				if ($this->input->post('submit')) {
						$date = date("Y-m-d H:i:s");
					$UpdateData["title"] = $this->input->post('txtname');
					$UpdateData["merchant_id"] = implode($this->input->post('ddltypemerchant'), ",");
					
					
					$UpdateData["type"] = $this->input->post('ddltypeoffer');
					$UpdateData["description"] = $this->input->post('txtdesc');
					$UpdateData["start_date"] = $this->input->post('txtstartdate');
					
					$UpdateData["end_date"] = $this->input->post('txtenddate');
					$UpdateData["amount"] = $this->input->post('txtamount');
					$UpdateData["offer_discount"] = $this->input->post('txtdiscount');
					 $UpdateData["maxdiscountperuser"] = $this->input->post('txtmaxdiscountusr');
					$UpdateData["maxdiscountpertransaction"] = $this->input->post('txtmaxdiscounttrans');
					$UpdateData["promotionaltext"] = $this->input->post('txtpromotional');
					$UpdateData["modified_date"] = date("Y-m-d H:i:s");
					 //$UpdateData["max_discount"] = $this->input->post('txtmaxdiscount');
					//$UpdateData["discounttype"] = implode($this->input->post('discounttype'), ",");
				   $config['upload_path'] = 'uploads/offers/';
										$config['allowed_types'] = 'gif|jpg|png|jpeg';

										$this->upload->initialize($config);
										if($this->upload->do_upload('image')){
												$data=$this->upload->data();
												if($data['file_name']!=''){
													$UpdateData["image"] = $data['file_name'];

												}
												
										 }else{
						$error = array('error' => $this->upload->display_errors());
							
							$this->session->set_flashdata('error', 'Sorry!!!Some error has occured while uploading file.');

											}
										   
				
				$updateFlag = $this->Offers_model->update_offer($iuserId, $UpdateData);
						if ($updateFlag) {
								$this->session->set_flashdata('success', 'Offer Details Updated Successfully !!');
								redirect('admin/offers');
						}
						else{
								$this->session->set_flashdata('error', 'Failed to Update Offer Details!!');
								redirect('admin/offers/edit/' . $iuserId);
						}
		}
		else{
				$ports_details = $this->Offers_model->getoffersDetails($iuserId);
				$arrData['portsDetailsArr'] = $ports_details;
		}
		
  }
		$arrData['merchants'] = $this->Merchants_model->getmerchantDetails();

		$arrData['offertypes'] = $this->Offers_model->getoffertypeDetails();
		$ports_details = $this->Offers_model->getoffersDetails($iuserId);
		$arrData['portsDetailsArr'] = $ports_details;
		$arrData['tab'] = $this->uri->segment(2);
		$arrData['tab1']= '';
		$arrData['title'] = 'Edit Offer';
		$arrData['middle'] = 'admin/offers/edit';
		$this->load->view('admin/template', $arrData);
}
	   /**
 * delete
 * 
 * delete Offer Details
 * @access public
 * @param $id,$name
 * @return integer-$id
 * @author Ketan Sangani
 * 
 */
	function delete($id)
	{
			$this->Offers_model->delete_offer($id);
			
			$this->session->set_flashdata('success', 'Offer deleted successfully!!!');
			redirect('admin/offers/');
	}
	/**
	 * do_upload
	 *
	 * uploads the image files
	 * 
	 * @author Amit Salunkhe
	 * @access public
	 * @param String - Image container name
	 * @return array - Image data/Error
	 */
	function do_upload($fieldName){
		$config['upload_path'] = 'uploads/offers/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';

		$this->upload->initialize($config);

		if (!$this->upload->do_upload($fieldName)) {
			$error = array("error" => $this->upload->display_errors());
		   
			return $error;
		} else {
			$data = $this->upload->data();
			return $data;
		}
	}
   
  /**
   * publish
   *
   * This help to publish Offer Details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   $icategoryId
   * @return  void
   */

  public function publish($icategoryId) {
		$delete = $this->Offers_model->set_offer($icategoryId);
  }

   /**
   * unpublish
   *
   * This help to unpublish Offer Details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   $icategoryId
   * @return  void
   */
  public function unpublish($icategoryId) {
	   $delete = $this->Offers_model->unset_offer($icategoryId);
  }
  /**
	 * index
	 *
	 * display type of offers list
	 * 
	 * @author Amit Salunkhe
	 * @access public
	 * @param none 
	 * @return void
	 */
	public function offertype() {
		
		$arrData['tab'] = $this->uri->segment(3);
		$arrData['title'] = 'Offer Type List';
		$arrData['middle'] = 'admin/offers/listtype';
		$arrData['tab1'] = '';
		$arrData['offertypes'] = $this->Offers_model->getoffertypeDetails();
		$this->load->view('admin/template', $arrData);
	}

	/**
	 * add
	 *
	 * Saves the offer type details to database
	 * 
	 * @author Amit Salunkhe
	 * @access public
	 * @param none
	 * @return void
	 */
	public function addtype() {
	   
			if ($this->input->post('submit')) {
							  $date = date("Y-m-d H:i:s");
					$arrData["offer_type"] = $this->input->post('txttype');
					$arrData["created_date"] = date("Y-m-d H:i:s");
					
					$insertedFlag = $this->Offers_model->saveoffertypeDetails($arrData);
					if ($insertedFlag) {
						$this->session->set_flashdata('success', 'Offer Type Details Added Successfully !!');
						redirect('admin/offers/offertype');
					} else {
						$this->session->set_flashdata('error', 'Failed to Add Offer Type Details!!');
						redirect('admin/offers/addtype');
					}
				}
			
		 else {
			$this->load->view('admin/offers/addtype');
		}
		
		
	}
/**
   * edittype
   *
   * This help to edit offer type Details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   $iuserId
   * @return  void
   */

  public function edittype($iuserId) {
				
				if ($this->input->post('submit')) {
						$date = date("Y-m-d H:i:s");
					$UpdateData["offer_type"] = $this->input->post('txttype');
					$UpdateData["modified_date"] = date("Y-m-d H:i:s");
				   
					
						$updateFlag = $this->Offers_model->update_type($iuserId, $UpdateData);
						if ($updateFlag) {
								$this->session->set_flashdata('success', 'Offer Type Details Updated Successfully !!');
								redirect('admin/offers/offertype');
						}
						else{
								$this->session->set_flashdata('error', 'Failed to Update Offer Type Details!!');
								redirect('admin/offers/edittype/' . $iuserId);
						}
				}
		else{
	$data['offertype'] = $this->Offers_model->getoffertypeDetails($iuserId);
	$this->load->view('admin/offers/edittype',$data);
  }
		
  
}
/**
	 * createrequests
	 *
	 * display create offers requests list
	 * 
	 * @author Ketan Sangani
	 * @access public
	 * @param none 
	 * @return void
	 */
	public function createrequests(){
		
 
		$arrData['tab'] = $this->uri->segment(2);
		$arrData['tab1']='';
		$arrData['title'] = 'Create Offers Requests';
		$arrData['middle'] = 'admin/offers/createrequests';
		$arrData['offers'] = $this->Offers_model->getrequestsofcreateoffers();
		$this->load->view('admin/template', $arrData);
	}
	/**
	 * updateequests
	 *
	 * display update offers requests list
	 * 
	 * @author Ketan Sangani
	 * @access public
	 * @param none 
	 * @return void
	 */
	public function updaterequests(){
		
 
		$arrData['tab'] = $this->uri->segment(2);
		$arrData['tab1']='';
		$arrData['title'] = 'Update Offers Requests';
		$arrData['middle'] = 'admin/offers/updaterequests';
		$arrData['offers'] = $this->Offers_model->getrequestsofupdateoffers();
		$this->load->view('admin/template', $arrData);
	}
	/**
	 * deleterequests
	 *
	 * display delete offers requests list
	 * 
	 * @author Ketan Sangani
	 * @access public
	 * @param none 
	 * @return void
	 */
	public function deleterequests(){
		
 
		$arrData['tab'] = $this->uri->segment(2);
		$arrData['tab1']='';
		$arrData['title'] = 'Delete Offers Requests';
		$arrData['middle'] = 'admin/offers/deleterequests';
		$arrData['offers'] = $this->Offers_model->getrequestsofdeleteoffers();
		$this->load->view('admin/template', $arrData);
	}
	/**
		 * approvecreate
		 *
		 * create offer by admin approval
		 * 
		 * @author Ketan Sangani
		 * @access public
		 * @param none 
		 * @return void
		 */
		public function approvecreate($id) {
				
				$arrData['tab'] = $this->uri->segment(2);
				$arrData['title'] = 'Create Offers Requests';
				
				$arrData['tab1']='';
				$requestsdetails = $this->Offers_model->getrequestsofcreateoffers($id);
				if($requestsdetails){
                $UpdateData["title"] = $requestsdetails[0]['title'];
				
				$UpdateData["merchant_id"] = $requestsdetails[0]['merchant_id'];
				$UpdateData["type"] = $requestsdetails[0]['type'];
			   $UpdateData["description"] = $requestsdetails[0]['description'];
			 $UpdateData["start_date"] = $requestsdetails[0]['start_date'];
			 $UpdateData["end_date"] = $requestsdetails[0]['end_date'];
			 $UpdateData["amount"] = $requestsdetails[0]['amount'];
			 $UpdateData["maxdiscountpertransaction"] = $requestsdetails[0]['maxdiscountpertransaction'];
			 $UpdateData["offer_discount"] = $requestsdetails[0]['offer_discount'];
			 $UpdateData["maxdiscountperuser"] = $requestsdetails[0]['maxdiscountperuser'];
			 $UpdateData["promotionaltext"] = $requestsdetails[0]['promotionaltext'];

			 $UpdateData["created_date"] = date("Y-m-d H:i:s");
			 
			 
				$insertedFlag = $this->Offers_model->saveofferDetails($UpdateData);
              
				if ($insertedFlag) {
				
                        $query = $this->db->query("DELETE FROM otap_offerscache WHERE id='$id'");
						$this->session->set_flashdata('success', 'Offers Created Successfully !!');
						redirect('admin/offers/createrequests');
					} else {
						$this->session->set_flashdata('error', 'Failed to Create Offer!!');
						redirect('admin/offers/createrequests');
					}
		}
				
		}
		/**
		 * approvedelete
		 *
		 * delete offer by admin approval
		 * 
		 * @author Ketan Sangani
		 * @access public
		 * @param none 
		 * @return void
		 */
		public function approvedelete($id,$merchantid,$offerid) {
				
				$arrData['tab'] = $this->uri->segment(2);
				$arrData['title'] = 'Delete Offers Requests';
				
				$arrData['tab1']='';
				$requestsdetails = $this->Offers_model->getoffersDetails($offerid);
				if($requestsdetails){
                $merchants = $requestsdetails[0]['merchant_id'];
                //echo $offerid;exit;
               // echo $merchants;exit;
                $parts = explode(',', $merchants);
    if(count($parts)==1){
      $query = $this->db->query("DELETE FROM otap_offers WHERE offer_id='$offerid'");
      if ($query) {
				
                        $query1 = $this->db->query("DELETE FROM otap_offerscache WHERE id='$id'");
                        //echo $this->db->last_query();exit;
						$this->session->set_flashdata('success', 'Offers Deleted Successfully !!');
						redirect('admin/offers/deleterequests');
					} else {
						$this->session->set_flashdata('error', 'Failed to Delete Offer!!');
						redirect('admin/offers/deleterequests');
					}
    }else{
    while(($i = array_search($merchantid, $parts)) !== false) {
        unset($parts[$i]);
    }
   $UpdateData['merchant_id'] = implode(',', $parts);
	 
			 
				$insertedFlag = $this->Offers_model->update_offer($offerid,$UpdateData);;
              
				if ($insertedFlag) {
				
                        $query = $this->db->query("DELETE FROM otap_offerscache WHERE id='$id'");
						$this->session->set_flashdata('success', 'Offers Deleted Successfully !!');
						redirect('admin/offers/deleterequests');
					} else {
						$this->session->set_flashdata('error', 'Failed to Deleted Offer!!');
						redirect('admin/offers/deleterequests');
					}
				}
		}
				
		}
		/**
		 * approvecreate
		 *
		 * create offer by admin approval
		 * 
		 * @author Ketan Sangani
		 * @access public
		 * @param none 
		 * @return void
		 */
		public function approveupdate($id) {
				
				$arrData['tab'] = $this->uri->segment(2);
				$arrData['title'] = 'Update Offers Requests';
				
				$arrData['tab1']='';
				$requestsdetails = $this->Offers_model->getrequestsofupdateoffers($id);
				if($requestsdetails){
                $UpdateData["title"] = $requestsdetails[0]['title'];
				
				//$UpdateData["merchant_id"] = $requestsdetails[0]['merchant_id'];
				$UpdateData["type"] = $requestsdetails[0]['type'];
			   $UpdateData["description"] = $requestsdetails[0]['description'];
			 $UpdateData["start_date"] = $requestsdetails[0]['start_date'];
			 $UpdateData["end_date"] = $requestsdetails[0]['end_date'];
			 $UpdateData["amount"] = $requestsdetails[0]['amount'];
			 $UpdateData["maxdiscountpertransaction"] = $requestsdetails[0]['maxdiscountpertransaction'];
			 $UpdateData["offer_discount"] = $requestsdetails[0]['offer_discount'];
			 $UpdateData["maxdiscountperuser"] = $requestsdetails[0]['maxdiscountperuser'];
			 $UpdateData["promotionaltext"] = $requestsdetails[0]['promotionaltext'];

			 $UpdateData["modified_date"] = date("Y-m-d H:i:s");
			 
			 
				$insertedFlag = $this->Offers_model->update_offer($requestsdetails[0]['offer_id'],$UpdateData);
              
				if ($insertedFlag) {
				
                        $query = $this->db->query("DELETE FROM otap_offerscache WHERE id='$id'");
						$this->session->set_flashdata('success', 'Offer Updated Successfully !!');
						redirect('admin/offers/updaterequests');
					} else {
						$this->session->set_flashdata('error', 'Failed to Update Offer!!');
						redirect('admin/offers/updaterequests');
					}
		}
				
		}

}

/* End of file Offers.php */
/* Location: ./application/controllers/admin/Offers.php */
