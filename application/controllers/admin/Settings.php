<?php 
/**
 * Settings Class 
 *
 * @author Ashok Jadhav
 * @package Codeigniter
 * @subpackage Controller
 *
 */
 

class Settings extends CI_Controller {

/**
 * construct
 * 
 * constructor method (checks login status)
 * 
 * @author Ashok Jadhav
 * @access public
 * @param none
 * @return void
 * 
*/
    function __construct()
    {
        parent::__construct();
        $this->load->model('Settings_model');
        if($this->session->userdata('logged_in') == FALSE) {
            $this->session->set_flashdata('error', 'Please Login First!!');
            redirect('admin');
            break;
        }
    }

/**
 * index
 *
 * This help to display Password Change Panel
 * 
 * @author Ashok Jadhav
 * @access public
 * @param none
 * @return void
 * 
 */
    public function index(){
        $arrData['tab']=$this->uri->segment(2);
        $arrData['tab1']='';
        $arrData['title']='Setting';
        $arrData['middle'] = 'admin/settings';
        $this->load->view('admin/template', $arrData);
    }

/**
 * psw_change
 *
 * This help to change the admin password
 * 
 * @author Ashok Jadhav
 * @access public
 * @param none
 * @return void
 * 
 */
    function psw_change()
    {	
        if($_POST){
            $id=$this->input->post('id');
            $old_psw=md5($this->input->post('old_psw'));
            $pwd=$this->input->post('psw');
            $cn_psw=$this->input->post('cn_psw');
                if($pwd!=$cn_psw){
                    $this->session->set_flashdata('error','New password and Confirmed password does not match,Please try again !!!');
                    redirect('admin/settings');
                }
                else
                {
                    $cn_password=md5($cn_psw);
                    $insertedflag=$this->Settings_model->set_password($id,$old_psw,$cn_password);
                    if($insertedflag){
                        $this->session->set_flashdata('success','Password Updated Successfully || Please Login');
                        redirect('admin/login');
                        //set success and redirect to login
                    }
                    else
                    {
                        $this->session->set_flashdata('error','You have entered wrong old password !!!');
                        redirect('admin/settings');
                        //
                    }

                }
        }
        else
        {

        }
    }
}