<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home Class 
 *
 * @author Ketan Sangani
 * @package Codeigniter
 * @subpackage Controller
 *
 */

class Home extends CI_Controller {
    
    
   /**
    * construct
    * 
    * constructor method (checks login status)
    * 
    * @author Ketan Sangani
    * @access public
    * @param none
    * @return void
    * 
    */
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
        if($this->session->userdata('logged_in') == FALSE) {
                $this->session->set_flashdata('error', 'Please Login First!!');
                redirect('admin');
                break;
        }
    }
    
   /**
    * index
    *
    * display content of home  page
    * 
    *@author Ketan Sangani
    *@access public
    *@param none 
    *@return void
    */
    public function index()
    {  
        if ($_POST) {
                if ($this->input->post('submit')) {
                        
                        
                                $date = date("Y-m-d H:i:s");
                                $inserted_rightsFlag = true;
                                $updateData["title1"] = $this->input->post('txttitle1');
                                 $updateData["title2"] = $this->input->post('txttitle2');
                                $updateData["db_cms"] = $this->input->post('tadescription1');
                                 $updateData["report_cms"] = $this->input->post('tadescription2');
                                $updateData["modified_date"] = $date;
                                $insertedFlag = $this->home_model->edit_description($updateData);
                                if ($insertedFlag) {
                                        $this->session->set_flashdata('success', 'Content Added Successfully !!');
                                        redirect('admin/home');
                                }
                                else{
                                        $this->session->set_flashdata('error','Failed to Add content!!');
                                        redirect('admin/home');
                                }
                        
                }
        }
        else
        {
                $arrData['error']='';
            
        }
         $arrData['tab'] = $this->uri->segment(2);
        $arrData['title'] = 'Home Cms';
        $arrData['description'] = $this->home_model->get_description();
        $arrData['middle'] = 'admin/home_cms';
        $this->load->view('admin/template', $arrData);
    }
}
/* End of file About_us.php */
/* Location: ./application/controllers/admin/About_us.php */
