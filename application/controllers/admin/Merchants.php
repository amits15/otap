<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Merchants Class 
 *
 * @author Ketan Sangani
 * @package Codeigniter
 * @subpackage Controller
 *
 */
class Merchants extends CI_Controller {

		/**
		 * construct
		 *
		 * constructor method (checks login status)
		 * @author Ketan Sangani
		 * @access public
		 * @param none
		 * @return void
		 *
		 */
		function __construct() {
				parent::__construct();
				if ($this->session->userdata('logged_in') == FALSE) {
						$this->session->set_flashdata('error', 'Please Login First!!');
						redirect('admin');
				}
				$this->load->model('Merchants_model');
				
				
		}

		/**
		 * index
		 *
		 * display merchants list
		 * 
		 * @author Ketan Sangani
		 * @access public
		 * @param none 
		 * @return void
		 */
		public function index() {
				if ($this->input->post('delete')) {
								$multiDelete = $this->Merchants_model->multi_delete_merchants($this->input->post('delete'));
								
								if ($multiDelete)
												$this->session->set_flashdata('success', 'Deleted !!');
								else
												$this->session->set_flashdata('error', 'Failed to Delete!!');
								
								 redirect('admin/merchants');
				}
				$arrData['tab'] = $this->uri->segment(2);
				$arrData['title'] = 'Merchants';
				$arrData['middle'] = 'admin/merchants/list';
				$arrData['tab1']='';
				$arrData['categories'] = $this->Merchants_model->getcategoryDetails();

				$arrData['merchants'] = $this->Merchants_model->getmerchantDetails();
				
				$this->load->view('admin/template', $arrData);
		}

		/**
		 * add
		 *
		 * Saves the customer details to database
		 * 
		 * @author Ketan Sangani
		 * @access public
		 * @param none
		 * @return void
		 */
		public function add() {
				if ($_POST) {
						if ($this->input->post('submit')) {
                            $this->form_validation->set_rules('txtmobile', 'Mobile', 'required|is_unique[merchants.merchant_contact]');
                            $this->form_validation->set_rules('txtemail', 'Email', 'required|is_unique[merchants.merchant_email]');
                            if($this->form_validation->run() == TRUE){
										
										$date = date("Y-m-d H:i:s");
										
										
										$arrData["merchant_name"] = $this->input->post('txtname');
										$arrData["merchant_email"] = $this->input->post('txtemail');
										$arrData["merchantshop_name"] = $this->input->post('txtshopname');
                            $address=$arrData["address"] = $this->input->post('txtaddress');
										$arrData["password_hash"] = md5($this->input->post('txtpassword'));
										$arrData['categories'] = implode($this->input->post('ddlcategory'), ",");
										//$arrData["merchant_contact"] = $this->input->post('txtmobile');
										//$arrData["division"] = implode($this->input->post('division'), ",");
                                        $arrData["merchant_contact"] = implode($this->input->post('txtmobile'), ",");
										$arrData["accountnumber"] = $this->input->post('txtaccount');
										$arrData["branch"] = $this->input->post('txtbranch');
										$arrData["ifsc"] = $this->input->post('txtifsc');
										$arrData["micr"] = $this->input->post('txtmicr');
										$arrData["pan"] = $this->input->post('txtpan');
										$arrData["pincode"] = $this->input->post('txtpin');
										$arrData["description"] = $this->input->post('txtadesc');
										$arrData["dateofincorporation"] = $this->input->post('txtincorporation');
										$arrData["created_date"] = $date;
										$arrData["profilepic"] = '';
										$config['upload_path'] = 'uploads/merchants/';
										$config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';

										$this->upload->initialize($config);
										if ($this->upload->do_upload('profilepic')) {
												$data=$this->upload->data();
												if($data['file_name']!=''){
													$arrData["profilepic"] = $data['file_name'];

												}
											}else{
						$error = array('error' => $this->upload->display_errors());
							
							$this->session->set_flashdata('error', 'Sorry!!!Some error has occured while uploading file.');

											}
											if ($this->upload->do_upload('kyc')) {
												$data=$this->upload->data();
												if($data['file_name']!=''){
													$arrData["kyc"] = $data['file_name'];

												}
											}else{
						$error = array('error' => $this->upload->display_errors());
						   
							$this->session->set_flashdata('error', 'Sorry!!!Some error has occured while uploading file.');

											}
                                       
								
								$prepAddr = str_replace(' ','+',$address);

								$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');

								$output= json_decode($geocode);
                                if($output->results){
								$arrData['latitude'] = $output->results[0]->geometry->location->lat;
								$arrData['longitude'] = $output->results[0]->geometry->location->lng;
                                 }else{
                                      $arrData['latitude']=0;
                                      $arrData['longitude']=0;
                                 }

 


										$insertedFlag = $this->Merchants_model->savemerchantDetails($arrData);

										if ($insertedFlag){
											$data['merchant_id'] =  $insertedFlag;
											$data['contact'] = $arrData["merchant_contact"];
											$data['status'] = 1;
											$this->Merchants_model->savemerchantcontactDetails($data);
											$arrData["created_date"] = $date; 
												$this->session->set_flashdata('success', 'Merchant Details Added Successfully !!');
												redirect('admin/merchants');
										}else{
												$this->session->set_flashdata('error', 'Failed to Add Merchant Details!!');
												redirect('admin/merchants/add');
										}
                        }else{

                            $this->session->set_flashdata('error', 'Failed to Add Merchant Details This Mobile Number Or Email Already Registered.!!');
                            redirect('admin/merchants/');

                        }
								
						}
				} else {
						$arrData['error'] = '';
				}
				$arrData['tab'] = $this->uri->segment(2);
				$arrData['title'] = 'Add Merchants';
				$arrData['middle'] = 'admin/merchants/add';
				$arrData['tab1']='';
				$arrData['categories'] = $this->Merchants_model->getcategoryDetails();

				$this->load->view('admin/template', $arrData);
		}
/**
	 * edit
	 *
	 * This help to edit user Details
	 *
	 * @author  Ketan Sangani
	 * @access  public
	 * @param   $iuserId
	 * @return  void
	 */

	public function edit($iuserId) {
				$arrData['portsDetailsArr'] = "";
				if ($_POST) {
								$policies_details = $this->Merchants_model->getmerchantDetails($iuserId);
								if ($this->input->post('submit')) {
												$date = date("Y-m-d H:i:s");
										$UpdateData["merchant_name"] = $this->input->post('txtname');
										$UpdateData["merchant_email"] = $this->input->post('txtemail');
										$UpdateData["merchantshop_name"] = $this->input->post('txtshopname');
										$address=$UpdateData["address"] = $this->input->post('txtaddress');
										$UpdateData["password_hash"] = md5($this->input->post('txtpassword'));
										$UpdateData['categories'] = implode($this->input->post('ddlcategory'), ",");
                                        $UpdateData["merchant_contact"] = implode($this->input->post('txtmobile'), ",");
									 
									    $UpdateData["bankname"] = $this->input->post('txtbank');
										$UpdateData["accountnumber"] = $this->input->post('txtaccount');
										$UpdateData["branch"] = $this->input->post('txtbranch');
										$UpdateData["ifsc"] = $this->input->post('txtifsc');
										$UpdateData["micr"] = $this->input->post('txtmicr');
										$UpdateData["pan"] = $this->input->post('txtpan');
										$UpdateData["pincode"] = $this->input->post('txtpin');
										$UpdateData["description"] = $this->input->post('txtadesc');
										$UpdateData["dateofincorporation"] = $this->input->post('txtincorporation');
										$UpdateData["modified_date"] = date("Y-m-d H:i:s");
								        $config['upload_path'] = 'uploads/merchants/';
										$config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';

										$this->upload->initialize($config);
										if($this->upload->do_upload('profilepic')){
												$data1=$this->upload->data();

												if($data1['file_name']!=''){
													$UpdateData["profilepic"] = $data1['file_name'];
													
												}
												
										 }else{
						$error = array('error' => $this->upload->display_errors());
							
							$this->session->set_flashdata('error', 'Sorry!!!Some error has occured while uploading file.');

											}
											if ($this->upload->do_upload('kyc')) {
												$data2=$this->upload->data();
												if($data2['file_name']!=''){
													$UpdateData["kyc"] = $data2['file_name'];

												}
											}else{
						$error = array('error' => $this->upload->display_errors());
							
							$this->session->set_flashdata('error', 'Sorry!!!Some error has occured while uploading file.');

											}
                                                $prepAddr = str_replace(' ','+',$address);

								$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
                                
								$output= json_decode($geocode);

                                if($output->results){
								$UpdateData['latitude'] = $output->results[0]->geometry->location->lat;
								$UpdateData['longitude'] = $output->results[0]->geometry->location->lng;
                                 }else{
                                      $UpdateData['latitude']=0;
                                      $UpdateData['longitude']=0;
                                 }


													$ports_details = $this->Merchants_model->getmerchantDetails($iuserId);
													$arrData['merchantdetails'] = $ports_details;
													$data['contact'] =  $UpdateData["merchant_contact"];
													$data["modified_date"] = date("Y-m-d H:i:s");
													$this->Merchants_model->update_merchantcontact($iuserId,$arrData['merchantdetails'][0]['merchant_contact'], $data);  
													$updateFlag = $this->Merchants_model->update_merchant($iuserId, $UpdateData);
												if ($updateFlag) {

																$this->session->set_flashdata('success', 'Merchants Details Updated Successfully !!');
																redirect('admin/merchants');
												}
												else{
																$this->session->set_flashdata('error', 'Failed to Update Merchants Details!!');
																redirect('admin/merchants/edit/' . $iuserId);
												}
				}
				else{
								$ports_details = $this->Merchants_model->getmerchantDetails($iuserId);
								$arrData['merchantdetails'] = $ports_details;
				}
				
	}
	$ports_details = $this->Merchants_model->getmerchantDetails($iuserId);
	$arrData['merchantdetails'] = $ports_details;
	$arrData['tab'] = $this->uri->segment(2);
	$arrData['tab1']='';
	$arrData['categories'] = $this->Merchants_model->getcategoryDetails();

		$arrData['title'] = 'Edit Merchants';
		$arrData['middle'] = 'admin/merchants/edit';
		$this->load->view('admin/template', $arrData);
}

/**
		 * do_upload
		 *
		 * uploads the image files
		 * 
		 * @author Amit Salunkhe
		 * @access public
		 * @param String - Image container name
		 * @return array - Image data/Error
		 */
		function do_upload($fieldName) {
				$config['upload_path'] = 'uploads/';
				$config['allowed_types'] = 'gif|jpg|png';

				$this->upload->initialize($config);

				if (!$this->upload->do_upload($fieldName)){
						$error = array("error" => $this->upload->display_errors());
						return $error;
				} else {
						$data = $this->upload->data();
						return $data;
				}
		}
	/**
	 * delete
	 *
	 * This help to delete merchant Details 
	 *
	 * @author  Ketan Sangani
	 * @access  public
	 * @param   $iuserId
	 * @return  void
	 */
	public function delete($iuserId) {
				$delete = $this->Merchants_model->delete_merchant($iuserId);
				if ($delete)
								$this->session->set_flashdata('success', 'Merchant Details Deleted Successfully !!');
				else
								$this->session->set_flashdata('error', 'Failed to Delete Merchant Details!!');
				redirect('admin/merchants');
	}
	/**
	 * publish
	 *
	 * This help to publish merchant Details
	 *
	 * @author  Ketan Sangani
	 * @access  public
	 * @param   $icategoryId
	 * @return  void
	 */

	public function publish($icategoryId) {
				$delete = $this->Merchants_model->set_merchant($icategoryId);
	}

	 /**
	 * unpublish
	 *
	 * This help to unpublish merchant Details
	 *
	 * @author  Ketan Sangani
	 * @access  public
	 * @param   $icategoryId
	 * @return  void
	 */
	public function unpublish($icategoryId) {
			 $delete = $this->Merchants_model->unset_merchant($icategoryId);
	}
/**
		 * category
		 *
		 * display merchant category list
		 * 
		 * @author Ketan Sangani
		 * @access public
		 * @param none 
		 * @return void
		 */
		public function category() {
				
				$arrData['tab'] = $this->uri->segment(2);
				$arrData['tab1'] = $this->uri->segment(3);
				$arrData['title'] = 'Merchants Category List';
				$arrData['middle'] = 'admin/category/list';
				
				$arrData['categories'] = $this->Merchants_model->getcategoryDetails();
				$this->load->view('admin/template', $arrData);
		}

		/**
		 * add
		 *
		 * Saves the merchant category details to database
		 * 
		 * @author Amit Salunkhe
		 * @access public
		 * @param none
		 * @return void
		 */
		public function addcategory() {
			 
						if ($this->input->post('submit')) {
						 
										$arrData["category"] = $this->input->post('txtname');
										$arrData["desc"] = $this->input->post('desc');
										$arrData["created_date"] = date("Y-m-d H:i:s");
										$config['upload_path'] = 'uploads/category/';
										$config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';
                                        
										$this->upload->initialize($config);
										if ($this->upload->do_upload('file')) {
												$data=$this->upload->data();
												if($data['file_name']!=''){
													$arrData["category_image"] = $data['file_name'];

												}
											}else{
						$error = array('error' => $this->upload->display_errors());
							var_dump($error);exit;
							$this->session->set_flashdata('error', 'Sorry!!!Some error has occured while uploading file.');
                                redirect('admin/merchants/category');
											}
										$insertedFlag = $this->Merchants_model->savecategoryDetails($arrData);
										if ($insertedFlag) {
												$this->session->set_flashdata('success', 'Category Details Added Successfully !!');
												redirect('admin/merchants/category');
										} else {
												$this->session->set_flashdata('error', 'Failed to Add Category Details!!');
												redirect('admin/merchants/addcategory');
										}
								}
						
				else {
						$this->load->view('admin/category/add');
				}
				
		}
/**
	 * edit
	 *
	 * This help to edit category Details
	 *
	 * @author  Ketan Sangani
	 * @access  public
	 * @param   $iuserId
	 * @return  void
	 */

	public function editcategory($iuserId) {
				
				
								
								if ($this->input->post('submit')) {
												$date = date("Y-m-d H:i:s");
										$UpdateData["category"] = $this->input->post('txtname');
										$UpdateData["desc"] = $this->input->post('desc');

										$UpdateData["modified_date"] = date("Y-m-d H:i:s");
										$config['upload_path'] = 'uploads/category/';
										$config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';

										$this->upload->initialize($config);
										if($this->upload->do_upload('file')){
												$data1=$this->upload->data();

												if($data1['file_name']!=''){
													$UpdateData["category_image"] = $data1['file_name'];
													
												}
												
										 }else{
						$error = array('error' => $this->upload->display_errors());
							
							$this->session->set_flashdata('error', 'Sorry!!!Some error has occured while uploading file.');

											}
										
												$updateFlag = $this->Merchants_model->update_category($iuserId, $UpdateData);
												if ($updateFlag) {
																$this->session->set_flashdata('success', 'Category Details Updated Successfully !!');
																redirect('admin/merchants/category');
												}
												else{
																$this->session->set_flashdata('error', 'Failed to Update Category Details!!');
																redirect('admin/merchants/editcategory/' . $iuserId);
												}
								}
				else{
							$data['category'] = $this->Merchants_model->getcategoryDetails($iuserId);
							
					 $this->load->view('admin/category/edit', $data);
				}
			
}
/**
		 * index
		 *
		 * display merchants list
		 * 
		 * @author Ketan Sangani
		 * @access public
		 * @param none 
		 * @return void
		 */
		public function contacts($merchantid) {
				if ($this->input->post('delete')) {
								$multiDelete = $this->Merchants_model->multi_delete_merchants($this->input->post('delete'));
								
								if ($multiDelete)
												$this->session->set_flashdata('success', 'Deleted !!');
								else
												$this->session->set_flashdata('error', 'Failed to Delete!!');
								
								 redirect('admin/merchants');
				}
				$arrData['tab'] = $this->uri->segment(2);
				$arrData['title'] = 'Merchants Contacts';
				$arrData['middle'] = 'admin/merchants/contactslist';
				$arrData['tab1']='';

				$arrData['contacts'] = $this->Merchants_model->getmerchantcontactsDetails($merchantid);
				
				$this->load->view('admin/template', $arrData);
		}
 /**
	 * addcontact
	 *
	 * Saves the merchnat contact details to database
	 * 
	 * @author Ketan sangani
	 * @access public
	 * @param integer-$merchantsid
	 * @return void
	 */
	public function addcontact($merchantid) {
		
			if ($this->input->post('submit')) {
					 $arrData["merchant_id"] = $merchantid;
					 //$arrData["empid"] = $this->input->post('txtempid');
					 $UpdateData['employeename'] = $arrData["employeename"] = $this->input->post('txtempname');
					 $UpdateData['contact'] = $arrData["contact"] = $this->input->post('txtmobile');
 
					$UpdateData['modified_date'] = $arrData["created_date"] = date("Y-m-d H:i:s");
					$query = $this->db->query('SELECT MIN(cindex) as cindex,id FROM  otap_merchants_contacts where isDeleted=1 AND merchant_id='.$merchantid.' 	
');
					//echo $query;exit;
					$result = $query->result_array();
					//var_dump($result);exit;
                     if($result[0]['cindex']!=NULL)
{                        $UpdateData['isDeleted'] = 0;

 $insertedFlag = $this->Merchants_model->update_merchantcontactdetails($result[0]['id'],$UpdateData);
 if ($insertedFlag) {
						$this->session->set_flashdata('success', 'Merchant Contact Details Updated Successfully !!');
						redirect('admin/merchants/contacts/'.$merchantid);
					} else {
						$this->session->set_flashdata('error', 'Failed to Updated Merchant Contact Details!!');
						redirect('admin/merchants/contacts/'.$merchantid);
					}
}else{
$query1 = $this->db->query('SELECT MAX(cindex) as cindex FROM  otap_merchants_contacts where isDeleted=0 AND merchant_id='.$merchantid.'
');
					$result1 = $query1->result_array();
 if($result1[0]['cindex']!=NULL)
{
$arrData["cindex"] = $result1[0]['cindex'] +1;
$arrData['isDeleted'] = 0;
$insertedFlag1 = $this->Merchants_model->savemerchantcontactDetails($arrData);
if ($insertedFlag1) {
						$this->session->set_flashdata('success', 'Merchant Contact Details Added Successfully !!');
						redirect('admin/merchants/contacts/'.$merchantid);
					} else {
						$this->session->set_flashdata('error', 'Failed to Add Merchant Contact Details!!');
						redirect('admin/merchants/contacts/'.$merchantid);
					}
}else{
$arrData["cindex"] = 1;
$arrData['isDeleted'] = 0;
$insertedFlag2 = $this->Merchants_model->savemerchantcontactDetails($arrData);
if ($insertedFlag2) {
						$this->session->set_flashdata('success', 'New Merchant Contact Details Added Successfully !!');
						redirect('admin/merchants/contacts/'.$merchantid);
					} else {
						$this->session->set_flashdata('error', 'Failed to Add New Merchant Contact Details!!');
						redirect('admin/merchants/contacts/'.$merchantid);
					}
}
}

					
					
				}
			
		else {
			
					$this->load->view('admin/merchants/addcontact'); 

		}
	   
	}
/**
   * edit
   *
   * This help to edit zone Details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   $iuserId
   * @return  void
   */

  public function editcontact($merchantid,$iuserId) {
		
		
				
				if ($this->input->post('submit')) {
						$date = date("Y-m-d H:i:s");
					
						$data["merchant_contact"]=$UpdateData["contact"] = $this->input->post('txtmobile');                    
					$data["modified_date"]=$UpdateData["modified_date"] = date("Y-m-d H:i:s");
					//$UpdateData["empid"] = $this->input->post('txtempid');
					 $UpdateData["employeename"] = $this->input->post('txtempname');
					//$contact = $this->Merchants_model->getmerchantcontactsDetails($merchantid,$iuserId);
						$updateFlag = $this->Merchants_model->update_merchantcontactdetails($iuserId, $UpdateData);
						if ($updateFlag) {
							//$this->Merchants_model->update_merchantdetails($merchantid,$contact[0]['contact'],$data);
								$this->session->set_flashdata('success', 'Merchant Contact Details Updated Successfully !!');
								redirect('admin/merchants/contacts/'.$merchantid);
						}
						else{
								$this->session->set_flashdata('error', 'Failed to Update Merchant Contact Details!!');
								redirect('admin/merchants/contacts/'.$merchantid);
						}
			   
		
		
  }else{
	$data['contact'] = $this->Merchants_model->getmerchantcontactsDetails($merchantid,$iuserId);
	$this->load->view('admin/merchants/editcontact',$data);
  }
  
  
}
/**
   * deletecontact
   *
   * This help to delete employee contact Details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   $iuserId
   * @return  void
   */

  public function deletecontact($iuserId,$merchantid) {
		
		
				
				
						$date = date("Y-m-d H:i:s");
					
					$UpdateData["contact"] ='';                    
                    $UpdateData["modified_date"] = date("Y-m-d H:i:s");
					$UpdateData["isDeleted"] =1;
					 $UpdateData["employeename"] = '';
					//$contact = $this->Merchants_model->getmerchantcontactsDetails($merchantid,$iuserId);
						$updateFlag = $this->Merchants_model->update_merchantcontactdetails($iuserId, $UpdateData);
						if ($updateFlag) {
							//$this->Merchants_model->update_merchantdetails($merchantid,$contact[0]['contact'],$data);
								$this->session->set_flashdata('success', 'Employee Details Deleted Successfully !!');
								redirect('admin/merchants/contacts/'.$merchantid);
						}
						else{
								$this->session->set_flashdata('error', 'Failed to Update Employee Details!!');
								redirect('admin/merchants/contacts/'.$merchantid);
						}
			   
		
		
  
  
  
}
	
		
	 
	/**
	 * publishcontact
	 *
	 * This help to publish merchant contact Details
	 *
	 * @author  Ketan Sangani
	 * @access  public
	 * @param   $icategoryId
	 * @return  void
	 */

	public function publishcontact($icategoryId) {
				$delete = $this->Merchants_model->set_contact($icategoryId);
	}

	 /**
	 * unpublishcontact
	 *
	 * This help to unpublish merchant contact Details
	 *
	 * @author  Ketan Sangani
	 * @access  public
	 * @param   $icategoryId
	 * @return  void
	 */
	public function unpublishcontact($icategoryId) {
			 $delete = $this->Merchants_model->unset_contact($icategoryId);
	}
/**
		 * requests
		 *
		 * display profile update requests of merchants list
		 * 
		 * @author Ketan Sangani
		 * @access public
		 * @param none 
		 * @return void
		 */
		public function requests() {
				
				$arrData['tab'] = $this->uri->segment(2);
				$arrData['title'] = 'Merchants';
				$arrData['middle'] = 'admin/merchants/requestslist';
				$arrData['tab1']=$this->uri->segment(3);
				

				$arrData['merchants'] = $this->Merchants_model->getupdaterequests();
				//var_dump($arrData['customers']);exit;
				$this->load->view('admin/template', $arrData);
		}
		/**
		 * approveupdate
		 *
		 * update profile of merchants
		 * 
		 * @author Ketan Sangani
		 * @access public
		 * @param none 
		 * @return void
		 */
		public function approveupdate($id,$merchantid) {
				
				$arrData['tab'] = $this->uri->segment(2);
				$arrData['title'] = 'Merchants';
				
				$arrData['tab1']='';
				$requestsdetails = $this->Merchants_model->getupdaterequests($id);
				if($requestsdetails){
                
                $UpdateData["merchant_name"] = $requestsdetails[0]['merchant_name'];
				
				
			 $UpdateData["address"] = $requestsdetails[0]['address'];
				
			 $UpdateData["merchant_contact"] = $requestsdetails[0]['merchant_contact'];
			
			 $UpdateData["profilepic"] = $requestsdetails[0]['profilepic'];
			 
			 $UpdateData["modified_date"] = date("Y-m-d H:i:s");
			 $ports_details = $this->Merchants_model->getmerchantDetails($merchantid);
			 
				$insertedFlag = $this->Merchants_model->update_merchant($merchantid,$UpdateData);
                $arrData['merchantdetails'] = $ports_details;
				$data['contact'] =  $requestsdetails[0]['merchant_contact'];
				
				$data["modified_date"] = date("Y-m-d H:i:s");
				
				if ($insertedFlag) {
				
                        $query = $this->db->query("DELETE FROM otap_merchantscache WHERE id='$id'");
						$this->session->set_flashdata('success', 'Merchant Details Updated Successfully !!');
						redirect('admin/merchants/');
					} else {
						$this->session->set_flashdata('error', 'Failed to Update Merchant  Details!!');
						redirect('admin/merchants/');
					}
		}
				
		}
}

/* End of file Merchants.php */
/* Location: ./application/controllers/admin/Merchants.php */
