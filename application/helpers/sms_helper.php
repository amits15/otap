<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * Supply Chain Exchange SMS Helper
 *
 * @subpackage  Helpers
 * @category    Helpers
 * @author  ROHINI BAJPEI
 */

// ------------------------------------------------------------------------

/**
 * This function is use for send sms using mVaayoo API
 *
 * @access     public
 * @param       String $text 
 * @param       Array $gsm array of phone number
 * @return      String Json encode string
 */
if (!function_exists('send_sms')) {

    function send_sms($text = null, $gsm = array()) {
          
        // Get current CodeIgniter instance
        $CI = & get_instance();
        if (!defined('SMS_USERNAME'))
            exit('Username is not set.');
        if (!defined('SMS_PASSWORD'))
            exit('Password is not set.');
        if (!defined('SMS_API_URL'))
            exit('API url is not defined.');
        if (!defined('SMS_SENDER'))
            exit('Sender is not defined.');
        if (!defined('SMS_STATE'))
            exit('State is not defined.');
        if (!defined('SMS_DCS'))
            exit('DCS is not defined.');

        if ($text == null || empty($gsm))
            exit('Please pass appropriate parameters.');
		else
			$recepients = implode(',', array_filter($gsm));
            

		$parameters_string = "user=".SMS_USERNAME." : ".SMS_PASSWORD." &senderID=".SMS_SENDER." &receipientno=".$recepients." &dcs=".SMS_DCS." &msgtxt=".$text." &state=".SMS_STATE;
	    
		 $url = SMS_API_URL.'?'.$parameters_string;
		 //echo $url; exit;
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, SMS_API_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters_string);
        $response = curl_exec($ch);
        
		if(curl_errno($ch))
		{
			echo 'error:' . curl_error($ch); exit;
		}

        curl_close($ch);
        return $response;

    }
}
if (!function_exists('send_otp')) 
{

    function send_otp($text = null, $num) 
    {

        $CI = & get_instance();
        if (!defined('SMS_UNAME'))
            exit('Userid is not set.');
        if (!defined('SMS_PASS'))
            exit('Password is not set.');
        
        $request =""; //initialise the request variable
$param['method']= "sendMessage";
$param['send_to'] =  $num;
$param['msg'] = $text;
$param['userid'] = SMS_UNAME;
$param['password'] = SMS_PASS;
$param['v'] = "1.1";
$param['msg_type'] = "TEXT"; //Can be "FLASH”/"UNICODE_TEXT"/”BINARY”
$param['auth_scheme'] = "PLAIN";
//Have to URL encode the values
foreach($param as $key=>$val) {
$request.= $key."=".urlencode($val);
//we have to urlencode the values
$request.= "&";
//append the ampersand (&) sign after each
//parameter/value pair
}
$request = substr($request, 0, strlen($request)-1);
//remove final (&) sign from the request
$url =
"http://enterprise.smsgupshup.com/GatewayAPI/rest?".$request;
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$curl_scraped_page = curl_exec($ch);
curl_close($ch);
//echo $curl_scraped_page;
		
    }
}
/* End of file sms_helper.php */
/* Location: ./application/helpers/sms_helper.php */