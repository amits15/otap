<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * User Model
 *
 * This class is used to interact with users table.
 *
 * @author  Ketan Sanganni
 * @sub-package Models
 * @category Models
 */
class User_model extends CI_Model {

	

	/**
	 * Constructor
	 *
	 * Calls parent constructor
	 */
	function __construct()
	{
		// Initialization of class
		parent::__construct();
		
	}
    /**
    * getuserDetails
    *
    * retrievs the users details based on userid
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $userid
    *@return array 
    */
    function getuserDetails($userid =0)
    {
        if($userid!=0){
            $this->db->where('id', $userid);
        }
         $this->db->select('name,username,contact,email,password_hash,address,id');
        $objQuery = $this->db->get('users');
        return $objQuery->result_array();
    }

    /**
    * getUserInfo
    *
    * retrievs the users details based on userid
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $userid
    *@return array 
    */
    function getUserInfo($userid)
    {
        
        $this->db->where('id', $userid);
        $this->db->select('id as userid,name,username,contact as mobile ,email,address,profilepic as image');
        $objQuery = $this->db->get('users');
        return $objQuery->row_array();
    }

	/**
     * registeruser
     *
     * This is for register of the User
     *
     * @author Ketan Sangani
     * @access  public
     * @param   array-$data
     * @return  void
     */
	public function registeruser($data)
	{
		if($this->db->insert('users', $data)){
          return $this->db->insert_id();
    }else{
      return false;
    }
	}
     /**
   * updateuser
   *
   * This is used to update  users details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData, integer-$iNewfeedId
   * @return boolean
   */
  function updateuser($arrData,$iNewfeedId){

    $this->db->where('id',$iNewfeedId);
    if($this->db->update('users', $arrData))
    {
    
        return true;
    }
    else
    {
        return false;
    }
  }
   /**
    * getalreadymerchantexist
    *
    * check merchant already registered with email or mobile number
    * 
    *@author Ketan Sangani
    *@access public
    *@param string -$email,$mobile
    *@return array 
    */
    function getalreadyuserexist($email,$mobile)
            
    {
      $where = "email='$email' OR contact='$mobile'";
        $this->db->select('*');
        $this->db->where($where);
        //$this->db->where('merchant_contact',$mobile);
        ///$this->db->where('verified',1);
        $this->db->limit(1);
        $result = $this->db->get('users');
      //echo $this->db->last_query();exit;
        return $result->result_array();
    }
  
	/**
     * loginuser
     *
     * This is for login of the User
     *
     * @author Ketan Sangani
     * @access  public
     * @param   array-$data
     * @return  void
     */
    public function loginuser($username,$password)
    {
       $result = $this->get_user($username,$password);
            
            /*--------Send OTP Sms ----------------*/
            
    
            if ($result->num_rows() > 0)
            { 
                //if login credentials are correct set session variables
            
            $row = $result->row_array();
            if($row['verified']==1){
                
                $arrResponse['isValid']  = true;
                $arrResponse['isVerified'] = true;
                $arrResponse['info'] = array('userid' => $row['id'] ,'name'=>$row['name'],'username'=>$row['username'], 'address'=>$row['address'], 'image'=>$row['profilepic'],'mobile'=>$row['contact'],'email'=>$row['email'],'pushon'=>$row['pushon'],'smson'=>$row['smson']);
                $arrResponse['message'] = "You Have succesfully logged in";
            }elseif($row['verified']==0){
                $OTP = 9999;   
            //$OTP = mt_rand(100000,999999);
            $this->load->helper('sms_helper');      
            $message = "Dear User,\nYour OTP is $OTP\nThank you.";
            //$response = send_otp($message,  $param['mobile']);
    
                
                $this->db->where('id', $row['id']);
                $update = $this->db->update('users', array('user_pin' => $OTP));
                if($update){
            $arrResponse['isValid'] = true;
            $arrResponse['isVerified'] = false;
            $arrResponse['message'] = 'OTP sends On Your Mobile Number please verify it';
            $arrResponse['info'] = array('userid' =>  $row['id'] ,'mobile'=>$row['contact']);
                
                 }
            }
            }
            else
            {  
            
            $arrResponse['isValid']  = false;
            $arrResponse['message'] = "Your Username Or Password are incorrect";         
                
             }

             echo json_encode($arrResponse);
    }
   /**
     * get_user
     *
     * This is for retrievs details of user
     *
     * @author Ketan Sangani
     * @access  public
     * @param   string-$username,$password
     * @return  void
     */
    function get_user($username,$password)
            
    {
        $this->db->select('*');
        $this->db->where('username',$username);
        $this->db->where('password_hash',MD5($password));
        
        $this->db->limit(1);
        $result = $this->db->get('users');
    
        return $result;
    }
    /**
     * forgot_password
     *
     * This is for retrievs details of user for forgot password
     *
     * @author Ketan Sangani
     * @access  public
     * @param   string-$username,$password
     * @return  void
     */
    function forgot_password($email)
            
    {
        $this->db->select('*');
        $this->db->where('email',$email);
        
        $q = $this->db->get('users');

        if($q->num_rows() == 0){
            return false;
        }
        else{
            $token = md5(uniqid(rand(), true));
              $this->db->where('email',$email);
            $update_flag = $this->db->update('users', array('token' => $token));
            
            
            if($update_flag){
                $this->db->where('email',$email);
                $q = $this->db->get('users');
                return $q->row_array();
            }
        }
    
       
    }

    /* check_token
     *
     * This function checks if the token is valid for a user.
     *
     * @param string
     * @return boolean
     */
    public function check_token($token)
    {
        $url = base64_decode(urldecode($token));
        $url_chunks = explode('/',$url);
        $this->db->where('id',$url_chunks['1']);
        $q = $this->db->get('users');

        if($q->num_rows()> 0){
            $result = $q->row_array();
            
            if($result['token']==$url_chunks['0'])
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    /**
     * update_reset_password
     *
     * resets password for forgot pasword process
     *
     * @param array
     * @return boolean
     */
    public function update_reset_password($data)
    {
        $this->db->where('id', $data['id']);
        $update_flag = $this->db->update('users', array('password_hash' => md5($data['password']), 'token' => ''));
        if($update_flag)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
	/**
     * verifyUser
     *
     * This is for Verification of the User
     *
     * @author  Ketan Sangani
     * @access  public
     * @param   string $u, string $pw
     * @return  void
     */
    function verifyUser($u,$pw){
        
        // Set the Select parameter to return adminID and adminName column values of admin table
        $this->db->select('id,name,employee_password');

        // Sets the where constraint to fetch the record having adminName as the username ($u) passed by the user
        $this->db->where('employee_user_name',$u);

        // Sets the where constraint to fetch the record having adminPassword as the password ($pw) passed by the user. The password is encrypted using MD5 algorithm and is saved in the database in the encrypted format
        //$this->db->where('adminUserPassword',MD5($pw));
        $this->db->where('employee_password',MD5($pw));

        // Sets the limit constraint to fetch 1 record
        $this->db->limit(1);

        // Fetch record from the admin table
        $Q = $this->db->get('employees');
        //echo $this->db->last_query();
        //exit;
        // Count if there are any rows returned
        if ($Q->num_rows() > 0){

            // Return the result in the form of an array
            $row = $Q->row_array();

            // This allows the user with correct login details to log into the site and a session is set
            $ses_user = array("username"=>$row['employee_user_name'],"password"=>$row['employee_password'], "id"=>$row['employee_id'],"logged_in"=>TRUE,"privileges"=>'All');
            $this->session->set_userdata($ses_user);
        }else{
        
                // This will give an error message to the user for incorrect login or password details.
                $ses_user = array("username"=>"","id"=>0,"logged_in"=>FALSE);
                $this->session->set_userdata($ses_user);
                $this->session->set_flashdata('error', 'Sorry, your username or password is incorrect!');
        }
    
    }
    /**
     * verify_otp
     *
     * This is for pin Verification of the User
     *
     * @author  Ketan Sangani
     * @access  public
     * @param   string $u, string $pw
     * @return  void
     */
    function verify_otp($otp,$userid)
    {
        if($otp != '' && strlen($otp)== 4 )
        {
        $this->db->select('*');
        $this->db->where('id', $userid);                
        $this->db->limit(1);
        $result = $this->db->get('users');
        if ($result->num_rows() > 0)
            { 
                $row = $result->row_array();
                if($row['user_pin'] == $otp)
                {  
                $this->db->where('id', $userid);
                  $update = $this->db->update('users', array('verified' => 1));
                   if($update){                  
                  $res['isValid']  = true;
                  $res['isVerified'] = true;
                  $res['info'] = array('userid' => $row['id'] ,'name'=>$row['name'],'username'=>$row['username'],'image'=>$row['profilepic'],'mobile'=>$row['contact'],'email'=>$row['email'],'pushon'=>$row['pushon'],'smson'=>$row['smson'],'address'=>$row['address']);

                  $res['message'] = "You Have Signup Succesfully";
                  }   
                }
                else
                {
                    $res['isValid']=false;
                    $res['message']='Please enter correct Pin number';
                    
                }
            }
    
        }
        else 
        {
            $res['isValid']=false;
            $res['message']='Enter Valid Pin number';
            
        }
        
        echo json_encode($res);
    }
    
	/**
     * checkuserdeviceregistered
     *
     * This is for check user registered with device
     *
     * @author Ketan Sangani
     * @access  public
     * @param   string-$username,$password
     * @return  void
     */
    function checkuserdeviceregistered($userid)
            
    {
        $this->db->select('*');
        $this->db->where('userid',$userid);
                
        $this->db->limit(1);
        $result = $this->db->get('userdevices');
        
        return $result->row_array();
    }
    /**
     * registeruserdevice
     *
     * This is for register device of the User
     *
     * @author Ketan Sangani
     * @access  public
     * @param   array-$data
     * @return  void
     */
    public function registeruserdevice($data)
    {
        if($this->db->insert('userdevices', $data)){
          return true;
    }else{
      return false;
    }
    }
     /**
   * updateregisteredevice
   *
   * This is used to update  device  details of registered user
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData, integer-$iNewfeedId
   * @return boolean
   */
  function updateregisteredevice($arrData,$iNewfeedId){

    $this->db->where('userid',$iNewfeedId);
    if($this->db->update('userdevices', $arrData))
    {
    
        return true;
    }
    else
    {
        return false;
    }
  }
    /**
   * unregisterDevice
   *
   * This is used to unregister the device
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId,string - $platform
   * @return boolean
   */
  function unregisterDevice($platform,$iNewfeedId){

    $this->db->where('userid',$iNewfeedId);
    $this->db->where('platform',urldecode($platform));
    if($this->db->delete('userdevices'))
    {
        return true;
    }
    else
    {
        return false;
    }
  }
}