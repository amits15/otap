<?php
/**
 * Safe Doc
 *
 * @Description  This class is used to interact with the admin table using Codeignitor db core class. All the Data Insert,Retrival and Update operations related to admin are performed here.
 *
 * @package		Safe Doc
 * @subpackage  Model
 * @author		Manish Dubey
 * @copyright	Copyright (c) 2012 - 2013
 * @since		Version 1.0
 */

// ------------------------------------------------------------------------

/**
 *
 * This is Login Model
 *
 * @author Ashok Jadhav
 * @package Codeigniter
 * @subpackage	Model
 */

class Login_model extends CI_Model{

    // --------------------------------------------------------------------

    /**
     * __construct
     *
     * Calls parent constructor
     * @author	Ashok Jadhav
     * @access	public
     * @return	void
     */
    function __construct()
    {
        // Initialization of class
        parent::__construct();
    }



    /**
     * verifysiteUser
     *
     * This is for Verification of the  User
     *
     * @author  Ketan Sangani
     * @access  public
     * @param   string $u, string $pw
     * @return  void
     */
    function verifysiteUser($u,$pw){
        
        // Set the Select parameter to return adminID and adminName column values of admin table
        $this->db->select('users.*,company.status');
        $this->db->join('company','company.company_id=users.company_id');
        // Sets the where constraint to fetch the record having adminName as the username ($u) passed by the user
        $this->db->where('username',$u);
       $this->db->where('company.status',"1");
       $this->db->where('users.status',"1");
        // Sets the where constraint to fetch the record having adminPassword as the password ($pw) passed by the user. The password is encrypted using MD5 algorithm and is saved in the database in the encrypted format
        //$this->db->where('adminUserPassword',MD5($pw));
        $this->db->where('password_hash',MD5($pw));

        // Sets the limit constraint to fetch 1 record
        $this->db->limit(1);

        // Fetch record from the admin table
        $Q = $this->db->get('users');
        ///echo $this->db->last_query();
        //exit;
        // Count if there are any rows returned
        if ($Q->num_rows() > 0){

            // Return the result in the form of an array
            $row = $Q->row_array();
            
            // This allows the user with correct login details to log into the site and a session is set
            $ses_user = array("susername"=>$row['username'],"spassword"=>$row['password_hash'], "sid"=>$row['id'],"companyid"=>$row['company_id'],"division"=>$row['division'],"slogged_in"=>TRUE);
            $this->session->set_userdata($ses_user);
        }else{
        
                // This will give an error message to the user for incorrect login or password details.
                $ses_user = array("susername"=>"","sid"=>0,"companyid"=>"","division"=>"","slogged_in"=>FALSE);
                $this->session->set_userdata($ses_user);
                $this->session->set_flashdata('error', 'Sorry, your username or password is incorrect!');
        }
    
    }
    /**
     * verifyUser
     *
     * This is for Verification of the User
     *
     * @author  Ashok Jadhav
     * @access  public
     * @param   string $u, string $pw
     * @return  void
     */
    function verifyUser($u,$pw){
        
        // Set the Select parameter to return adminID and adminName column values of admin table
        $this->db->select('employee_id,employee_user_name,employee_password');

        // Sets the where constraint to fetch the record having adminName as the username ($u) passed by the user
        $this->db->where('employee_user_name',$u);

        // Sets the where constraint to fetch the record having adminPassword as the password ($pw) passed by the user. The password is encrypted using MD5 algorithm and is saved in the database in the encrypted format
        //$this->db->where('adminUserPassword',MD5($pw));
        $this->db->where('employee_password',MD5($pw));

        // Sets the limit constraint to fetch 1 record
        $this->db->limit(1);

        // Fetch record from the admin table
        $Q = $this->db->get('employees');
        //echo $this->db->last_query();
        //exit;
        // Count if there are any rows returned
        if ($Q->num_rows() > 0){

            // Return the result in the form of an array
            $row = $Q->row_array();

            // This allows the user with correct login details to log into the site and a session is set
            $ses_user = array("username"=>$row['employee_user_name'],"password"=>$row['employee_password'], "id"=>$row['employee_id'],"logged_in"=>TRUE,"privileges"=>'All');
            $this->session->set_userdata($ses_user);
        }else{
        
                // This will give an error message to the user for incorrect login or password details.
                $ses_user = array("username"=>"","id"=>0,"logged_in"=>FALSE);
                $this->session->set_userdata($ses_user);
                $this->session->set_flashdata('error', 'Sorry, your username or password is incorrect!');
        }
    
    }
}


/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */