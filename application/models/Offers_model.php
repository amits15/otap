<?php
/**
 * Safe Doc
 *
 * @Description  This class is used to interact with the admin table using Codeignitor db core class. All the Data Insert,Retrival and Update operations related to admin are performed here.
 *
 * @package Safe Doc
 * @subpackage  Model
 * @author Amit Salunkhe
 * @copyright	Copyright (c) 2015
 * @since Version 1.0
 */

// ------------------------------------------------------------------------

/**
 *
 * This is Offers Model
 *
 * @author Ketan Sangani
 * @package Codeigniter
 * @subpackage	Model
 */

class Offers_model extends CI_Model{

    // --------------------------------------------------------------------

   /**
    * __construct
    *
    * Calls parent constructor
    * @author	Ketan Sangani
    * @access	public
    * @return	void
    */
    function __construct()
    {
        // Initialization of class
        parent::__construct();
    }
    
  
   /**
    * getoffersDetails
    *
    * retrievs the list of offers details
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $offerId
    *@return array 
    */
    function getoffersDetails($offerId = False)
    {
        if($offerId){
            $this->db->where('offer_id', $offerId);
        }
        $this->db->select('offers.*,merchants.merchant_name,offer_types.offer_type');
        $this->db->from('offers');
        $this->db->join('merchants','merchants.merchant_id=offers.merchant_id');
        $this->db->join('offer_types','offer_types.id=offers.type');
        $objQuery = $this->db->get();
        return $objQuery->result_array();
    }
    /**
    * getrequestsofcreateoffers
    *
    * retrievs the list of requests of create offers details
    * 
    *@author Ketan Sangani
    *@access public
    *@param none
    *@return array 
    */
    function getrequestsofcreateoffers($id=False)
    {
       if($id){
            $this->db->where('offerscache.id', $id);
        }
        $this->db->select('offerscache.*,merchants.merchant_name,offer_types.offer_type');
        $this->db->from('offerscache');
        $this->db->join('merchants','merchants.merchant_id=offerscache.merchant_id');
        $this->db->join('offer_types','offer_types.id=offerscache.type');
         $this->db->where('crudtype','add');
        $objQuery = $this->db->get();
        return $objQuery->result_array();
    }
     /**
    * getrequestsofupdateeoffers
    *
    * retrievs the list of requests of update offers details
    * 
    *@author Ketan Sangani
    *@access public
    *@param none
    *@return array 
    */
    function getrequestsofupdateoffers($id=False)
    {
       if($id){
            $this->db->where('offerscache.id', $id);
        }
        $this->db->select('offerscache.*,merchants.merchant_name,offer_types.offer_type');
        $this->db->from('offerscache');
        $this->db->join('merchants','merchants.merchant_id=offerscache.merchant_id');
        $this->db->join('offer_types','offer_types.id=offerscache.type');
        $this->db->where('crudtype','edit');
        $objQuery = $this->db->get();
        return $objQuery->result_array();
    }
     /**
    * getrequestsofdeleteoffers
    *
    * retrievs the list of requests of delete offers details
    * 
    *@author Ketan Sangani
    *@access public
    *@param none
    *@return array 
    */
    function getrequestsofdeleteoffers()
    {
       
        $this->db->select('offers.*,offerscache.id,offerscache.offer_id,offerscache.merchant_id as merchant,merchants.merchant_name,offer_types.offer_type');
        $this->db->from('offerscache');
        $this->db->join('offers','offers.offer_id=offerscache.offer_id');
        $this->db->join('merchants','merchants.merchant_id=offerscache.merchant_id');
        
        $this->db->join('offer_types','offer_types.id=offers.type');
         $this->db->where('offerscache.crudtype','delete');
        $objQuery = $this->db->get();
        //echo $this->db->last_query();exit;
        return $objQuery->result_array();
    }
    /**
    * getofferDetail
    *
    * retrievs the offer details based on offerid
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $offerId
    *@return array 
    */
    function getofferDetail($offerId)
    {
        $this->db->select('offers.offer_id,offers.title,offers.image,offers.description,offers.start_date,offers.end_date,offers.amount,offers.offer_discount,merchants.merchant_name,offer_types.offer_type');
        $this->db->from('offers');
        $this->db->join('merchants','merchants.merchant_id=offers.merchant_id');
        $this->db->join('offer_types','offer_types.id=offers.type');
        $this->db->where('offer_id', $offerId);
        $objQuery = $this->db->get();
        return $objQuery->result_array();
    }
     /**
    * offerslist
    *
    * retrievs the offers details
    * 
    *@author Ketan Sangani
    *@access public
    *@param none
    *@return array 
    */
    function getofferslist($offset)
    {
        
        $this->db->select('offers.offer_id,offers.title,offers.description,offers.start_date,offers.end_date,offers.amount,offers.offer_discount,,merchants.merchant_name,offer_types.offer_type');
        $this->db->from('offers');
        $this->db->join('merchants','merchants.merchant_id=offers.merchant_id','left');
        $this->db->join('offer_types','offer_types.id=offers.type');

        $this->db->limit(10,$offset);
        
        $objQuery = $this->db->get();
        
        return $objQuery->result_array();
    }
    /**
     * gettotaloffersofmerchant
     *
     * retrievs total count of offers of merchant
     *
     *@author Ketan Sangani
     *@access public
     *@param integer - $merchantid
     *@return array
     */
    function gettotaloffersofmerchant($merchantid)
    {

       $objQuery= $this->db->query("SELECT otap_offers.offer_id, otap_offers.title,otap_offers.description,otap_offers.start_date,otap_offers.end_date,otap_offers.amount,otap_offers.offer_discount,otap_merchants.merchant_name,otap_offer_types.offer_type
FROM otap_offers
JOIN otap_merchants ON otap_merchants.merchant_id=otap_offers.merchant_id
JOIN otap_offer_types ON otap_offer_types.id=otap_offers.type
WHERE FIND_IN_SET($merchantid, TRIM( REPLACE(otap_offers.merchant_id, ' ', '' ) ) ) >0");

        return  $objQuery->num_rows();
    }
      /**
    * getofferslistofmerchant
    *
    * retrievs the offers details of merchant
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $merchantid,$offset
    *@return array 
    */
    function getofferslistofmerchant($merchantid,$offset)
    {
      
        
       $objQuery= $this->db->query("SELECT otap_offers.offer_id, otap_offers.title,otap_offers.maxdiscountperuser,otap_offers.maxdiscountpertransaction,otap_offers.promotionaltext,  otap_offers.description,otap_offers.start_date,otap_offers.end_date,otap_offers.amount,otap_offers.offer_discount,otap_merchants.merchant_name,otap_offer_types.offer_type
FROM otap_offers
JOIN otap_merchants ON otap_merchants.merchant_id=otap_offers.merchant_id
JOIN otap_offer_types ON otap_offer_types.id=otap_offers.type
WHERE FIND_IN_SET($merchantid, TRIM( REPLACE(otap_offers.merchant_id, ' ', '' ))) >0
 LIMIT ".$offset.",10");
       
        return $objQuery->result_array();
    }
    
    
    /**
    * saveofferDetails
    *
    * Save the offer details.
    * 
    *@author Ketan Sangani
    *@access public
    *@param array - $arrData
    *@return Integer - No. of rows affected
    */
    function saveofferDetails($arrData)
    {
        $objQuery = $this->db->insert('offers', $arrData);
        return $this->db->affected_rows();
    }

  /**
    * saveoffercache
    *
    * Save the offer in offercache table for admin approval.
    * 
    *@author Ketan Sangani
    *@access public
    *@param array - $arrData
    *@return Integer - No. of rows affected
    */
    function saveoffercache($arrData)
    {
        $objQuery = $this->db->insert('offerscache', $arrData);
        return $this->db->affected_rows();
    }
     /**
   * delete_offer
   *
   * This is used to delete offer details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId
   * @return boolean
   */
  function delete_offer($iNewfeedId){

    
    if($this->db->delete('offers', array('offer_id' => $iNewfeedId)))
    {
        return true;
    }
    else
    {
        return false;
    }
  }
  /**
   * update_offer
   *
   * This is used to update  offer details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData, integer-$iNewfeedId
   * @return boolean
   */
  function update_offer($iNewfeedId,$arrData){
  ;
    $this->db->where('offer_id',$iNewfeedId);
    if($this->db->update('offers', $arrData))
    {

        return true;
    }
    else
    {
        return false;
    }
  }
/**
   * set_offer
   *
   * This is used to publish offer details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId
   * @return boolean
   */
  function set_offer($iNewfeedId){

    
    $query=$this->db->query("UPDATE otap_offers SET status='1' WHERE offer_id='$iNewfeedId'");

  }
     /**
   * unset_offer
   *
   * This is used to unpublish offer details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId
   * @return status
   */
   function unset_offer($iNewfeedId){

    
     $query=$this->db->query("UPDATE otap_offers SET status='0' WHERE offer_id='$iNewfeedId'");
  }
   /**
    * getoffertypeDetails
    *
    * retrievs the offer type details
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $offerId
    *@return array 
    */
    function getoffertypeDetails($offerId = False)
    {
        if($offerId){
            $this->db->where('id', $offerId);
        }
        $this->db->select('*');
        
        $objQuery = $this->db->get('offer_types');
        return $objQuery->result_array();
    }
    
    
    /**
    * saveoffertypeDetails
    *
    * Save the offer type details.
    * 
    *@author Ketan Sangani
    *@access public
    *@param array - $arrData
    *@return Integer - No. of rows affected
    */
    function saveoffertypeDetails($arrData)
    {
        $objQuery = $this->db->insert('offer_types', $arrData);
        return $this->db->affected_rows();
    }
 /**
   * update_type
   *
   * This is used to update  offer type details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData, integer-$iNewfeedId
   * @return boolean
   */
  function update_type($iNewfeedId,$arrData){
  ;
    $this->db->where('id',$iNewfeedId);
    if($this->db->update('offer_types', $arrData))
    {

        return true;
    }
    else
    {
        return false;
    }
  }
 }

/* End of file Offers_model.php */
/* Location: ./application/models/Offers_model.php */
