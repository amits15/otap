<?php
/**
 * Safe Doc
 *
 * @Description  This class is used to interact with the Newsfeed table using Codeignitor db core class. All the Data Insert,Retrival and Update operations related to country are performed here.
 *
 * @package   Codeigniter
 * @subpackage  Model
 * @author    Ashok Jadhav
 * @copyright Copyright (c) 2012 - 2013
 * @since   Version 1.0
 */

// ------------------------------------------------------------------------

/**
 *
 * This is Settings Model
 *
 * @author  Ashok Jadhav
 * @package Codeigniter
 * @subpackage  Model
 */

class Settings_model extends CI_Model{

  // --------------------------------------------------------------------

/**
 * __construct
 *
 * Calls parent constructor
 * @author  Ashok Jadhav
 * @access  public
 * @return  void
 */
    
    function __construct()
    {
        // Initialization of class
        parent::__construct();
    }

/**
 * set_password
 *
 * This is used to set new password.
 *
 * @author  Ashok Jadhav
 * @access  public
 * @param   integer-$id,$password,$cn_password
 * @return boolean
 */
    function set_password($id,$password,$cn_password)
    {
        $this->db->select('password');
        $this->db->where('id',$id);
        $this->db->where('password',$password);
        $objQuery = $this->db->get('admin');
        if($objQuery->num_rows()>0){
            $Q=$this->db->query("UPDATE exim_admin SET password='$cn_password' WHERE id='$id'");
            return true;
        }
        else{
            return false;
        }
    }


}

/* End of file settings_model.php */
/* Location: ./application/models/settings_model.php */