<?php
/**
 * Safe Doc
 *
 * @Description  This class is used to interact with the admin table using Codeignitor db core class. All the Data Insert,Retrival and Update operations related to admin are performed here.
 *
 * @package Safe Doc
 * @subpackage  Model
 * @author Amit Salunkhe
 * @copyright	Copyright (c) 2015
 * @since Version 1.0
 */

// ------------------------------------------------------------------------

/**
 *
 * This is Merchants Model
 *
 * @author Amit Salunkhe
 * @package Codeigniter
 * @subpackage	Model
 */

class Merchants_model extends CI_Model{

    // --------------------------------------------------------------------

   /**
    * __construct
    *
    * Calls parent constructor
    * @author	Ketan Sangani
    * @access	public
    * @return	void
    */
    function __construct()
    {
        // Initialization of class
        parent::__construct();
    }
    /**
     * loginmerchant
     *
     * This is for login of the merchant
     *
     * @author Ketan Sangani
     * @access  public
     * @param   array-$data
     * @return  void
     */
    public function loginmerchant($email,$password)
    {
       $result = $this->get_merchant($email,$password);
            
            /*--------Send OTP Sms ----------------*/
            
            if ($result->num_rows() > 0)
            { 
                //if login credentials are correct set session variables
                
                $row = $result->row_array();
                if($row['verified']==1){                
                $res['isValid']  = true;
                $res['isVerified'] = true;
                $res['info'] = array('merchantid' => $row['merchant_id'] ,'name'=>$row['merchant_name'],'shopname'=>$row['merchantshop_name'],'image'=>$row['profilepic'],'mobile'=>$row['merchant_contact'],'email'=>$row['merchant_email'],'address'=>$row['address'],'isSMSON'=>$row['isSmson']);
                $res['message'] = "You Have succesfully logged in";
              }
              elseif($row['verified']==0){
                $OTP = 9999;   
            //$OTP = mt_rand(100000,999999);
            $this->load->helper('sms_helper');      
            $message = "Dear Merchant,\nYour OTP is $OTP\nThank you.";
            //$response = send_otp($message,  $row['merchant_contact']);
                $this->db->where('merchant_id', $row['merchant_id']);
                $update = $this->db->update('merchants', array('merchant_pin' => $OTP));
                if($update){
               $res['isValid'] = true;
            $res['isVerified'] = false;
                $res['info'] = array('merchantid' => $row['merchant_id'],'mobile'=>$row['merchant_contact']);
                $res['message'] = "OTP sends On Your Mobile Number please verify it";
                 }
              }
            }
            else
            {  
            
            $res['isValid']  = false;
            $res['message'] = "Your Email Or Password are incorrect";         
                
             }

             echo json_encode($res);
    }
     /**
    * get_merchant
    *
    * retrievs the merchant details based on email and password
    * 
    *@author Ketan Sangani
    *@access public
    *@param string -$email,$password
    *@return array 
    */
    function get_merchant($email,$password)
            
    {
        $this->db->select('*');
        $this->db->where('merchant_email',$email);
        $this->db->where('password_hash',MD5($password));
        //$this->db->where('verified',1);
        $this->db->limit(1);
        $result = $this->db->get('merchants');
    
        return $result;
    }

     /**
    * getalreadymerchantexist
    *
    * check merchant already registered with email or mobile number
    * 
    *@author Ketan Sangani
    *@access public
    *@param string -$email,$mobile
    *@return array 
    */
    function getalreadymerchantexist($email,$mobile)
            
    {
      $where = "merchant_email='$email' OR merchant_contact='$mobile'";
        $this->db->select('*');
        $this->db->where($where);
        //$this->db->where('merchant_contact',$mobile);
        ///$this->db->where('verified',1);
        $this->db->limit(1);
        $result = $this->db->get('merchants');
      //echo $this->db->last_query();exit;
        return $result->result_array();
    }
  
   /**
    * getmerchantDetails
    *
    * retrievs the merchant details based on company ID
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $companyId
    *@return array 
    */
    function getmerchantDetails($companyId =0)
    {
        if($companyId!=0){
            $this->db->where('merchant_id', $companyId);
        }
         $this->db->select('*');
        $objQuery = $this->db->get('merchants');
        return $objQuery->result_array();
    }
    /**
    * getmerchantcount
    *
    * retrievs the total count of merchant
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $companyId
    *@return array 
    */
    function getmerchantcount()
    {
        
         $this->db->select('*');
        $objQuery = $this->db->get('merchants');
        return $objQuery->num_rows();
    }
    /**
     * getmerchantDetails
     *
     * retrievs the merchant details based on company ID
     *
     *@author Ketan Sangani
     *@access public
     *@param integer - $companyId
     *@return array
     */
    function getmerchants($offset)
    {
        $this->db->select('merchant_id,merchant_name,merchantshop_name,merchant_contact,address,bankname,accountnumber,branch,ifsc,micr,profilepic');

        $this->db->limit(10,$offset);
        $objQuery = $this->db->get('merchants');
        return $objQuery->result_array();
    }
    /**
    * getmerchantprofile
    *
    * retrievs the merchant details based on company ID
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $companyId
    *@return array 
    */
    function getmerchantprofile($companyId)
    {
       
         $this->db->select('merchant_id,merchant_name,merchantshop_name,merchant_contact,address,bankname,accountnumber,branch,ifsc,micr,profilepic,latitude,longitude,description');

         $this->db->where('merchant_id', $companyId);
         $objQuery = $this->db->get('merchants');
        return $objQuery->row_array();
    }
    /**
    * getmerchantcontactsDetails
    *
    * retrievs the merchant contacts details based on merchantid
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $companyId
    *@return array 
    */
    function getmerchantcontactsDetails($merchantid,$id=0)
    {
           $this->db->select('merchants_contacts.*,merchants.merchant_name');
           $this->db->from('merchants_contacts');
           $this->db->join('merchants','merchants_contacts.merchant_id=merchants.merchant_id');
          $this->db->where('merchants_contacts.merchant_id', $merchantid);
          $this->db->where('merchants.merchant_id', $merchantid);
          $this->db->where('merchants_contacts.isDeleted',0);
          if($id!=0){
           $this->db->where('merchants_contacts.id', $id);
          }        
        $objQuery = $this->db->get();
        return $objQuery->result_array();
    }
     /**
    * getmerchantcontacts
    *
    * retrievs the merchant contacts details based on merchantid
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $merchantid
    *@return array 
    */
    function getmerchantcontacts($merchantid,$offset)
    {
           $this->db->select('merchants_contacts.id,merchants_contacts.contact,merchants_contacts.status,merchants_contacts.employeename,merchants_contacts.cindex');
           
          $this->db->where('merchant_id', $merchantid);
           $this->db->where('isDeleted',0);
           $this->db->limit(10,$offset);
        $objQuery = $this->db->get('merchants_contacts');
        return $objQuery->result_array();
    }
    /**
     * gettotalcountmerchantcontacts
     *
     * retrievs total count of employees of merchant
     *
     *@author Ketan Sangani
     *@access public
     *@param integer - $merchantid
     *@return array
     */
    function gettotalcountmerchantcontacts($merchantid)
    {
             $this->db->select('*');
         $this->db->where('merchant_id', $merchantid);
           $this->db->where('isDeleted',0);
           
        $objQuery = $this->db->get('merchants_contacts');

        return  $objQuery->num_rows();
    }
    /**
   * savemerchantDetails
   *
   * This is used to save merchant details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData
   * @return boolean
   */
  function savemerchantDetails($arrData){

    if($this->db->insert('merchants', $arrData)){
      
return $this->db->insert_id();
   
    }else{

      return false;
    }

  }
    /**
   * savemerchantcontactDetails
   *
   * This is used to save merchant contact details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData
   * @return boolean
   */
  function savemerchantcontactDetails($arrData){

    if($this->db->insert('merchants_contacts', $arrData)){
      
return true;
   
    }else{
      
      return false;
    }

  }
   /**
   * request_update
   *
   * This is used to send request of updating profile of merchant
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData
   * @return boolean
   */
  function request_update($arrData){

    if($this->db->insert('merchantscache', $arrData)){
      
return true;
   
    }else{
      
      return false;
    }

  }
  /**
    * getupdaterequests
    *
    * retrievs the requests of updation of profile
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $companyId
    *@return array 
    */
    function getupdaterequests($companyId =0)
    {
        if($companyId!=0){
            $this->db->where('id', $companyId);
        }
         $this->db->select('*');
        $objQuery = $this->db->get('merchantscache');
        return $objQuery->result_array();
    }
   /**
   * update_merchant
   *
   * This is used to update  merchant details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData, integer-$iNewfeedId
   * @return boolean
   */
  function update_merchant($iNewfeedId,$arrData){
  ;
    $this->db->where('merchant_id',$iNewfeedId);
    if($this->db->update('merchants', $arrData))
    {
    //$this->db->last_query();exit;
        return true;
    }
    else
    {
        return false;
    }
  }
  /**
   * update_merchantcontact
   *
   * This is used to update  merchant contact details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData, integer-$iNewfeedId
   * @return boolean
   */
  function update_merchantcontact($iNewfeedId,$mobile,$arrData){
  
    $this->db->where('merchant_id',$iNewfeedId);
    $this->db->where('contact',$mobile);
    if($this->db->update('merchants_contacts', $arrData))
    {
    
    
        return true;
    }
    else
    {
        return false;
    }
  }
  /**
    * getmerchantsforcategory
    *
    * retrievs the merchants for particular category
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $companyId
    *@return array 
    */
    function getmerchantsforcategory($categoryid,$offset = NULL)
    { 
      
        
      $objQuery= $this->db->query("SELECT merchant_id,merchant_name,profilepic,address
FROM `otap_merchants`
WHERE FIND_IN_SET($categoryid, TRIM( REPLACE(categories, ' ', '' ) ) ) >0 LIMIT ".$offset.",10");
      //echo $this->db->last_query();exit;
        return $objQuery->result_array();
    }
    /**
    * gettotalmerchantsforcategory
    *
    * retrievs the TOTAL rows of merchants for particular category
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $categoryid
    *@return array 
    */
    function gettotalmerchantsforcategory($categoryid)
    { 
      
        
      $objQuery= $this->db->query("SELECT merchant_id,merchant_name,profilepic,address
FROM `otap_merchants`
WHERE FIND_IN_SET($categoryid, TRIM( REPLACE(categories, ' ', '' ) ) ) >0");
      //echo $this->db->last_query();exit;
        return $objQuery->num_rows();
    }
   /**
   * update_merchantdetails
   *
   * This is used to update  merchant contact details in merchans tables
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData, integer-$iNewfeedId
   * @return boolean
   */
  function update_merchantdetails($iNewfeedId,$mobile,$arrData){
  
    $this->db->where('merchant_id',$iNewfeedId);
    $this->db->where('merchant_contact',$mobile);
    if($this->db->update('merchants', $arrData))
    {
    
        return true;
    }
    else
    {
        return false;
    }
  }
  /**
   * update_merchantcontactdetails
   *
   * This is used to update  multiple merchant contact details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData, integer-$iNewfeedId
   * @return boolean
   */
  function update_merchantcontactdetails($iNewfeedId,$arrData){
  
    $this->db->where('id',$iNewfeedId);
    
    if($this->db->update('merchants_contacts', $arrData))
    {
    
        return true;
    }
    else
    {
        return false;
    }
  }
   /**
   * delete_merchant
   *
   * This is used to delete merchant details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId
   * @return boolean
   */
  function delete_merchant($iNewfeedId){

    if($this->db->delete('merchants', array('merchant_id' => $iNewfeedId)))
    {
        return true;
    }
    else
    {
        return false;
    }
  }
    
   
/**
   * set_merchant
   *
   * This is used to publish merchant details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId
   * @return boolean
   */
  function set_merchant($iNewfeedId){

    
    $query=$this->db->query("UPDATE otap_merchant SET status='1' WHERE merchant_id='$iNewfeedId'");

  }
     /**
   * unset_merchant
   *
   * This is used to unpublish company details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId
   * @return status
   */
   function unset_merchant($iNewfeedId){

    
     $query=$this->db->query("UPDATE otap_merchant SET status='0' WHERE merchant_id='$iNewfeedId'");
  }
  /**
   * set_contact
   *
   * This is used to publish merchant contact details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId
   * @return boolean
   */
  function set_contact($iNewfeedId){

    
    $query=$this->db->query("UPDATE otap_merchants_contacts SET status='1' WHERE id='$iNewfeedId'");

  }
     /**
   * unset_contact
   *
   * This is used to unpublish merchant contact details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId
   * @return status
   */
   function unset_contact($iNewfeedId){

    
     $query=$this->db->query("UPDATE otap_merchants_contacts SET status='0' WHERE id='$iNewfeedId'");
  }

   /**
    * getcategoryDetails
    *
    * retrievs the merchant category details based on company ID
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $companyId
    *@return array 
    */
    function getcategoryDetails($companyId =0)
    {
        if($companyId!=0){
            $this->db->where('category_id', $companyId);
        }
         $this->db->select('category_id,category,category_image,desc');
        $objQuery = $this->db->get('category');
        return $objQuery->result_array();
    }
    /**
    * getcategories
    *
    * retrievs the merchant category list
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $offset
    *@return array 
    */
    function getcategories($offset)
    {
         $this->db->select('category_id,category, category_image');
         $this->db->limit(10,$offset);
        $objQuery = $this->db->get('category');
        return $objQuery->result_array();
    }
     /**
    * getcategories
    *
    * retrievs the merchant category list
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $offset
    *@return array 
    */
    function getcategoriesofmerchants()
    {
         $this->db->select('category_id,category');
         
        $objQuery = $this->db->get('category');
        return $objQuery->result_array();
    }
      /**
    * gettotalcategories
    *
    * retrievs the total number of merchant category 
    * 
    *@author Ketan Sangani
    *@access public
    *@param 
    *@return array 
    */
    function gettotalcategories()
    {
         $this->db->select('category_id,category, category_image');
         
        $objQuery = $this->db->get('category');
        return $objQuery->num_rows();
    }
    
    /**
   * savecategoryDetails
   *
   * This is used to save merchant category details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData
   * @return boolean
   */
  function savecategoryDetails($arrData){

    if($this->db->insert('category', $arrData)){
return true;
    }else{
      return false;
    }

  }
   /**
   * update_category
   *
   * This is used to update  merchant category details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData, integer-$iNewfeedId
   * @return boolean
   */
  function update_category($iNewfeedId,$arrData){
  
    $this->db->where('category_id',$iNewfeedId);
    if($this->db->update('category', $arrData))
    {
    //$this->db->last_query();exit;
        return true;
    }
    else
    {
        return false;
    }
  }
   /**
   * delete_category
   *
   * This is used to delete merchant category details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId
   * @return boolean
   */
  function delete_category($iNewfeedId){

    if($this->db->delete('category', array('category_id' => $iNewfeedId)))
    {
        return true;
    }
    else
    {
        return false;
    }
  }
    
   
/**
   * set_category
   *
   * This is used to publish merchant category details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId
   * @return boolean
   */
  function set_category($iNewfeedId){

    
    $query=$this->db->query("UPDATE otap_category SET status='1' WHERE category_id='$iNewfeedId'");

  }
     /**
   * unset_category
   *
   * This is used to unpublish merchant category details
   *
   * @author  Ketan Sangani
   * @access  public
   * @param  integer-$iNewfeedId
   * @return status
   */
   function unset_category($iNewfeedId){

    
     $query=$this->db->query("UPDATE otap_category SET status='0' WHERE category_id='$iNewfeedId'");
  }
  /**
     * verify_otp
     *
     * This is for pin Verification of the Merchant
     *
     * @author  Ketan Sangani
     * @access  public
     * @param   string $u, string $pw
     * @return  void
     */
    function verify_otp($otp,$userid)
    {
        if($otp != '' && strlen($otp)== 4 )
        {
        $this->db->select('*');
        $this->db->where('merchant_id', $userid);                
        $this->db->limit(1);
        $result = $this->db->get('merchants');
        if ($result->num_rows() > 0)
            { 
                $row = $result->row_array();
                if($row['merchant_pin'] == $otp)
                {
                  $this->db->where('merchant_id', $userid);
                  $update = $this->db->update('merchants', array('verified' => 1));
                  if($update){                   
                  $res['isValid']  = true;
                  $res['isVerified'] = true;
                  $res['info'] = array('merchantid' => $row['merchant_id'] ,'name'=>$row['merchant_name'],'shopname'=>$row['merchantshop_name'],'image'=>$row['profilepic'],'mobile'=>$row['merchant_contact'],'email'=>$row['merchant_email'],'address'=>$row['address'],'isSMSON'=>$row['isSmson']);
                  $res['message'] = "You Have Signup Succesfully";
                  }   
                }
                else
                {
                    $res['isValid']=false;
                    $res['message']='Please enter correct Pin number';
                    
                }
                }
    
        }
        else 
        {
            $res['isValid']=false;
            $res['message']='Enter Valid Pin number';
            }
        
        echo json_encode($res);
    }
     /**
    * favouritemerchantexist
    *
    *Check whether merchant already added in favourites or not
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $userid,$merchantid
    *@return boolean 
    */
    function favouritemerchantexist($userid,$merchantid)
    {
        
         $this->db->select('*');
         $this->db->where('merchant_id',$merchantid);
         $this->db->where('userid',$userid);
        $objQuery = $this->db->get('favouritesmerchants');
        if($objQuery->num_rows()>0){
          return $objQuery->row_array();
        }else{
          return false;
        }
        
    }



    /**
   * addmerchanttofavourite
   *
   * This is used to add merchant in favourite or remove from it
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData
   * @return boolean
   */
  function addmerchanttofavourite($arrData){

    if($this->db->insert('favouritesmerchants', $arrData)){
      return true;
    }else{
      return false;
    }

  }
   /**
   * updatemerchantfavourite
   *
   * Merchany Add or revmove from favourites of existing data
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData, integer-$userid,$merchantid,
   * @return boolean
   */
  function updatemerchantfavourite($userid,$merchantid,$arrData){
  
    $this->db->where('merchant_id',$merchantid);
    $this->db->where('userid',$userid);
    if($this->db->update('favouritesmerchants', $arrData))
    {
    //$this->db->last_query();exit;
        return true;
    }
    else
    {
        return false;
    }
  }
   /**
    * getfavouritesmerchants
    *
    * Retrieves list of favourites merchants of user
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $userid,$offset
    *@return array 
    */
    function getfavouritesmerchants($userid,$offset)
    {
         $this->db->select('favouritesmerchants.userid,favouritesmerchants.merchant_id,merchants.merchant_name,merchants.profilepic,merchants.address');
         $this->db->from('favouritesmerchants');
         $this->db->join('merchants','merchants.merchant_id=favouritesmerchants.merchant_id','left');
         $this->db->where('favouritesmerchants.userid',$userid);
         $this->db->where('favouritesmerchants.isfav',1);
         $this->db->limit(10,$offset);
         $objQuery = $this->db->get();
         return $objQuery->result_array();
    }
    /**
    * gettotalfavouritesmerchants
    *
    * Retrieves total count of favurites merchants of user
    * 
    *@author Ketan Sangani
    *@access public
    *@param integer - $userid
    *@return array 
    */
    function gettotalfavouritesmerchants($userid)
    {
         $this->db->select('favouritesmerchants.userid,favouritesmerchants.merchant_id,merchants.merchant_name,merchants.profilepic,merchants.address');
         $this->db->from('favouritesmerchants');
         $this->db->join('merchants','merchants.merchant_id=favouritesmerchants.merchant_id','left');
         $this->db->where('favouritesmerchants.userid',$userid);
         $this->db->where('favouritesmerchants.isfav',1);
        
         $objQuery = $this->db->get();
         return $objQuery->num_rows();
    }
    /**
     * searchmerchant
     *
     * search merchant
     *
     *@author Ketan Sangani
     *@access public
     *@param string -$search,integer-$offset
     *@return array
     */
    function searchmerchant($search,$offset)

    {

        $this->db->select('merchant_id,merchant_name,profilepic,address');
        $this->db->like('merchant_name',$search);
        $this->db->limit(10,$offset);
        $objQuery = $this->db->get('merchants');
        return $objQuery->result_array();
    }
     /**
     * searchmerchantcount
     *
     * search merchant count
     *
     *@author Ketan Sangani
     *@access public
     *@param string -$search
     *@return array
     */
    function searchmerchantcount($search)

    {

        $this->db->select('merchant_id,merchant_name,profilepic,address');
        $this->db->like('merchant_name',$search);
        
        $objQuery = $this->db->get('merchants');
        return $objQuery->num_rows();
    }
    /**
     * getnearbymerchants
     *
     * search merchant
     *
     *@author Ketan Sangani
     *@access public
     *@param string -$search,integer-$offset
     *@return array
     */
    function getnearbymerchants($lat,$long,$offset)

    {
          // we'll want everything within, say, 10km distance
          $distance = 50;

          // earth's radius in km = ~6371
          $radius = 6371;

          // latitude boundaries
          $maxlat = $lat + rad2deg($distance / $radius);
          $minlat = $lat - rad2deg($distance / $radius);

          // longitude boundaries (longitude gets smaller when latitude increases)
          $maxlng = $long + rad2deg($distance / $radius / cos(deg2rad($lat)));
          $minlng = $long - rad2deg($distance / $radius / cos(deg2rad($lat)));
      
        $this->db->select('merchant_id,merchant_name,profilepic,address,latitude,longitude');
         $this->db->where('latitude <',$maxlat);
        $this->db->where('latitude >',$minlat);
         $this->db->where('longitude <',$maxlng);
        $this->db->where('longitude >',$minlng);

        $this->db->limit(10,$offset);
        $objQuery = $this->db->get('merchants');
        return $objQuery->result_array();
    }
    /**
     * totalgetnearbymerchants
     *
     * total number of  nearby merchants
     *
     *@author Ketan Sangani
     *@access public
     *@param float-$lat,$long
     *@return array
     */
    function totalgetnearbymerchants($lat,$long)

    {
          // we'll want everything within, say, 10km distance
          $distance = 50;

          // earth's radius in km = ~6371
          $radius = 6371;

          // latitude boundaries
          $maxlat = $lat + rad2deg($distance / $radius);
          $minlat = $lat - rad2deg($distance / $radius);

          // longitude boundaries (longitude gets smaller when latitude increases)
          $maxlng = $long + rad2deg($distance / $radius / cos(deg2rad($lat)));
          $minlng = $long - rad2deg($distance / $radius / cos(deg2rad($lat)));
      
        $this->db->select('merchant_id,merchant_name,profilepic,address,latitude,longitude');
          $this->db->where('latitude <',$maxlat);
        $this->db->where('latitude >',$minlat);
         $this->db->where('longitude <',$maxlng);
        $this->db->where('longitude >',$minlng);

       
        $objQuery = $this->db->get('merchants');
        return $objQuery->num_rows();
    }
    /**
     * checkmerchantdeviceregistered
     *
     * This is for check merchant registered with device
     *
     * @author Ketan Sangani
     * @access  public
     * @param   string-$username,$password
     * @return  void
     */
    function checkmerchantdeviceregistered($userid)
            
    {
        $this->db->select('*');
        $this->db->where('merchantid',$userid);
                
        $this->db->limit(1);
        $result = $this->db->get('merchantdevices');
        
        return $result->row_array();
    }
    /**
     * registermerchantdevice
     *
     * This is for register device of the Merchant
     *
     * @author Ketan Sangani
     * @access  public
     * @param   array-$data
     * @return  void
     */
    public function registermerchantdevice($data)
    {
        if($this->db->insert('merchantdevices', $data)){
          return true;
    }else{
      return false;
    }
    }
     /**
   * updateregisteredevice
   *
   * This is used to update  device  details of registered merchant
   *
   * @author  Ketan Sangani
   * @access  public
   * @param   array-$arrData, integer-$iNewfeedId
   * @return boolean
   */
  function updateregisteredevice($arrData,$iNewfeedId){

    $this->db->where('merchantid',$iNewfeedId);
    if($this->db->update('merchantdevices', $arrData))
    {
    
        return true;
    }
    else
    {
        return false;
    }
  }
   /**
     * forgot_password
     *
     * This is for retrievs details of merchant for forgot password
     *
     * @author Ketan Sangani
     * @access  public
     * @param   string-$username,$password
     * @return  void
     */
    function forgot_password($email)
            
    {
        $this->db->select('*');
        $this->db->where('merchant_email',$email);
        
        $q = $this->db->get('merchants');

        if($q->num_rows() == 0){
            return false;
        }
        else{
            $token = md5(uniqid(rand(), true));
              $this->db->where('merchant_email',$email);
            $update_flag = $this->db->update('merchants', array('token' => $token));
            
            
            if($update_flag){
                $this->db->where('merchant_email',$email);
                $q = $this->db->get('merchants');
                return $q->row_array();
            }
        }
    
       
    }

    /* check_token
     *
     * This function checks if the token is valid for a user.
     *
     * @param string
     * @return boolean
     */
    public function check_token($token)
    {
        $url = base64_decode(urldecode($token));
        $url_chunks = explode('/',$url);
        $this->db->where('merchant_id',$url_chunks['1']);
        $q = $this->db->get('merchants');

        if($q->num_rows()> 0){
            $result = $q->row_array();
            
            if($result['token']==$url_chunks['0'])
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    /**
     * update_reset_password
     *
     * resets password for forgot pasword process
     *
     * @param array
     * @return boolean
     */
    public function update_reset_password($data)
    {
        $this->db->where('merchant_id', $data['id']);
        $update_flag = $this->db->update('merchants', array('password_hash' => md5($data['password']), 'token' => ''));
        if($update_flag)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
/* End of file Merchants_model.php */
/* Location: ./application/models/Merchants_model.php */

