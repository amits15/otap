$(document).ready(function(){
    
        $("body").css('min-height', $(window).height() + 1 );
        $(window).resize(function(){
            $("body").css('min-height', $(window).height() + 1 );
        });
            //$("#title").on('keyup', function() {
            $('body').on('click','.imgchp', function() {
                //alert('hi');
             value = $(this).attr('id').split('_');
             
             //alert(value[1]);
             v = $('#first_'+value[1]).val();
             if(v==''){alert('please enter chapter number'); return false;}
             else{
             $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: baseURL+"admin/databases/get_chapter/"+v,
                            success: function(response) {
                                
                                                        $.each( response.records, function( i, val ) {
                                $('#txtbox'+value[1]).html(val.description);
                                //console.log(response);
                            });
                        
                        
                    }
            });
         }
        });
            //Add More
            $(".add-txt").click(function(){
                var no = $(".form-group").last().find('input#first_1').val();
                //alert(no);
                    no = parseInt(no,10) + 1;
                var more_textbox = $('<div class="form-group section _100">' +
                '<label class="col-sm-2 control-label" for="txtbox' + no + '">Chapter <span class="label-numbers"><input class="title required" id="first_'+no+'" name="title[]" type="text" value="" /><a href ="javascript:void(0);" class="imgchp" id="save_'+no+'"> <img class="update-txt"   src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/124.png"/></a></span></label>' +
                '<div class="col-sm-10">'+
                '<a href="#" class="btn btn-danger btn-xs remove-txt"><img src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/104.png"/></a>'+'<textarea class="form-control required" type="text" name="boxes[]" id="txtbox' + no + '" readonly></textarea>' +
                '</div></div>');
                more_textbox.hide();
                $(".form-group:last").after(more_textbox);
                more_textbox.fadeIn("slow");
                return false;
            });
             
            //Remove
            $('.form-horizontal').on('click','.remove-txt', function(event){
                //var a = $(event.target).parent().parent().parent().find('input#first').val();
                var Id =$(this).data('role');
                if (typeof Id == 'undefined') var Id = false;
                if(Id!=false){
                    var clicked = function(){
                        $.fallr('hide');

                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: baseURL+"admin/databases/delete_description/"+Id,
                            success: function(response) {
                                if(response.status == '1'){
                                        var flag = $( "#"+Id ).hasClass( "dynamic" );
                                        if(flag){
                                                $('#'+Id).parent().parent().parent().css( 'background-color', '#547eab' );
                                                $('#'+Id).parent().parent().parent().slideUp("slow", function() {
                                                        $('#'+Id).parent().parent().parent().parent().parent().css( 'background-color', '#FFFFFF' );
                                                        $('#'+Id).parent().parent().parent().remove();
                                                //alert('No');
                                                });
                                        }
                                        else{
                                                $('#'+Id).css( 'background-color', '#547eab' );
                                                $('#'+Id).slideUp("slow", function() {
                                                        $('#'+Id).parent().parent().css( 'background-color', '#FFFFFF' );
                                                        $('#'+Id).remove();
                                                //alert('No');
                                                });
                                        }
                                        
                                        
                                  
                                }
                            }
                        });
                    };
                    $.fallr('show', {
                        buttons : {
                                button1 : {text: 'Yes', danger: true, onclick: clicked},
                                button2 : {text: 'Cancel', onclick: function(){$.fallr('hide')}}
                        },
                        content : '<p>Are you sure you want to delete record ?</p>',
                        icon    : 'error'
                    });
                }
                else{
                    $(this).parent().parent().css( 'background-color', '#547eab' );
                    $(this).parent().parent().slideUp("slow", function() {
                        $(this).parent().parent().css( 'background-color', '#FFFFFF' );
                        $(this).remove();
                        //handle ordering only for adding new description
//                        $('.label-numbers').each(function(index){
//                            $(this).html('<input class="title" id="first" name="title[]" type="text" value="'+(index + 1)+'" />');
//                        });
                    });
                }
                return false;
            });
            
            //$(".abc").click(function() {
            /* $('body').on('click','.abc', function () {
                var focus = $(this).find('.label-text').text();             //get data from span
                var id =$(this).find('.label-text').attr('id');
                $(this).parent().append('<label class="def" id="label_def_'+id+'"><textarea class="form-control required"  name="txtbox'+id+'" id="txtbox'+id+'">'+focus+'</textarea></label><a title="Update" href="javascript:void(0);" class="btn btn-danger btn-xs update-txt"><img class="update-txt" id="img'+id+'" src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/124.png"/></a><div style="display:none" id="animate'+id+'"><a class="btn"><img src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/501.GIF"/></a>');
                $('#chapter'+id).removeAttr('readonly');//remove readonly attribute of chapter no for update
                $(this).hide();
                $('#img'+id).click(function(event) {
                       var desc =$('#txtbox'+id).val();
                        if(desc == ''){
                                $.fallr('show', {
                                        closeKey          : true,
                                        closeOverlay     : true,
                                        content            : '<p>This field is required.</p>',
                                        icon                    : 'info'
                                    }); 
                                return false;
                        }
                         $('#img'+id).hide();
                        $('#animate'+id).show();
                        var title =$('#chapter'+id).val();
                        $.ajax({
                                type: "POST",
                                dataType: "json",
                                data:{d:desc,t:title},
                                url: baseURL+"admin/databases/update_description/"+id,
                                success: function(response) {
                                        if(response.status == 1){
                                                if(response.records != ''){
                                                        $.each( response.records, function( i, val ) {
                                                                $('#label_abc_'+id).find('.label-text').html(val.description);//shows updated record in span
                                                                $('#label_abc_'+id).show();
                                                                $('#chapter'+id).val(val.title);//shows updated chapter no.
                                                                $('#chapter'+id).attr('readonly', 'readonly');
                                                                $('#label_def_'+id).remove();//remove textarea
                                                                $('#img'+id).parent().remove();//remove update button
                                                                $('#animate'+id).remove();//remove update loading  button
                                                                $('#remove'+id).show();//show remove button
                                                        });
                                                }
                                                setTimeout(function(){
                                                        $('#img'+id).show();
                                                        $('#animate'+id).hide();
                                                }, 10000);
                                        }
                                }
                        });
                });
            });*/
            
        //Add More in Edit page.
        //$("").click(function(){
        $('body').on('click','.add-txtupdate', function () {
                var a = $(".form-group").last().find('input').val();
		//alert(a);
                var no = a;
                no = parseInt(no,10) + 1;
                
                var more_textbox = $('<div class="form-group section _100">' +
                '<label class="col-sm-2 control-label" for="txtbox' + no + '">Chapter <span class="label-numbers"><input class="title required" id="first_'+no+'" name="title[]" type="text" value="" /><a href ="javascript:void(0);" class="imgchp" id="save_'+no+'"> <img class="update-txt"   src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/124.png"/></a></span></label>' +
                '<div class="col-sm-10">'+
                '<a href="#" class="btn btn-danger btn-xs remove-txt"><img src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/104.png"/></a>'+'<textarea class="form-control" type="text" name="boxes[]" id="txtbox' + no + '" readonly></textarea>' +
                '</div></div>');
                more_textbox.hide();
                $(".form-group:last").after(more_textbox);
                more_textbox.fadeIn("slow");
                return false;
                //$("img.update-txt1").click(function(){
                /*$('body').on('click','#img1_'+no, function () {
                        var array  = (this.id).split("_");
                        var id =array[1];
                        var desc =$('#txtbox1_'+id).val();
                        if(desc == ''){
                                $.fallr('show', {
                                        closeKey          : true,
                                        closeOverlay     : true,
                                        content            : '<p>This field is required.</p>',
                                        icon                    : 'info'
                                    });  
                                    return false;
                        }
                        var title =$('#chapter1_'+id).val();
                        $('#img1_'+id).hide();
                        $('#animate1_'+id).show();
                        $.ajax({
                                type: "POST",
                                dataType: "json",
                                data:{d:desc,t:title},
                                url: baseURL+"admin/databases/add_own_description/"+id+'/'+subcat_id,
                                success: function(response) {
                                        if(response.status == 1){
                                                $('#img1_'+id).show();
                                                $('#animate1_'+id).hide();
                                                //console.log(response.records);//exit;
                                                if(response.records != ''){
                                                        $.each( response.records, function( i, val ) {
                                                                $('#label_def_'+no).parent().append('<a href="javascript:void(0);" data-role="'+val.id+'" id="remove'+val.id+'" class="btn btn-danger btn-xs remove-txt"><img src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/104.png"></a>'+
                                                                '<label class="abc" id="label_abc_'+val.id+'">'+
                                                                '<span class="label-text dynamic" id="'+val.id+'">'+val.description+'</span>'+
                                                                '</label>');
                                                                $('#label_'+no).html('<input id="chapter'+val.id+'" class="title" name="chapter'+val.id+'" type="text" value="'+val.title+'" readonly="readonly">');
                                                                 $('#label_def_'+id).remove();//remove textarea
                                                                $('#chapter1_'+id).val(val.title);//shows updated chapter no.
                                                                $('#chapter1_'+id).attr('readonly', 'readonly');
                                                                $('#img1_'+id).parent().remove();//remove update button
                                                                $('#animate1_'+id).remove();//remove update loading  button
                                                                $('#rem_'+id).remove();//remove update loading  button
                                                        });
                                                }
                                        }
                                        
                                }
                        });
                });*/
        });
            
            
            
        });