$(document).ready(function(){
       //Add more ITC chapters
        $('body').on('click','.add-chptr', function () {
                var no = $(".container").length;
             
                //ajax request for get chapter list
                $.ajax({
                        type: "GET",
                        dataType: "json",
                        data:{},
                        url: baseURL+"admin/databases/chapterTittle/"+subcat_id,
                        success: function(response) {
                                //Fetches ITC chapter list 
                                //response = (response);
                                console.log(response);
                                if(response.ChapterExist != ''){
                                        var arr="[";
                                        $.each( response.ChapterExist, function( i1, val1 ) {
                                            
                                                arr = arr+val1.id+",";
                                                
                                        });
                                        arr = arr+"-5]";
                                      
                                        var middle='';
                                        if(response.chapterindex != ''){
                                                $.each(response.chapterindex, function( i, val ) {
                                                        if(jQuery.inArray(val.id, arr) != -1) {
                                                            console.log(val.id+"is in array");
                                                        } else {
                                                            middle =middle +  '<option value="'+val.id +'">'+(i+1)+'</option>';  
                                                            console.log(val.id+"is NOT in array");
                                                        } 
                                                        //middle =middle +  '<option value="'+val.id +'">'+(i+1)+'</option>';  
                                                });
                                        }
                                }
                            
                                //Create dropdown for selecting chapters
                                //alert(no);
                                var more_textbox = $('<div class="container" id="container_' +(no+1)+'"><div class="form-group section _100 title">' +
                                                     '<label for="ddlchapter_category"><b>ITC Chapter:</b>'+
                                                     '<select  name="ddlchapter_category" class="ddlchapter_category required">'+
                                                     '<option></option>' + middle +   '</select>'+
                                                     '</label>'+
                                                     '<div id="show_cptr_desc_' +(no+1)+'">'+
                                                     '</div></div></div>');
                                more_textbox.hide();
                                //append at the end of container
                                $('.ddlchapter_category').chosen();
                                $(".container:last").after(more_textbox);
                                more_textbox.fadeIn("slow");
                                return false;
                        }
                });
        });
        
       // setTimeout(function(){ 
                //$( ".ddlchapter_category" ).on( "change", function(){
                $('body').on('change','.ddlchapter_category', function () {
                        var id = $(this).val();
                        var ht =$(this).closest('.container').attr('id');
                        var a =ht.split("_"); 
                        ht = a[1];
                        //ajax request for get chapter title of selected chapter
                        $.ajax({
                                type: "POST",
                                dataType: "json",
                                url: baseURL+"admin/databases/get_ITC_Chapters/"+id,
                                success: function(response) {
                                        $('#show_cptr_desc_'+ht).html();
                                        $('#show_cptr_desc_'+ht).html('<a style="float:right;" href="javascript:void(0);" class="btn btn-success btn-xs add-ta add-andar" id="add_'+response.id+'"><img   src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/103.png"/></a><span><b>'+response.title+'</b></span>');
                                }
                        });
                
                        //Add more chapter description on selecting chapters.
                       // setTimeout(function(){ 
                                $("#add_"+id).click(function(){
                                        var a =(this.id).split("_"); 
                                        var contID=$(this).closest('.container').attr('id');
                                        var l = $('#'+contID+' > div').length;
                                        var no = a[1];
                                        
                                        //create div containing textarea for adding description,update and delete button 
                                        var more_textbox = $('<div class="form-group section _100">' +
                                        '<label class="label-numbers"><b>'+(l) +'</b></label>'+
                                        '<div>'+'<a href="javascript:void(0);"  class="btn btn-danger btn-xs remove-txt"><img src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/104.png"/></a>'+'<label class="def" id="label_def_"'+no+'><span class="label-text" id="'+ l +'"><textarea class="form-control required" type="text" name="tadesc_'+no+'_' + l + '" id="tadesc_'+no+'_' + l + '" ></textarea></span></label><a title="Update" href="javascript:void(0);" class="btn btn-danger btn-xs"><img class="update-txt1" id="img1_'+no+'_'+l+'" src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/124.png"/></a><div style="display:none" id="animate1_'+no+'_'+l+'"><a class="btn"><img src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/501.GIF"/></a></div>' +
                                        '</div></div>');
                                        more_textbox.hide();
                                        $("#"+contID).last().append(more_textbox);
                                        more_textbox.fadeIn("slow");
                                        var count = 0;
                                        $('#img1_'+no+'_'+l).click(function(){
                                                var array  = ('img1_'+no+'_'+l).split("_");
                                                var id =array[2];
                                                var desc =$('#tadesc_'+no+'_'+id).val();
                                                if(desc == ''){
                                                $.fallr('show', {
                                                        closeKey          : true,
                                                        closeOverlay     : true,
                                                        content            : '<p>This field is required.</p>',
                                                        icon                    : 'info'
                                                    });        s
                                                }
                                                $('#img1_'+no+'_'+id).hide();
                                                $('#animate1_'+no+'_'+id).show();
                                                //ajax request for adding new description
                                                $.ajax({
                                                        type: "POST",
                                                        dataType: "json",
                                                        data:{d:desc},
                                                        url: baseURL+"admin/databases/add_ITC_description/"+no+'/'+subcat_id,
                                                        success: function(response) {
                                                                if(response.status == 1){
                                                                        $('#img1_'+no+'_'+id).show();
                                                                        $('#animate1_'+no+'_'+id).hide();
                                                                        if(response.records != ''){
                                                                                $.each( response.records, function( i, val ) {
                                                                                        $('#tadesc_'+no+'_'+id).parent('span').append(val.description);//shows updated record in span
                                                                                        $('#tadesc_'+no+'_'+id).remove();//remove textarea
                                                                                        $('#img1_'+no+'_'+id).parent().remove();//remove update button
                                                                                        $('#animate1_'+no+'_'+id).remove();//remove update loading  button
                                                                                        //disable selectbox
                                                                                        $('#'+contID).find('.ddlchapter_category').prop('disabled', true).trigger("liszt:updated");
                                                                                        $('#'+contID).find('.ddlchapter_category').prop('disabled', true).trigger("chosen:updated");

                                                                                });
                                                                        }
                                                                }
                                                        }
                                                });
                                        });
                                }); 
                        //}, 1000);
                        return false;
                }); 
        //}, 5000);
            
        //Add More to Edit page.
        //$(".add-ta").click(function(){
        $('body').on('click','.add-ta', function () {
                var a =(this.id).split("_"); 
                var contID=$(this).closest('.container').attr('id');
                var l = $('#'+contID+' > div').length;
                var no = a[1];
                  
                var more_textbox = $('<div class="form-group section _100">' +
                '<label class="label-numbers"><b>'+(l) +'</b></label>'+
                '<div>'+'<a href="javascript:void(0);" id="rem_'+no+ '_'+l+'"  class="btn btn-danger btn-xs remove-txt"><img src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/104.png"/></a>'+'<label class="def" id="label_def_'+no+'_'+ l +'"><span class="label-text" id="'+ l +'"><textarea class="form-control required" type="text" name="tadesc_'+no+'_' + l + '" id="tadesc_'+no+'_' + l + '" ></textarea></span></label><a title="Update" href="javascript:void(0);" class="btn btn-danger btn-xs"><img class="update-txt1" id="img1_'+no+'_'+l+'" src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/124.png"/></a><div style="display:none" id="animate1_'+no+'_'+l+'"><a class="btn"><img src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/501.GIF"/></a></div>' +
                '</div></div>');
                more_textbox.hide();
                $("#"+contID).last().append(more_textbox);
                more_textbox.fadeIn("slow");
                var count = 0;
                $('#img1_'+no+'_'+l).click(function(){
                        var array  = ('img1_'+no+'_'+l).split("_");
                        var id =array[2];
                        var desc =$('#tadesc_'+no+'_'+id).val();
                        if(desc == ''){
                                $.fallr('show', {
                                        closeKey          : true,
                                        closeOverlay     : true,
                                        content            : '<p>This field is required.</p>',
                                        icon                    : 'info'
                                    });        
                        }
                        $('#img1_'+no+'_'+id).hide();
                        $('#animate1_'+no+'_'+id).show();
                        $.ajax({
                                type: "POST",
                                dataType: "json",
                                data:{d:desc},
                                url: baseURL+"admin/databases/add_ITC_description/"+no+'/'+subcat_id,
                                success: function(response) {
                                    //console.log(response);
                                        if(response.status == 1){
                                                $('#img1_'+no+'_'+id).show();
                                                $('#animate1_'+no+'_'+id).hide();
                                                if(response.records != ''){
                                                        $.each( response.records, function( i, val ) {
                                                                $('#label_def_'+no+'_'+id).parent().append('<a href="javascript:void(0);" data-role="'+val.id+'" id="remove'+val.id+'" class="btn btn-danger btn-xs remove-txt"><img src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/104.png"></a>'+
                                                                                                                        '<label class="abc" id="label_abc_'+val.id+'">'+
                                                                                                                                '<span class="label-text" id="'+val.id+'">'+val.description+'</span>'+
                                                                                                                        '</label>');
                                                                $('#label_def_'+no+'_'+id).remove();//remove textarea
                                                                $('#img1_'+no+'_'+id).parent().remove();//remove update button
                                                                $('#animate1_'+no+'_'+id).remove();//remove update loading  button
                                                                $('#rem_'+no+'_'+id).remove();//remove update loading  button
                                                                //disable selectbox
                                                                $('#'+contID).find('.ddlchapter_category').prop('disabled', true).trigger("liszt:updated");
                                                                $('#'+contID).find('.ddlchapter_category').prop('disabled', true).trigger("chosen:updated");

                                                        });
                                                }
                                        }
                                }
                        });
                });
                
        }); 
        
        
        //Remove
        $('.form-horizontal').on('click','.remove-txt', function(event){
                var Id =$(this).data('role');
                if (typeof Id == 'undefined') var Id = false;
                var contID=$(this).closest('.container').attr('id');
                var l = $('#'+contID+' > div').length;
                if(Id!=false){
                        var clicked = function(){
                                $.fallr('hide');
                                $.ajax({
                                        type: "POST",
                                        dataType: "json",
                                        url: baseURL+"admin/databases/delete_descriptionC/"+Id,
                                        success: function(response) {
                                                if(response.status == '1'){
                                                        $('#'+Id).css( 'background-color', '#547eab' );
                                                        $('#'+Id).parent().parent().parent().slideUp("slow", function() {
                                                                $('#'+Id).parent().parent().parent().css( 'background-color', '#FFFFFF' );
                                                                $('#'+Id).parent().parent().parent().remove();
                                                                //disable selectbox
                                                                if(l<3){
                                                                    $('#'+contID).find('.ddlchapter_category').prop('disabled', false).trigger("liszt:updated");
                                                                    $('#'+contID).find('.ddlchapter_category').prop('disabled', false).trigger("chosen:updated");
                                                                }
                                                                //handle ordering only for adding new description
                                                                $('#'+contID).children('.form-group').children('.label-numbers').each(function(index){
                                                                        $(this).html('<b>'+(index + 1)+'</b>');
                                                                });
                                                        });
                                                }
                                        }
                                });
                        };
                        $.fallr('show', {
                                buttons : {
                                        button1 : {text: 'Yes', danger: true, onclick: clicked},
                                        button2 : {text: 'Cancel', onclick: function()
                                                                                              {
                                                                                                  $.fallr('hide')
                                                                                              }
                                                        }   
                                },
                                content : '<p>Are you sure you want to delete record?</p>',
                                icon    : 'error'
                        });
                }
                else{
                        $(this).parent().parent().css( 'background-color', '#547eab' );
                        $(this).parent().parent().slideUp("slow", function() {
                                $(this).parent().parent().css( 'background-color', '#FFFFFF' );
                                $(this).remove();
                                //handle ordering only for adding new description
                                $('#'+contID).children('.form-group').children('.label-numbers').each(function(index){
                                        $(this).html('<b>'+(index + 1)+'</b>');
                                });
                        });
                }
                return false;
        });
            
        //$(".label-text").click(function(event) {
        $('body').on('click','.abc', function () {
                var focus = $(this).find('.label-text').text();             //get data from span
                var id =$(this).find('.label-text').attr('id');
                $(this).parent().append('<label class="def" id="label_def_'+id+'"><textarea class="form-control required"  name="tadesc_'+id+'" id="tadesc_'+id+'">'+focus+'</textarea></label><a title="Update" href="javascript:void(0);" class="btn btn-danger btn-xs update-txt"><img class="update-txt" id="img_'+id+'" src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/124.png"/></a><div style="display:none" id="animate_'+id+'"><a class="btn"><img src="'+baseURL+'template/admin/img/icons/packs/diagona/16x16/501.GIF"/></a>');
                $(this).hide();
                $('#tadesc_'+id).height( $('#tadesc_'+id)[0].scrollHeight );
                
            
                //Update
                $('#img_'+id).click(function(event) {
                        $('#img_'+id).hide();
                        $('#animate_'+id).show();
                        var desc =$('#tadesc_'+id).val();
                        $.ajax({
                                type: "POST",
                                dataType: "json",
                                data:{d:desc},
                                url: baseURL+"admin/databases/update_descriptionC/"+id,
                                success: function(response) {
                                        if(response.status == 1){
                                                if(response.records != ''){
                                                        $.each( response.records, function( i, val ) {
                                                                $('#label_abc_'+id).find('.label-text').html(val.description);//shows updated record in span
                                                                $('#label_abc_'+id).show();
                                                                $('#label_def_'+id).remove();//remove textarea
                                                                $('#img_'+id).parent().remove();//remove update button
                                                                $('#animate_'+id).remove();//remove update loading  button
                                                                $('#remove_'+id).show();//show remove button
                                                        });
                                                }
                                                setTimeout(function(){
                                                        $('#img_'+id).show();
                                                        $('#animate_'+id).hide();
                                                }, 10000);
                                        }   
                                
                                }
                        });
                }); 
        });
});
