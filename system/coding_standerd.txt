Form Element Declaration		 
		 
Form Element		Declaration		Example 
		 
Form			frmVariableName		frmCreateCompany 
textbox			txtVariableName		txtFirstName 
textarea		taVariableName		taDescription 
dropdown		ddlVariableName		ddlCountry 
radio			rdVariableName		rdGender 
Button			btnVariableName		btnSubmit 
Label			lblVariableName		lblFirstName 
Password		pwVariableName		pwPassword 
File			fileVariableName	fileImage 
Image			imgVariableName		imgProfile 
Checkbox		chkVariableName		chkFeatures 
Hidden			hidVariableName		hidFirstName 
		 
		 
		 
Variable Declaration		 
		 
Boolean	        bVariableName	 
Integer		iVariableName	 
String		strVariableName	 
array		arrVariableName	 
object		objVariableName	 
Decimal	        dVariableName	 
		 
		 
		 
Array Key Name:	$arrName[key_name];


// comment style

/**
 *function name
 *
 *Description
 *@author Author Name
 *@access Access specifier
 *@param Parameter Name
 *@return return type
 */





/**
 *save
 *
 *Save Data
 *@author Priyank Jain
 *@access public
 *@param array $arrData Key[Column Name] =>value 
 *@return void
 */


function save($arrData)
{
	$this->db->set('BorrowedOn','NOW()',FALSE);
	$this->db->insert('playerLoan',$arrData);
}


