-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2015 at 12:27 PM
-- Server version: 5.5.44
-- PHP Version: 5.4.45-1+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `margins_view`
--

-- --------------------------------------------------------

--
-- Table structure for table `mrgn_company`
--

CREATE TABLE IF NOT EXISTS `mrgn_company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(200) NOT NULL,
  `company_logo` varchar(200) NOT NULL,
  `company_division` varchar(100) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mrgn_customer`
--

CREATE TABLE IF NOT EXISTS `mrgn_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(255) NOT NULL,
  `customer_sales_qty` bigint(20) NOT NULL,
  `customer_sales_value` bigint(20) NOT NULL,
  `customer_margins` bigint(20) NOT NULL,
  `customer_margins_percentage` float NOT NULL,
  `year` varchar(10) NOT NULL,
  `quarter` varchar(5) NOT NULL,
  `company_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mrgn_division`
--

CREATE TABLE IF NOT EXISTS `mrgn_division` (
  `division_id` int(11) NOT NULL AUTO_INCREMENT,
  `division_name` int(255) NOT NULL,
  PRIMARY KEY (`division_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mrgn_domestic`
--

CREATE TABLE IF NOT EXISTS `mrgn_domestic` (
  `domestic_id` int(11) NOT NULL AUTO_INCREMENT,
  `domestic_name` int(255) NOT NULL,
  PRIMARY KEY (`domestic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mrgn_employees`
--

CREATE TABLE IF NOT EXISTS `mrgn_employees` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(200) NOT NULL,
  `employee_user_name` varchar(200) NOT NULL,
  `employee_password` varchar(200) NOT NULL,
  `created_date` datetime NOT NULL,
  `company_id` int(11) NOT NULL,
  `employee_type` int(11) NOT NULL DEFAULT '1' COMMENT '0 - Admin, 1 - Employee',
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mrgn_employees`
--

INSERT INTO `mrgn_employees` (`employee_id`, `employee_name`, `employee_user_name`, `employee_password`, `created_date`, `company_id`, `employee_type`) VALUES
(1, 'Admin', 'admin', '0f359740bd1cda994f8b55330c86d845', '2015-09-09 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mrgn_product`
--

CREATE TABLE IF NOT EXISTS `mrgn_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_sales_qty` bigint(20) NOT NULL,
  `product_sales_value` bigint(20) NOT NULL,
  `product_margins` bigint(20) NOT NULL,
  `product_margins_percentage` float NOT NULL,
  `year` varchar(10) NOT NULL,
  `quarter` varchar(7) NOT NULL,
  `company_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mrgn_related_data`
--

CREATE TABLE IF NOT EXISTS `mrgn_related_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cost_to_convert` bigint(20) NOT NULL,
  `cost_to_make` bigint(20) NOT NULL,
  `cost_to_sustain` bigint(20) NOT NULL,
  `cost_serve` bigint(20) NOT NULL,
  `sales_qty` bigint(20) NOT NULL,
  `sales_value` bigint(20) NOT NULL,
  `year` varchar(15) NOT NULL,
  `quarter` varchar(10) NOT NULL,
  `division_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `Product_id` int(11) NOT NULL,
  `segment_id` int(11) NOT NULL,
  `domestic_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mrgn_segment`
--

CREATE TABLE IF NOT EXISTS `mrgn_segment` (
  `segment_id` int(11) NOT NULL AUTO_INCREMENT,
  `segment_name` int(255) NOT NULL,
  PRIMARY KEY (`segment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mrgn_zones`
--

CREATE TABLE IF NOT EXISTS `mrgn_zones` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_name` int(255) NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
